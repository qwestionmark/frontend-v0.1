/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable no-underscore-dangle */
// This code comes from the official NextJS example called with-react-intl.
// Current link: https://raw.githubusercontent.com/zeit/next.js/canary/examples/with-react-intl/

const { basename } = require('path');
const glob = require('glob');
const areIntlLocalesSupported = require('intl-locales-supported').default;
const { readFileSync } = require('fs');
const { createServer } = require('http');
const next = require('next');
const accepts = require('accepts');
const { parse } = require('url');
const getLocale = require('./utils/getLocale');

// Get the supported languages by looking for translations in the `lang/` dir.
const supportedLanguages = glob.sync('./lang/*.json').map((f) => basename(f, '.json'));

// Polyfill Node with `Intl` that has data for all locales.
// See: https://formatjs.io/guides/runtime-environments/#server
if (global.Intl) {
  // Determine if the built-in `Intl` has the locale data we need.
  if (!areIntlLocalesSupported(supportedLanguages)) {
    // `Intl` exists, but it doesn't have the data we need, so load the
    // polyfill and patch the constructors we need with the polyfills.
    const IntlPolyfill = require('intl');
    Intl.NumberFormat = IntlPolyfill.NumberFormat;
    Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
    Intl.__disableRegExpRestore = IntlPolyfill.__disableRegExpRestore;
  }
} else {
  // No `Intl`, so use and load the polyfill.
  global.Intl = require('intl');
}

// Fix: https://github.com/zeit/next.js/issues/11777
// See related issue: https://github.com/andyearnshaw/Intl.js/issues/308
if (Intl.__disableRegExpRestore) {
  Intl.__disableRegExpRestore();
}

const port = parseInt(process.env.PORT, 10) || 3000;
const host = process.env.HOST || 'localhost';
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

// We need to expose React Intl's locale data on the request for the user's
// locale. This function will also cache the scripts by lang in memory.
const localeDataCache = new Map();
const getLocaleDataScript = (locale) => {
  const lang = locale.split('-')[0];
  if (!localeDataCache.has(lang)) {
    const localeDataFile = require.resolve(`@formatjs/intl-relativetimeformat/dist/locale-data/${lang}`);
    const localeDataScript = readFileSync(localeDataFile, 'utf8');
    localeDataCache.set(lang, localeDataScript);
  }
  return localeDataCache.get(lang);
};

// We need to load and expose the translations on the request for the user's
// locale. These will only be used in production, in dev the `defaultMessage` in
// each message description in the source code will be used.
const getMessages = (locale) => require(`./lang/${locale}.json`);

app.prepare().then(() => {
  createServer((req, res) => {
    if (
      process.env.REACT_APP_NODE_ENV === 'production' &&
      req.headers['x-forwarded-proto'] === 'http' &&
      process.env.FRONTEND_URL
    ) {
      res.writeHead(302, {
        Location: `${process.env.FRONTEND_URL}${req.url}`,
      });
      res.end();
      return;
    }
    const parsedUrl = parse(req.url, true);
    const { pathname, query, search } = parsedUrl;
    const accept = accepts(req);
    // Get the best accepted lang and supported lang from the incoming request
    const acceptedLocale = accept.language(supportedLanguages) || 'en';

    // Get the final locale, if `locale` cookie is set, it will prefer the cookie
    // one with "en" fallback if the locale is not supported.
    const locale = getLocale(req.headers, acceptedLocale, supportedLanguages);

    req.locale = locale;
    req.localeDataScript = getLocaleDataScript(locale);
    // Force defaultMessages to be used during dev
    req.messages = dev ? {} : getMessages(locale);
    // TODO: Rewrite the redirects in next.config.js
    // when https://github.com/zeit/next.js/issues/9081 will be closed.
    switch (pathname + search) {
      case '/search?i=Projects':
        app.render(req, res, '/search/projects', query);
        break;
      case '/search?i=Challenges':
        app.render(req, res, '/search/challenges', query);
        break;
      case '/search?i=Needs':
        app.render(req, res, '/search/needs', query);
        break;
      case '/search?i=Groups':
        app.render(req, res, '/search/groups', query);
        break;
      case '/search?i=Members':
        app.render(req, res, '/search/members', query);
        break;
      default:
        handle(req, res, parsedUrl);
        break;
    }
  }).listen(port, host, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://${host}:${port}`);
  });
});
