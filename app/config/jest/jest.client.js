module.exports = {
  ...require('./jest-common'),
  displayName: 'dom',
  // This is the default jest environment, it means jest has access to `document`, if the environment was node it won't have access to it.
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: [require.resolve('./setupTests.js')],

  // /////////////////////////////////////////////
  // setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  // snapshotSerializers: ['jest-emotion'],
};
