/* eslint-disable @rushstack/no-null */
// ? Too many states, maybe group all the form state into one state. Or use useReducer
/* eslint-disable no-nested-ternary */
import Link from 'next/link';
import signUpFormRules from './signUpFormRules.json';
import { FormattedMessage, useIntl } from 'react-intl';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { toAlphaNum } from '~/components/Tools/Nickname';
import { UserContext } from '~/contexts/UserProvider';
import Alert from '~/components/Tools/Alert';
import FormValidator from '~/components/Tools/Forms/FormValidator';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import ReactGA from 'react-ga';
// import Image from 'next/image';
import Image2 from '~/components/Image2';
// import "./Auth.scss";

const validator = new FormValidator(signUpFormRules);

const SignUp: NextPage = () => {
  const router = useRouter();

  const [acceptConditions, setAcceptConditions] = useState(false);
  const [acceptLegal, setAcceptLegal] = useState(false);
  const [mail_newsletter, setMail_newsletter] = useState(false);
  const [mail_weekly, setMail_weekly] = useState(false);
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [nickname, setNickname] = useState('');
  // Passing the mail from the url params to the state as a default value
  const [email_value, setEmail_value] = useState(router.query.email ? router.query.email : '');
  const [password, setPassword] = useState('');
  const [password_confirmation, setPassword_confirmation] = useState('');
  const [error, setError] = useState('');
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [stateValidation, setStateValidation] = useState({
    valid_password_confirmation: '',
    valid_acceptConditions: '',
    valid_acceptLegal: '',
    valid_first_name: '',
    valid_last_name: '',
    valid_nickname: '',
    valid_email: '',
    valid_password: '',
  });
  const api = useApi();
  const userContext = useContext(UserContext);
  const intl = useIntl();

  const checkPassword = (pwd, pwdConfirm) => {
    if (pwd !== pwdConfirm) {
      setStateValidation((prevState) => ({
        ...prevState,
        valid_password_confirmation: { isInvalid: 'is-invalid', message: 'err-4001' },
      }));
      return false;
    }
    return true;
  };
  const generateNickname = (firstName, lastName) => {
    let proposalNickname = firstName.trim() + lastName.trim();
    proposalNickname = toAlphaNum(proposalNickname);
    setNickname(proposalNickname);
    return proposalNickname;
  };

  const handleChange = (e) => {
    const { checked, name } = e.target;
    let { value } = e.target;
    let proposalNickname;
    switch (name) {
      case 'first_name':
        proposalNickname = generateNickname(value, last_name);
        setFirst_name(value);
        break;
      case 'last_name':
        proposalNickname = generateNickname(first_name, value);
        setLast_name(value);
        break;
      case 'nickname':
        value = toAlphaNum(value);

        setNickname(value);
        break;
      case 'acceptConditions':
        value = checked;
        setAcceptConditions(value);
        break;
      case 'mail_newsletter':
        value = checked;
        setMail_newsletter(value);
        break;
      case 'email':
        setEmail_value(value);
        break;
      case 'password':
        setPassword(value);
        break;
      case 'password_confirmation':
        setPassword_confirmation(value);
        break;
      case 'mail_weekly':
        value = checked;
        setMail_weekly(value);
        break;
      case 'acceptLegal':
        value = checked;
        setAcceptLegal(value);
        break;
      default:
        // console.warn(`Case ${name} is not handled`);
        break;
    }
    /* Validators start */
    const state = {};
    state[name] = value;

    // Check proposalNickname
    if (name === 'first_name' || name === 'last_name') {
      state.nickname = proposalNickname;
    }

    const validation = validator.validate(state);
    if (validation[name] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${name}`] = validation[name];
      // Update nickname too only if firstname or nickname has been changed
      if (name === 'first_name' || name === 'last_name') {
        newStateValidation.valid_nickname = validation.nickname;
      }
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    setError('');
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // make logo_url randomly equals to one of our default image (1 to 12)
    const logoUrl = `/images/default/default-user-${randomNumber}.png`;
    const user = {
      first_name,
      last_name,
      nickname,
      email: email_value,
      password,
      password_confirmation,
      acceptConditions,
      acceptLegal,
      mail_newsletter,
      mail_weekly,
      logo_url: logoUrl,
    };
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      if (checkPassword(password, password_confirmation)) {
        setError('');
        setSending(true);
        api
          .post('/api/users/', { user })
          .then((res) => {
            setSending(false);
            if (res?.data?.nickname) {
              setStateValidation((prevState) => ({
                ...prevState,
                valid_nickname: { isInvalid: 'is-invalid', message: 'err-4005' },
              }));
            } else {
              setSuccess(true);
              // send event to google analytics
              ReactGA.event({ category: 'User', action: 'signup' });
              // add "?success=1" to the url, so we can measure the number of people who signed up in google analytics
              router.push({ pathname: '/signup', query: { success: 1 } }, undefined, { shallow: true });
            }
          })
          .catch((err) => {
            if (err?.response?.data?.error) {
              const errorMessage = err.response.data.error;
              setError(errorMessage);
            }
            setSending(false);
          });
      }
    } else {
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };
  const {
    valid_acceptConditions,
    valid_acceptLegal,
    valid_password_confirmation,
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_email,
    valid_password,
  } = stateValidation;

  const errorMessage = error.includes('err-') ? (
    <FormattedMessage id={error} defaultMessage="An error has occurred" />
  ) : (
    error
  );

  useEffect(() => {
    if (userContext.isConnected) {
      router.push('/');
    }
  });

  let formStyle;
  let messageStyle;
  let title;
  let msg;
  if (!success) {
    formStyle = { display: 'block' };
    messageStyle = { display: 'none' };
    title = {
      text: 'Welcome to JOGL',
      id: 'signUp.title',
    };
    msg = {
      text: 'Empowering humanity to solve global challenges.',
      id: 'signUp.description',
    };
  } else {
    formStyle = { display: 'none' };
    messageStyle = { display: 'block' };
    title = {
      text: 'Thanks for registering. Please check your emails.',
      id: 'signup.confTitle',
    };
    msg = {
      text: 'To complete your sign up, click the verification link in the confirmation email sent to you.',
      id: 'signup.confMsg',
    };
  }

  return (
    <Layout
      className="no-margin"
      title={`${intl.formatMessage({ id: 'header.signUn', defaultMessage: 'Sign up' })} | JOGL`}
    >
      <Box pb={[10, undefined, undefined, 0]}>
        <div className="auth-form row align-items-center">
          <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
            <Link href="/">
              <a>
                <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" unsized />
              </a>
            </Link>
          </div>
          <div className="col-12 col-lg-7 rightPannel">
            <div className="form-content">
              <div className="form-header signup">
                <h2 className="form-title" id="signModalLabel">
                  <FormattedMessage id={title.id} defaultMessage={title.text} />
                </h2>
                <p>
                  <FormattedMessage id={msg.id} defaultMessage={msg.text} />
                </p>
              </div>

              <div className="form-body" style={formStyle}>
                <div className="goToSignUp signup">
                  <span>
                    <FormattedMessage id="signUp.alreadyAccount" defaultMessage="Already have an account?" />
                  </span>
                  <span className="goToSignUp--signup">
                    <Link href="/signin">
                      <a className="nav-link">
                        <FormattedMessage id="header.signIn" defaultMessage="Sign in" />
                      </a>
                    </Link>
                  </span>
                </div>

                <form onSubmit={handleSubmit} className="row">
                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage id="signUp.firstname" defaultMessage="First name" />
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="first_name"
                        id="first_name"
                        className={`form-control ${
                          valid_first_name ? (!valid_first_name.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={intl.formatMessage({ id: 'signUp.firstname.placeholder', defaultMessage: 'John' })}
                        onChange={handleChange}
                      />
                      {valid_first_name ? (
                        valid_first_name.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage id={valid_first_name.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage id="signUp.lastname" defaultMessage="Last Name" />
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="last_name"
                        id="last_name"
                        className={`form-control ${
                          valid_last_name ? (!valid_last_name.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={intl.formatMessage({ id: 'signUp.lastname.placeholder', defaultMessage: 'Doe' })}
                        onChange={handleChange}
                      />
                      {valid_last_name ? (
                        valid_last_name.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage id={valid_last_name.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage id="signUp.nickname" defaultMessage="Nickname" />
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="nickname"
                        id="nickname"
                        className={`form-control ${
                          valid_nickname ? (!valid_nickname.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={intl.formatMessage({
                          id: 'signUp.nickname.placeholder',
                          defaultMessage: 'johndoe',
                        })}
                        onChange={handleChange}
                        value={nickname}
                      />
                      {valid_nickname ? (
                        valid_nickname.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage id={valid_nickname.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      <FormattedMessage id="auth.email" defaultMessage="Email" />
                    </label>
                    <div className="input-group">
                      <input
                        type="email"
                        name="email"
                        id="email"
                        value={email_value}
                        placeholder={intl.formatMessage({
                          id: 'auth.email.placeholder',
                          defaultMessage: 'your.email@domain.com',
                        })}
                        className={`form-control ${
                          valid_email ? (!valid_email.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        onChange={handleChange}
                      />
                      {valid_email ? (
                        valid_email.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage id={valid_email.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="password">
                      <FormattedMessage id="signUp.pwd" defaultMessage="Password" />
                    </label>
                    <div className="input-group">
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className={`form-control ${
                          valid_password ? (!valid_password.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={intl.formatMessage({
                          id: 'signUp.pwd.placeholder',
                          defaultMessage: 'Your password',
                        })}
                        onChange={handleChange}
                      />
                      {valid_password ? (
                        valid_password.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage id={valid_password.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="password_confirmation">
                      <FormattedMessage id="signUp.pwdConfirm" defaultMessage="Password Confirmation" />
                    </label>
                    <div className="input-group">
                      <input
                        type="password"
                        name="password_confirmation"
                        id="password_confirmation"
                        className={`form-control ${
                          valid_password_confirmation
                            ? !valid_password_confirmation.isInvalid
                              ? 'is-valid'
                              : 'is-invalid'
                            : ''
                        }`}
                        placeholder={intl.formatMessage({
                          id: 'signUp.pwdConfirm.placeholder',
                          defaultMessage: 'Confirm password',
                        })}
                        onChange={handleChange}
                      />
                      {valid_password_confirmation ? (
                        valid_password_confirmation.message !== '' ? (
                          <div className="invalid-feedback">
                            <FormattedMessage
                              id={valid_password_confirmation.message}
                              defaultMessage="Value is not valid"
                            />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  {error !== '' && <Alert type="danger" message={errorMessage} />}

                  <div className="form-check">
                    <div className="input-group">
                      <input
                        className="form-check-input"
                        id="mail_newsletter"
                        name="mail_newsletter"
                        onChange={handleChange}
                        type="checkbox"
                        checked={mail_newsletter}
                      />
                      <label className="form-check-label" htmlFor="mail_newsletter">
                        <FormattedMessage
                          id="signUp.mail_newsletter"
                          defaultMessage="Do you want to subscribe to the JOGL monthly newsletter?"
                        />
                      </label>
                    </div>
                  </div>

                  <div className="form-check">
                    <div className="input-group">
                      <input
                        className="form-check-input"
                        id="mail_weekly"
                        name="mail_weekly"
                        onChange={handleChange}
                        type="checkbox"
                        checked={mail_weekly}
                      />
                      <label className="form-check-label" htmlFor="mail_weekly">
                        <FormattedMessage
                          id="signUp.mail_weekly"
                          defaultMessage="Do you want to receive weekly updates of JOGL items you follow?"
                        />
                      </label>
                    </div>
                  </div>

                  <div className="form-check accept">
                    <div className="input-group">
                      <input
                        className={`form-check-input ${
                          valid_acceptConditions ? (!valid_acceptConditions.isInvalid ? '' : 'is-invalid') : ''
                        }`}
                        id="acceptConditions"
                        name="acceptConditions"
                        onChange={handleChange}
                        type="checkbox"
                        ischecked={acceptConditions.toString()}
                      />
                      <label className="form-check-label d-inline" htmlFor="acceptConditions">
                        <FormattedMessage
                          id="signUp.termsOfService"
                          defaultMessage={
                            'By creating your account, you accept our {termsOfService}, {dataPolicy} and our {ethicsPledge}*'
                          }
                          values={{
                            termsOfService: (
                              <a href="/terms" target="_blank" className="nav-link">
                                <FormattedMessage
                                  id="singUp.termsOfServiceLinkText"
                                  defaultMessage="terms of services"
                                />
                              </a>
                            ),
                            dataPolicy: (
                              <a href="/data" target="_blank" className="nav-link">
                                <FormattedMessage id="footer.data" defaultMessage="User data Policy" />
                              </a>
                            ),
                            ethicsPledge: (
                              <a href="/ethics-pledge" target="_blank" className="nav-link">
                                <FormattedMessage id="signUp.ethicsPledgeTextLink" defaultMessage="Ethics pledge" />
                              </a>
                            ),
                          }}
                        />
                      </label>
                      {valid_acceptConditions ? (
                        valid_acceptConditions.message !== '' ? (
                          <div className="invalid-feedback text-left">
                            <FormattedMessage id={valid_acceptConditions.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-check accept">
                    <div className="input-group">
                      <input
                        className={`form-check-input ${
                          valid_acceptLegal ? (!valid_acceptLegal.isInvalid ? '' : 'is-invalid') : ''
                        }`}
                        id="acceptLegal"
                        name="acceptLegal"
                        onChange={handleChange}
                        type="checkbox"
                        ischecked={acceptLegal.toString()}
                      />
                      <label className="form-check-label" htmlFor="acceptLegal">
                        <span>
                          <FormattedMessage id="signUp.legalConf" defaultMessage="I am legally major in my country" />
                        </span>
                      </label>
                      {valid_acceptLegal ? (
                        valid_acceptLegal.message !== '' ? (
                          <div className="invalid-feedback text-left">
                            <FormattedMessage id={valid_acceptLegal.message} defaultMessage="Value is not valid" />
                          </div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <button type="submit" className="btn btn-primary btn-block" disabled={sending}>
                    {sending && (
                      <>
                        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
                        <span className="sr-only">
                          {intl.formatMessage({ id: 'general.loading', defaultMessage: 'Loading...' })}
                        </span>
                        &nbsp;
                      </>
                    )}
                    <FormattedMessage id="signUp.btnCreate" defaultMessage="Create my JoGL account" />
                  </button>
                </form>
              </div>
              <div className="form-message" style={messageStyle}>
                <img src="/images/envelope.svg" alt="Message sent envelope" />
              </div>
            </div>
          </div>
        </div>
      </Box>
    </Layout>
  );
};

export default SignUp;
