import ReactGA from 'react-ga';
import Router from 'next/router';
import { FormattedMessage, useIntl } from 'react-intl';
import { NextPage } from 'next';
import { useContext, useState } from 'react';
import CommunityForm from '~/components/Community/CommunityForm';
import Layout from '~/components/Layout';
import { UserContext } from '~/contexts/UserProvider';
import { useApi } from '~/contexts/apiContext';

const CreatePage: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const [newCommunity, setNewCommunity] = useState({
    creator_id: Number(userContext.credentials.userId),
    interests: [],
    short_description: '',
    short_title: '',
    skills: [],
    resources: [],
    title: '',
    is_private: false,
  });
  const [sending, setSending] = useState(false);
  const { formatMessage } = useIntl();

  const handleChange = (key, content) => {
    setNewCommunity((prevState) => ({ ...prevState, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/communities/', { community: newCommunity })
      .then((res) => {
        setSending(false);
        const userId = res.config.headers.userId;
        // record event to Google Analytics
        ReactGA.event({ category: 'Group', action: 'create', label: `[${userId},${res.data.id}]` /* group id */ });
        // follow the object after having created it
        api.put(`/api/communities/${res.data.id}/follow`);
        // go to created group edition page
        Router.push(`/community/${res.data.id}/edit`);
      })
      .catch((err) => {
        console.error("Couldn't post new community", err);
        setSending(false);
        // if error response data is that shortTitle is already taken, show message in an alert warning
        if (err.response.data.data === 'ShortTitle is already taken') {
          alert(formatMessage({ id: 'err-4006', defaultMessage: 'Short title is already taken' }));
        }
      });
  };
  return (
    <Layout title={`${formatMessage({ id: 'community.create.title', defaultMessage: 'Create a new group' })} | JOGL`}>
      <div className="communityCreate container-fluid">
        <h1>
          <FormattedMessage id="community.create.title" defaultMessage="Create a new group" />
        </h1>
        <CommunityForm
          community={newCommunity}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          mode="create"
          sending={sending}
        />
      </div>
    </Layout>
  );
};
export default CreatePage;
