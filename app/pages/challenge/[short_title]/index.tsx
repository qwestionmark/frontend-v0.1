/* eslint-disable camelcase */
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { FormattedMessage, useIntl, FormattedDate } from 'react-intl';
import { flexbox, layout, space } from 'styled-system';
import Box from '~/components/Box';
import Feed from '~/components/Feed/Feed';
import PostDisplayMinimal from '~/components/Feed/Posts/PostDisplayMinimal';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import A from '~/components/primitives/A';
import Button from '~/components/primitives/Button';
import H1 from '~/components/primitives/H1';
import H2 from '~/components/primitives/H2';
import P from '~/components/primitives/P';
import ChallengeHome from '~/components/Challenge/ChallengeHome';
import ChallengeMembers from '~/components/Challenge/ChallengeMembers';
import ChallengeNeeds from '~/components/Challenge/ChallengeNeeds';
import ChallengeProjects from '~/components/Challenge/ChallengeProjects';
import BtnFollow from '~/components/Tools/BtnFollow';
import BtnSave from '~/components/Tools/BtnSave';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import Loading from '~/components/Tools/Loading';
import NoResults from '~/components/Tools/NoResults';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import { useScrollHandler } from '~/utils/utils';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
// import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import ChallengeHeader from '~/components/Challenge/ChallengeHeader';
import InviteMember from '~/components/Tools/InviteMember';
import ChallengeFaq from '~/components/Challenge/ChallengeFaq';
import Chips from '~/components/Chip/Chips';
// import Image from 'next/image';
import Image2 from '~/components/Image2';
import DocumentsList from '~/components/Tools/Documents/DocumentsList';
import { Challenge } from '~/types';
import 'twin.macro';

const DEFAULT_TAB = 'home';
const ChallengeDetails: NextPage<{ challenge: Challenge }> = ({ challenge }) => {
  const router = useRouter();
  const { locale, formatMessage } = useIntl();
  // const { showModal, setIsOpen } = useModal();
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [offSetTop, setOffSetTop] = useState();
  const [tabs, setTabs] = useState([
    { value: 'home', intlId: 'program.home', defaultMessage: 'Home' },
    { value: 'about', intlId: 'general.tab.about', defaultMessage: 'About' },
    { value: 'news', intlId: 'entity.tab.news', defaultMessage: 'News' },
    { value: 'projects', intlId: 'entity.tab.projects', defaultMessage: 'Projects' },
    { value: 'needs', intlId: 'entity.tab.needs', defaultMessage: 'Needs' },
    { value: 'members', intlId: 'entity.tab.participants', defaultMessage: 'Participants' },
  ]);
  const { data: dataPosts, revalidate: revalidatePost } = useGet(`/api/feeds/${challenge?.feed_id}?items=5&page=1`);
  const { data: dataExternalLink } = useGet(`/api/challenges/${challenge?.id}/links`);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/challenges/${challenge.id}/faq`);
  const { userData } = useUserData();
  const theme = useTheme();
  const headerRef = useRef();
  const navRef = useRef();
  const isSticky = useScrollHandler(offSetTop);

  useEffect(() => {
    // on first load, check if url has a tab param
    if (router.query.tab) {
      // if it does, select this tab
      setSelectedTab(router.query.tab as string);
    }
  }, []);

  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  const getDaysLeft = (endDate) => {
    const now = new Date();
    const end = new Date(endDate);
    const daysLeft = (end - now) / 1000 / 60 / 60 / 24;
    return Math.ceil(daysLeft <= 0 ? 0 : daysLeft);
  };

  // add faq tab only if has faq
  useEffect(() => {
    tabs[6]?.value !== 'faq' &&
      faqList &&
      faqList.documents.length !== 0 &&
      setTabs([...tabs, { value: 'faq', intlId: 'entity.info.faq', defaultMessage: 'FAQ' }]); // don't add another time if tab already exists
  }, [faqList]);
  const contactEmail = challenge?.contact_email || 'hello@jogl.io';

  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'home') {
        router.push(`/challenge/${router.query.short_title}`, undefined, { shallow: true });
      }
      setSelectedTab(router.query.tab as string);
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 120 : 145; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  // const ContactButton = styled(FontAwesomeIcon)`
  //   color: ${(p) => p.theme.colors.greys['700']};
  //   :hover {
  //     cursor: pointer;
  //     color: ${(p) => p.theme.colors.primary};
  //   }
  // `;

  const BannerContainer = styled(Box)`
    ${[flexbox, layout, space]};
    img {
      object-fit: cover;
      width: 100%;
      height: 390px;
      @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
        height: 100%;
      }
    }
  `;

  const StickyHeading = styled(Box)`
    position: fixed;
    height: 70px;
    top: ${() => (isSticky ? '64px' : '-1000px')};
    width: 100%;
    z-index: 9;
    border-top: 1px solid grey;
    left: 0;
    transition: top 333ms;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
    @media (max-width: ${(p) => p.theme.breakpoints.md}) {
      height: 60px;
      .actions {
        display: none;
      }
      img {
        width: 40px;
        height: 40px;
      }
    }
    > div {
      max-width: 1280px;
      margin: 0 auto;
      width: 100%;
    }
  `;

  const bannerUrl = challenge?.banner_url || '/images/default/default-challenge.jpg';

  return (
    <Layout
      title={challenge?.title && `${challenge.title} | JOGL`}
      desc={challenge?.short_description}
      img={bannerUrl}
      className="small-top-margin"
    >
      <BannerContainer width="100%" height={['100%', undefined, undefined, undefined, '390px']}>
        <Image2 src={bannerUrl} unsized />
      </BannerContainer>
      <Box ref={headerRef} />
      <ChallengeHeader challenge={challenge} lang={locale} />
      <StickyHeading className="stickyHeading" bg="white" justifyContent="center">
        <Box row alignItems="center" px={[4, undefined, undefined, undefined, 0]}>
          {/* <LogoImg src={challenge?.logo_url || 'images/jogl-logo.png'} mr={4} alt="challenge logo" /> */}
          <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px">
            {locale === 'fr' && challenge?.title_fr ? challenge?.title_fr : challenge?.title}
          </H1>
          <Box row pl={5} className="actions">
            {userData && (
              <>
                <Box pr={5}>
                  <BtnSave itemType="challenges" itemId={challenge.id} saveState={challenge.has_saved} />
                </Box>
                <BtnFollow followState={challenge?.has_followed} itemType="challenges" itemId={challenge.id} />
              </>
            )}
            <ShareBtns type="challenge" />
          </Box>
        </Box>
      </StickyHeading>
      <Grid
        display="grid"
        mt={[5, undefined, 8]}
        gridTemplateColumns={['100%', undefined, '13rem calc(100% - 13rem)', undefined, '18% 57% 25%']}
        bg={[theme.colors.lightBlue, undefined, 'white']}
        ref={navRef}
      >
        {/* Header and nav (left col on desktop, top col on mobile */}
        <Box
          alignItems={[undefined, undefined, 'center']}
          bg="white"
          position="sticky"
          top={['123px', undefined, '133px']}
          pt="13px"
          zIndex={8}
        >
          <NavContainer bg="white">
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="left" />
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="right" />
            <Nav
              as="nav"
              flexDirection={['row', undefined, 'column']}
              spaceX={[3, undefined, 0]}
              width="100%"
              bg="white"
            >
              {tabs.map((item, index) => (
                <Link
                  shallow
                  scroll={false}
                  key={index}
                  href={`/challenge/${router.query.short_title}${
                    item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`
                  }`}
                >
                  <Tab selected={item.value === selectedTab} px={[1, undefined, 3]} py={3} as="button">
                    <FormattedMessage id={item.intlId} defaultMessage={item.defaultMessage} />
                  </Tab>
                </Link>
              ))}
            </Nav>
          </NavContainer>
        </Box>

        {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
        <Box px={[0, undefined, 4]}>
          {selectedTab === 'home' && (
            <TabPanel id="home" px={[3, 4, undefined, 0]} pt={[5, undefined, 0]}>
              <DesktopBorders>
                <ChallengeHome
                  shortDescription={
                    locale === 'fr' && challenge?.short_description_fr
                      ? challenge?.short_description_fr
                      : challenge?.short_description
                  }
                  challengeId={challenge.id}
                  challengeFeedId={challenge.feed_id}
                  isAdmin={challenge.is_admin}
                  posts={dataPosts}
                  refreshPost={revalidatePost}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'news' && (
            <TabPanel id="news" style={{ margin: '0 auto' }} width="100%" pt={[2, undefined, 0]}>
              {
                //  Show feed, and pass DisplayCreate to admins
                challenge.feed_id && (
                  <Feed
                    feedId={challenge.feed_id}
                    displayCreate={challenge.is_admin || challenge.is_owner}
                    isAdmin={challenge.is_admin || challenge.is_owner}
                  />
                )
              }
            </TabPanel>
          )}
          {selectedTab === 'projects' && (
            <TabPanel id="projects" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ChallengeProjects challengeId={challenge.id} />
            </TabPanel>
          )}
          {selectedTab === 'needs' && (
            <TabPanel id="needs" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ChallengeNeeds challengeId={challenge.id} />
            </TabPanel>
          )}
          {selectedTab === 'members' && (
            <TabPanel id="members" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]} position="relative">
              <ChallengeMembers challengeId={challenge.id} />
            </TabPanel>
          )}

          {selectedTab === 'about' && (
            <TabPanel id="about" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <InfoHtmlComponent
                  title=""
                  content={
                    locale === 'fr' && challenge?.description_fr ? challenge?.description_fr : challenge?.description
                  }
                />
                <Box pt={4}>
                  <InfoInterestsComponent
                    title={formatMessage({ id: 'challenge.info.interests', defaultMessage: 'SDG(s) to tackle' })}
                    content={challenge?.interests}
                  />
                </Box>
                {challenge?.skills.length !== 0 && (
                  <Box>
                    <Box>{formatMessage({ id: 'entity.info.skills', defaultMessage: 'Skills' })}</Box>
                    <Chips
                      data={challenge.skills.map((skill) => ({
                        title: skill,
                        href: `/search/challenges/?refinementList[skills][0]=${skill}`,
                      }))}
                      // color={theme.colors.primary}
                      color="#F2F4F8"
                    />
                  </Box>
                )}

                {/* display challenge dates info, and status */}
                <Box pt={5}>
                  {challenge?.launch_date && (
                    <>
                      <strong>
                        <FormattedMessage id="entity.info.launch_date" defaultMessage="Launch" />:{' '}
                      </strong>
                      <FormattedDate value={challenge.launch_date} year="numeric" month="long" day="2-digit" />
                    </>
                  )}
                  {challenge?.final_date && (
                    <>
                      <strong>
                        <FormattedMessage id="entity.info.final_date" defaultMessage="Final Submissions" />:{' '}
                      </strong>
                      <FormattedDate value={challenge.final_date} year="numeric" month="long" day="2-digit" />
                    </>
                  )}
                  {challenge?.end_date && (
                    <>
                      <strong>
                        <FormattedMessage id="entity.info.end_date" defaultMessage="Winner Announced" />:{' '}
                      </strong>
                      <FormattedDate value={challenge.end_date} year="numeric" month="long" day="2-digit" />
                    </>
                  )}
                  {getDaysLeft(challenge?.final_date) > 0 && (
                    <Box pt={3}>
                      {getDaysLeft(challenge?.final_date)}
                      &nbsp;
                      <FormattedMessage id="program.due.days" defaultMessage="days" />
                      {getDaysLeft(challenge?.final_date) > 1 ? 's' : ''}
                      &nbsp;
                      <FormattedMessage id="program.due.left" defaultMessage="left" />
                    </Box>
                  )}
                </Box>
                <Box pt={8}>
                  <DocumentsList
                    documents={challenge?.documents}
                    itemId={challenge?.id}
                    isAdmin={challenge?.is_admin}
                    itemType="challenges"
                    cardType="cards"
                  />
                </Box>
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'faq' && (
            <TabPanel id="faq" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ChallengeFaq faqList={faqList} />
              </DesktopBorders>
            </TabPanel>
          )}
          <Box pt={11} pb={7} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <CtaAndLinks
              router={router}
              contactEmail={contactEmail}
              dataExternalLink={dataExternalLink}
              userData={userData}
              challengeId={challenge.id}
              formatMessage={formatMessage}
              isAdmin={challenge.is_admin}
            />
          </Box>
        </Box>

        {/* Aside right col, only on desktop */}
        <Box as="aside" display={['none', undefined, undefined, undefined, 'block']} spaceY={4} bg="white">
          {/* Create account CTA */}
          {!userData && ( // if user is not connected
            <Box borderRadius="1rem" alignItems="center" py={4}>
              <A href="/signup">
                <Button>
                  {formatMessage({ id: 'program.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
                </Button>
              </A>
              <P pt={2} mb={0}>
                {formatMessage({
                  id: 'program.rightCompo.createAccount.text2',
                  defaultMessage: 'to participate to the challenge',
                })}
              </P>
            </Box>
          )}
          {/* Latest announcements */}
          {selectedTab !== 'faq' && ( // show on all tabs except faq
            <Box>
              <Box borderBottom="1px solid #d3d3d3" py={2}>
                <H2 mb={0}>
                  {formatMessage({
                    id: 'program.rightCompo.latestsAnnouncements',
                    defaultMessage: 'Latest announcements',
                  })}
                </H2>
              </Box>
              {!dataPosts ? (
                <Loading />
              ) : dataPosts?.length === 0 ? (
                <NoResults type="post" />
              ) : (
                <>
                  <Box>
                    {[...dataPosts].splice(0, 3).map((post, i) => (
                      <PostDisplayMinimal post={post} key={i} user={userData} refresh={revalidatePost} />
                    ))}
                  </Box>
                  <Box row p={2} justifyContent="center">
                    <A href={`/challenge/${router.query.short_title}?tab=news`} shallow scroll={false}>
                      <Button>{formatMessage({ id: 'program.seeAll', defaultMessage: 'See all' })}</Button>
                    </A>
                  </Box>
                </>
              )}
            </Box>
          )}
          {/* Contact challenge leaders bloc */}
          {/* {selectedTab === 'faq' && members && (
            <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
              <H2>{formatMessage({ id: 'footer.contactUs', defaultMessage: 'Contact us' })}</H2>
              <Box pt={5} spaceY={4}>
                {members
                  .filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                  .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                    <Box key={index} row spaceX={4} alignItems="center">
                      <Link href={`/user/${id}`}>
                        <a>
                          <Box row spaceX={4} alignItems="center">
                            <img
                              width="50px"
                              height="50px"
                              style={{ borderRadius: '50%', objectFit: 'cover' }}
                              src={logo_url_sm}
                              alt={`${first_name} ${last_name}`}
                            />
                            <P fontWeight="bold" fontSize="1.2rem" mb={0}>
                              {first_name + ' ' + last_name}
                            </P>
                          </Box>
                        </a>
                      </Link>
                      <Box>
                        <ContactButton
                          icon="envelope"
                          size="lg"
                          onClick={() => {
                            showModal({
                              children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
                              title: 'Send message to {userFullName}',
                              titleId: 'user.contactModal.title',
                              values: { userFullName: first_name + ' ' + last_name },
                            });
                          }}
                        />
                      </Box>
                    </Box>
                  ))}
              </Box>
            </Box>
          )} */}
          {/* Cta and links section (+ add style to make whole section sticky on top when attaining it */}
          <div tw="hidden flex-col xl:flex xl:sticky xl:top-36">
            {/* Create account CTA */}
            <CtaAndLinks
              dataExternalLink={dataExternalLink}
              userData={userData}
              formatMessage={formatMessage}
              challengeId={challenge.id}
              isAdmin={challenge.is_admin}
            />
          </div>
        </Box>
      </Grid>
    </Layout>
  );
};

const CtaAndLinks = ({ dataExternalLink, userData, formatMessage, challengeId, isAdmin }) => {
  const ExternalLinkIcon = styled(Box)`
    img:hover {
      opacity: 0.8;
    }
  `;
  return (
    <>
      {!userData && ( // if user is not connected
        <Box row alignItems={['start', undefined, undefined, undefined, 'center']} pt={4} pb={8} flexWrap="wrap">
          <A href="/signup">
            <Button>
              {formatMessage({ id: 'program.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
            </Button>
          </A>
          <P pt={2} pl={2} mb={0}>
            {formatMessage({
              id: 'program.rightCompo.createAccount.text',
              defaultMessage: 'to participate to the challenge',
            })}
          </P>
        </Box>
      )}
      {/* Invite someone component */}
      {isAdmin && (
        <Box pt={5}>
          <H2>{formatMessage({ id: 'member.invite.general', defaultMessage: 'Invite someone to join!' })}</H2>
          <InviteMember itemType="challenges" itemId={challengeId} />
        </Box>
      )}
      {/* <H2>{formatMessage({ id: 'program.rightCompo.questions', defaultMessage: 'Questions?' })}</H2>
      <Box row justifyContent={['flex-start', undefined, undefined, undefined, 'space-between']} spaceX={3}>
        <QuestionBox>
          <A href={`/challenge/${router.query.short_title}?tab=faq`} shallow>
            <FontAwesomeIcon icon="question-circle" />
            <P>{formatMessage({ id: 'faq.title', defaultMessage: 'FAQs' })}</P>
          </A>
        </QuestionBox>
        <QuestionBox>
          <a href={`mailto:${contactEmail}`}>
            <FontAwesomeIcon icon="envelope" />
            <P>{formatMessage({ id: 'user.btn.contact', defaultMessage: 'Contact' })}</P>
          </a>
        </QuestionBox>
      </Box> */}
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <Box pt={8}>
          <H2 pb={4}>{formatMessage({ id: 'general.externalLink.findUs', defaultMessage: 'Find us' })}</H2>
          <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(55px, 1fr))">
            {[...dataExternalLink].map((link, i) => (
              <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                <a href={link.url} target="_blank">
                  <img width="45px" src={link.icon_url} />
                </a>
              </ExternalLinkIcon>
            ))}
          </Grid>
        </Box>
      )}
    </>
  );
};

const Tab = styled.a`
  display: inline-block;
  border-bottom: 3px solid transparent;
  border-bottom-color: ${(p) => p.selected && p.theme.colors.primary};
  font-weight: ${(p) => p.selected && 700};
  white-space: nowrap;
  color: inherit;
  text-align: right;
  cursor: pointer;
  &:hover {
    border-bottom-color: ${(p) => p.theme.colors.primary};
    color: inherit;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid transparent;
    &:hover {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
  }
  ${[space, layout]};
`;

const TabPanel = styled.div`
  ${[layout, space]};
  .infoHtml {
    width: 100% !important;
  }
`;

const NavContainer = styled(Box)`
  position: relative;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid ${(p) => p.theme.colors.greys['200']};
  }
  /* To make element sticky on top when reach top of page */
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    position: sticky;
    position: -webkit-sticky;
    top: 150px;
  }

  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    width: 100%;
    justify-content: flex-end;
    padding-right: 20px;
  }
`;
const Nav = styled(Box)`
  overflow-x: scroll;
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    font-size: ${(p) => p.theme.fontSizes.xl};
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding-left: 1.8rem;
    button:last-child {
      padding-right: 3rem;
    }
    button {
      padding: 0 3px 5px;
    }
  }
`;

const OverflowGradient = styled.div`
  ${layout};
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

// const QuestionBox = styled(Box)`
//   border: 2px solid ${(p) => p.theme.colors.greys['800']};
//   height: 6rem;
//   width: 6rem;
//   border-radius: 1rem;
//   align-items: center;
//   justify-content: center;
//   background: white;
//   cursor: pointer;
//   svg {
//     font-size: 2.3rem;
//     color: ${(p) => p.theme.colors.greys['800']};
//     margin-right: 0;
//   }
//   p {
//     margin: 10px 0 0;
//     font-size: 100%;
//     font-weight: bold;
//   }
//   a {
//     width: 100%;
//     text-align: center;
//   }
//   &:hover {
//     p,
//     svg {
//       color: ${(p) => p.theme.colors.primary};
//     }
//     border: 2px solid ${(p) => p.theme.colors.primary};
//     a {
//       text-decoration: none !important;
//     }
//   }
// `;

// for desktop tabs that need an englobing div with padding and border
const DesktopBorders = ({ children }) => (
  <Box
    borderLeft={['none', undefined, '2px solid #f4f4f4']}
    borderRight={['none', undefined, undefined, undefined, '2px solid #f4f4f4']}
    pl={[0, undefined, 4]}
    pr={[0, undefined, undefined, undefined, 4]}
  >
    {children}
  </Box>
);

const getChallenge = async (api, challengeId) => {
  const res = await api.get(`/api/challenges/${challengeId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

ChallengeDetails.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/challenges/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const challenge = await getChallenge(api, res.data.id).catch((err) => console.error(err));
      return { challenge };
    }
    isomorphicRedirect(ctx, '/');
  }
  // Case short_title is actually an id
  const challenge = await getChallenge(api, query.short_title).catch((err) => console.error(err));
  if (challenge) {
    return { challenge };
  } else {
    isomorphicRedirect(ctx, '/');
  }
};

export default ChallengeDetails;
