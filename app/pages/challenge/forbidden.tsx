import Link from 'next/link';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { NextPage } from 'next';
import Layout from '~/components/Layout';
// import "assets/css/ChallengePage.scss";

const ChallengeForbiddenPage: NextPage = () => {
  return (
    <Layout>
      <div className="container-fluid ChallengePage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            <FormattedMessage
              id="challenge.info.forbidden"
              defaultMessage="Please contact the JOGL team to obtain rights to create a challenge"
            />
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <Link href="/search/challenges">
              <a>
                <button className="btn btn-primary" type="button">
                  <FormattedMessage id="challenge.info.forbiddenBtn" defaultMessage="Return to challenges" />
                </button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default ChallengeForbiddenPage;
