// TODO: Refactor to functional component.
import { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useRouter } from 'next/router';
import Layout from '~/components/Layout';
import Loading from '~/components/Tools/Loading';
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import Select, { createFilter } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import ReactGA from 'react-ga';
import UserCard from '~/components/User/UserCard';
import Box from '~/components/Box';
import { useApi } from '~/contexts/apiContext';
import useUserData from '~/hooks/useUserData';

const CountriesCitiesData = require('public/data/countries-cities');
// import "./CompleteProfile.scss";
const CompleteProfile = () => {
  const [sending, setSending] = useState(false);
  const [suggestedUser, setSuggestedUser] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const { formatMessage } = useIntl();
  const api = useApi();
  const router = useRouter();
  const { userData } = useUserData();
  const suggestedUserId = process.env.NODE_ENV !== 'production' ? 1 : 191;
  // if env is dev, suggestuserId is 1, else if it's prod, make it 191 which is the JOGL user on app.jogl.io

  useEffect(() => {
    api
      .get(`/api/users/${suggestedUserId}`) // get jogl user
      .then((res) => {
        setSuggestedUser(res.data);
      })
      .catch((err) => {
        console.error(`Couldn't GET user id=${suggestedUserId}`, err);
      });
  }, []);
  useEffect(() => {
    setUser(userData);
  }, [userData]);

  const handleChange = (key, content) => {
    var newKey,
      newContent = '';
    if (content.name === 'category') {
      newKey = content.name;
      newContent = key.value;
    } else if (content.action === 'set-value' || content.action === 'select-option') {
      newKey = ['country'];
      newContent = key.value;
    } else if (content.action === 'clear') {
      newKey = ['country'];
      newContent = '';
    } else {
      newKey = key;
      newContent = content;
    }
    setUser((prevUser) => ({ ...prevUser, [newKey]: newContent }));
  };

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    setSending(true);
    api
      .patch(`/api/users/${user.id}`, { user })
      .then(() => {
        setSending(false);
        // send event to google analytics
        ReactGA.event({ category: 'User', action: 'completed profile', label: `${user.id}` });
        router.push(`/user/${user.id}/edit`);
      })
      .catch((err) => {
        console.error(`Couldn't PATCH user id=${user.id}`, err);
      });
  };

  const countriesList = Object.keys(CountriesCitiesData.list) // an array of all the countries from the list
    .sort((a, b) => a.localeCompare(b))
    .map((content) => {
      return { value: content, label: content };
    });

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };

  const disabledBtns =
    user?.interests.length === 0 ||
    !user?.short_bio ||
    !user?.category ||
    !user?.affiliation ||
    user?.skills.length === 0 ||
    !user?.country;

  return (
    <Layout
      noHeaderFooter
      title={formatMessage({ id: 'completeProfile.title', defaultMessage: 'Welcome aboard | JOGL' })}
    >
      <div className="container-fluid CompleteProfile">
        <div className="align-items-center content">
          <div className="form-content">
            <div className="form-header text-center">
              <h2 className="form-title" id="signModalLabel">
                <FormattedMessage id="completeProfile.title" defaultMessage="Welcome aboard" />
              </h2>
              <p>
                <FormattedMessage
                  id="completeProfile.description"
                  defaultMessage="First, a few ice-breakers questions."
                />
              </p>
            </div>
            <div className="form-body">
              <Loading active={!user} height="200px">
                <form onSubmit={handleSubmit}>
                  <FormDefaultComponent
                    id="short_bio"
                    title={formatMessage({ id: 'user.profile.bioShort', defaultMessage: 'Short bio' })}
                    placeholder={formatMessage({
                      id: 'user.profile.bioShort.placeholder',
                      defaultMessage: 'Describe yourself in a few words',
                    })}
                    content={user?.short_bio}
                    onChange={handleChange}
                  />
                  <FormDropdownComponent
                    id="category"
                    title={formatMessage({ id: 'user.profile.category', defaultMessage: 'Category' })}
                    content={user?.category}
                    // prettier-ignore
                    options={['fulltime_worker', "parttime_worker", "fulltime_student", "parttime_student",
                  "freelance", "between_jobs", "retired", "student_looking_internship"]}
                    onChange={handleChange}
                  />
                  <FormDefaultComponent
                    id="affiliation"
                    title={formatMessage({ id: 'user.profile.affiliation', defaultMessage: 'Affiliation' })}
                    placeholder={formatMessage({
                      id: 'user.profile.affiliation.placeholder',
                      defaultMessage: 'Your company, startup, association, NGO...',
                    })}
                    content={user?.affiliation}
                    onChange={handleChange}
                  />
                  <Select
                    name="country"
                    id="country"
                    defaultValue={user?.country && { label: user?.country, value: user?.country }}
                    options={countriesList}
                    placeholder={formatMessage({
                      id: 'general.country.placeholder',
                      defaultMessage: 'Select a country',
                    })}
                    filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                    components={{ MenuList }}
                    noOptionsMessage={() => null}
                    onChange={handleChange}
                    // onChange={handleCountryChange}
                    styles={countriesSelectStyles}
                    isClearable
                  />
                  <FormSkillsComponent
                    id="skills"
                    type="user"
                    title={formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
                    placeholder={formatMessage({
                      id: 'general.skills.placeholder',
                      defaultMessage: 'Big data, Web Development, Open Science...',
                    })}
                    content={user?.skills}
                    onChange={handleChange}
                  />
                  <p>
                    <FormattedMessage
                      id="completeProfile.description2"
                      defaultMessage="Choose at least 1 on the 17 sustainable development goals defined by the United Nations. This will help us show you projects you’ll love."
                    />
                  </p>
                  <FormInterestsComponent
                    title={formatMessage({ id: 'user.profile.interests', defaultMessage: 'Interested in' })}
                    content={user?.interests}
                    onChange={handleChange}
                  />
                  <Box width={['100%', '30rem']} margin="auto" textAlign="center" mt={3} mb={8}>
                    <p>
                      {formatMessage({
                        id: 'completeProfile.suggestFollow',
                        defaultMessage: 'You might want to follow',
                      })}
                    </p>
                    {suggestedUser && (
                      <UserCard
                        id={suggestedUser.id}
                        firstName={suggestedUser.first_name}
                        lastName={suggestedUser.last_name}
                        nickName={suggestedUser.nickname}
                        shortBio={suggestedUser.short_bio}
                        logoUrl={suggestedUser.logo_url}
                        hasFollowed={suggestedUser.has_followed}
                        canContact={false}
                        noMobileBorder={false}
                      />
                    )}
                  </Box>
                  <div className="buttonsContainer">
                    <button type="submit" className="btn btn-primary next" disabled={disabledBtns || sending}>
                      {sending && (
                        <>
                          <span
                            className="spinner-border spinner-border-sm text-center"
                            role="status"
                            aria-hidden="true"
                          />
                          &nbsp;
                        </>
                      )}
                      <FormattedMessage id="entity.form.btnNext" defaultMessage="Next" />
                    </button>
                  </div>
                </form>
              </Loading>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default CompleteProfile;
