import { useState, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import $ from 'jquery';
import { useRouter } from 'next/router';
import { getApiFromCtx } from '~/utils/getApi';
import Feed from '~/components/Feed/Feed';
import Loading from '~/components/Tools/Loading';
import CommunityList from '~/components/Community/CommunityList';
import InfoAddressComponent from '~/components/Tools/Info/InfoAddressComponent';
import InfoDefaultComponent from '~/components/Tools/Info/InfoDefaultComponent';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import ProjectList from '~/components/Project/ProjectList';
import UserHeader from '~/components/User/UserHeader';
import Alert from '~/components/Tools/Alert';
import useUserData from '~/hooks/useUserData';
import Layout from '~/components/Layout';
import { linkify, scrollToActiveTab, stickyTabNav } from '~/utils/utils';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import { textWithPlural } from '~/utils/managePlurals';
import TitleInfo from '~/components/Tools/TitleInfo';
import Grid from '~/components/Grid';
import styled from '~/utils/styled';
import useGet from '~/hooks/useGet';
import Chips from '~/components/Chip/Chips';
// import { useTheme } from '@emotion/react';
import UserCard from '~/components/User/UserCard';
import { useModal } from '~/contexts/modalContext';
import ChallengeCard from '~/components/Challenge/ChallengeCard';
import ProgramCard from '~/components/Program/ProgramCard';
// import "./UserProfile.scss";
export default function UserProfile({ user }) {
  const [listProjects, setListProjects] = useState();
  const [listCommunities, setListCommunities] = useState();
  const [listChallenges, setListChallenges] = useState();
  const [listPrograms, setListPrograms] = useState();
  useEffect(() => {
    // temp fix to redo those functions when you go from a user page to another
    if (user.id) {
      setListProjects(undefined);
      setListCommunities(undefined);
      setListChallenges(undefined);
      user.stats.projects_count !== 0 && loadProjects();
      user.stats.communities_count !== 0 && loadGroups();
      user.stats.challenges_count !== 0 && loadChallenges();
      user.stats.programs_count !== 0 && loadPrograms();
    }
  }, [user]);
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  const router = useRouter();
  const api = useApi();
  const modal = useModal();
  // const theme = useTheme();
  // const [mutualCounts, setMutualCounts] = useState();
  // useEffect(() => {
  //   userData &&
  //     api.get(`/api/users/${user.id}/mutual`).then((res) => {
  //       setMutualCounts(res.data);
  //     });
  // }, []);
  const { data: dataExternalLink } = useGet(`/api/users/${user?.id}/links`);
  useEffect(() => {
    const urlSuccessParam = router.query.success;
    setTimeout(() => {
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
      stickyTabNav('isEdit', router); // make the tab navigation bar sticky on top when we reach its scroll position
      if (urlSuccessParam === '1') {
        // if url success param is 1, show "saved changes" success alert
        $('#editSuccess').show(); // display success message
        setTimeout(() => {
          $('#editSuccess').hide(300);
        }, 2000); // hide it after 2sec
      }
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const loadProjects = () => {
    api.get(`/api/users/${user.id}/objects/projects`).then((res) => setListProjects(res.data));
    // .catch((err) => console.error(`Couldn't GET projects of user.id=${user.id}`, err));
  };
  const loadGroups = () => {
    api.get(`/api/users/${user.id}/objects/communities`).then((res) => setListCommunities(res.data));
  };
  const loadChallenges = () => {
    api.get(`/api/users/${user.id}/objects/challenges`).then((res) => setListChallenges(res.data));
  };
  const loadPrograms = () => {
    api.get(`/api/users/${user.id}/objects/programs`).then((res) => setListPrograms(res.data));
  };
  const showMutualConnections = () => {
    // get all user's mutual connections, then show modal displaying them
    api.get(`/api/users/${user?.id}/mutual`).then((res) => {
      modal.showModal({
        children: (
          <Grid gridGap={[2, 4]} gridCols={[1, 2]} display={['grid', 'inline-grid']} py={[0, 2, 4]}>
            {res.data.map((member, i) => (
              <UserCard
                key={i}
                id={member.id}
                firstName={member.first_name}
                lastName={member.last_name}
                nickName={member.nickname}
                shortBio={member.short_bio}
                logoUrl={member.logo_url}
                canContact={member.can_contact}
                hasFollowed={member.has_followed}
                mutualCount={member.stats.mutual_count}
              />
            ))}
          </Grid>
        ),
        maxWidth: '60rem',
        title: 'Mutual connections',
        titleId: 'user.info.mutualConnections',
      });
    });
  };

  const bioWithLinks = user.bio ? linkify(user.bio) : '';

  // make different tab active depending if user is member or not
  // TODO Pass active as prop to the styled component
  let newsTabClasses;
  let aboutTabClasses;
  let newsPaneClasses;
  let aboutPaneClasses;
  if (user && userData) {
    if (!user.has_followed && user.id !== userData.id) {
      newsTabClasses = 'nav-item nav-link';
      aboutTabClasses = 'nav-item nav-link active';
      newsPaneClasses = 'tab-pane';
      aboutPaneClasses = 'tab-pane active';
    } else {
      newsTabClasses = 'nav-item nav-link active';
      aboutTabClasses = 'nav-item nav-link';
      newsPaneClasses = 'tab-pane active';
      aboutPaneClasses = 'tab-pane';
    }
  } else {
    newsTabClasses = 'nav-item nav-link';
    aboutTabClasses = 'nav-item nav-link active';
    newsPaneClasses = 'tab-pane';
    aboutPaneClasses = 'tab-pane active';
  }
  return (
    <Layout title={`${user.first_name} ${user.last_name} | JOGL`} desc={user.short_bio || user.bio} img={user.logo_url}>
      <div className="userProfile">
        <Loading active={false}>
          <div className="userHeader justify-content-center container-fluid">
            <UserHeader user={user} />
          </div>
          <nav className="nav nav-tabs container-fluid">
            <a className={newsTabClasses} href="#news" data-toggle="tab">
              <FormattedMessage id="user.profile.tab.feed" defaultMessage="Feed" />
            </a>
            <a className={aboutTabClasses} href="#about" data-toggle="tab">
              <FormattedMessage id="general.tab.about" defaultMessage="About" />
            </a>
            {/* show collections tab only if user really has objects */}
            {(user.stats.projects_count !== 0 ||
              user.stats.communities_count !== 0 ||
              user.stats.challenges_count !== 0 ||
              user.stats.programs_count !== 0) && (
              <a
                className="nav-item nav-link"
                href="#collections"
                data-toggle="tab"
                onClick={() => loadProjects() + loadGroups() + loadChallenges() + loadPrograms()}
              >
                <FormattedMessage id="user.profile.tab.collections" defaultMessage="Collection" />
              </a>
            )}
            {/* <a className="nav-item nav-link" href="#saved" data-toggle="tab" onClick={() => loadSavedObjects()}>
              <FormattedMessage id="user.profile.tab.saved" defaultMessage="My saved objects" />
            </a> */}
          </nav>
          <div className="tabContainer">
            <div className="tab-content justify-content-center container-fluid">
              <div className={newsPaneClasses} id="news">
                {user.feed_id && (
                  <Feed
                    feedId={user.feed_id}
                    // show post creation box only to the user
                    displayCreate={userData && user.id === userData.id}
                    isAdmin={user.is_admin}
                  />
                )}
              </div>

              <div className={aboutPaneClasses} id="about">
                <Box width={['100%', undefined, undefined, '67%']} margin="auto">
                  <InfoDefaultComponent
                    title={formatMessage({ id: 'user.profile.bio', defaultMessage: 'Bio' })}
                    content={bioWithLinks}
                    containsHtml
                  />
                  <InfoDefaultComponent
                    title={formatMessage({ id: 'user.profile.affiliation', defaultMessage: 'Affiliation' })}
                    content={user.affiliation}
                  />
                  {user.category && user.category !== 'default' && (
                    <InfoDefaultComponent
                      title={formatMessage({ id: 'user.profile.category', defaultMessage: 'Category' })}
                      content={formatMessage({
                        id: `user.profile.edit.select.${user.category}`,
                        defaultMessage: user.category,
                      })}
                    />
                  )}
                  {user.skills.length !== 0 && (
                    <Box className="infoSkills" mb={4}>
                      <Box fontWeight="bold">
                        {formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
                      </Box>
                      <Chips
                        data={user.skills.map((skill) => ({
                          title: skill,
                          href: `/search/members/?refinementList[skills][0]=${skill}`,
                        }))}
                        // color={theme.colors.primary}
                        color="#F2F4F8"
                      />
                    </Box>
                  )}
                  {user.ressources.length !== 0 && (
                    <Box className="infoResources" mb={4}>
                      <Box fontWeight="bold">
                        {formatMessage({ id: 'user.profile.resources', defaultMessage: 'Resources' })}
                      </Box>
                      <Chips
                        data={user.ressources.map((resource) => ({
                          title: resource,
                          href: `/search/members/?refinementList[ressources][0]=${resource}`,
                        }))}
                        // color={theme.colors.pink}
                        // color={theme.colors.greys['300']}
                        color="#eff7ff"
                      />
                    </Box>
                  )}
                  <InfoInterestsComponent
                    title={formatMessage({ id: 'user.profile.interests', defaultMessage: 'Interests' })}
                    content={user.interests}
                  />
                  <InfoAddressComponent
                    title={formatMessage({ id: 'user.profile.address', defaultMessage: 'Address' })}
                    address={user.address}
                    city={user.city}
                    country={user.country}
                  />
                  {/* display projects counts and mutual connections (if you are connected and not the user */}
                  {userData?.id !== user.id && (user.stats.mutual_count > 0 || user.stats.projects_count > 0) && (
                    <Box pb={6} width="fit-content">
                      <TitleInfo
                        title={formatMessage({ id: 'user.info.other', defaultMessage: 'Other information' })}
                      />
                      {user.stats.projects_count > 0 && (
                        <Box pt={2}>{`${formatMessage({
                          id: 'user.info.currentlyOn',
                          defaultMessage: 'Currently on',
                        })} ${user.stats.projects_count} ${textWithPlural('project', user.stats.projects_count)}`}</Box>
                      )}
                      {user.stats.mutual_count > 0 && (
                        <FakeLink onClick={showMutualConnections} pt={2}>{`${user.stats.mutual_count} ${textWithPlural(
                          'mutualConnection',
                          user.stats.mutual_count
                        )}`}</FakeLink>
                      )}
                    </Box>
                  )}
                  {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <Box>
                      <TitleInfo
                        title={formatMessage({ id: 'general.externalLink.title', defaultMessage: 'External links' })}
                      />
                      <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(55px, 1fr))" mt={2}>
                        {[...dataExternalLink].map((link, i) => (
                          <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                            <a href={link.url} target="_blank">
                              <img width="45px" src={link.icon_url} />
                            </a>
                          </ExternalLinkIcon>
                        ))}
                      </Grid>
                    </Box>
                  )}
                </Box>
              </div>

              <div className="tab-pane" id="collections">
                {/* load/show only if user has projects */}
                {user.stats.projects_count !== 0 && (
                  <>
                    <h3>
                      <FormattedMessage id="user.profile.tab.projects" defaultMessage="Projects" />
                    </h3>
                    {/* don't show draft projects to other users */}
                    <ProjectList
                      listProjects={
                        userData && user.id === userData.id
                          ? listProjects
                          : listProjects?.filter(({ status }) => status !== 'draft')
                      }
                    />
                  </>
                )}
                {/* load/show only if user has groups */}
                {user.stats.communities_count !== 0 && (
                  <>
                    <div className="communityList" style={{ marginTop: '2.6rem' }}>
                      <h3>
                        <FormattedMessage id="user.profile.tab.communities" defaultMessage="Communities" />
                      </h3>
                      <CommunityList listCommunities={listCommunities} />
                    </div>
                  </>
                )}
                {/* load/show only if user has challenges */}
                {user.stats.challenges_count !== 0 && (
                  <>
                    <div className="communityList" style={{ marginTop: '2.6rem' }}>
                      <h3>
                        <FormattedMessage id="general.challenges" defaultMessage="Challenges" />
                      </h3>
                      <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} pt={3}>
                        {!listChallenges ? (
                          <Loading />
                        ) : (
                          listChallenges
                            ?.filter(({ status }) => status !== 'draft') // don't show draft challenges
                            .map((challenge, i) => (
                              <ChallengeCard
                                key={i}
                                id={challenge.id}
                                short_title={challenge.short_title}
                                title={challenge.title}
                                title_fr={challenge.title_fr}
                                short_description={challenge.short_description}
                                short_description_fr={challenge.short_description_fr}
                                membersCount={challenge.members_count}
                                needsCount={challenge.needs_count}
                                has_saved={challenge.has_saved}
                                clapsCount={challenge.claps_count}
                                status={challenge.status}
                                programId={challenge.program.id}
                                programShortTitle={challenge.program.short_title}
                                programTitle={challenge.program.title}
                                programTitleFr={challenge.program.title_fr}
                                projectsCount={challenge.projects_count}
                                banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                              />
                            ))
                        )}
                      </Grid>
                    </div>
                  </>
                )}
                {/* load/show only if user has programs */}
                {user.stats.programs_count !== 0 && (
                  <>
                    <div className="communityList" style={{ marginTop: '2.6rem' }}>
                      <h3>
                        <FormattedMessage id="general.programs" defaultMessage="Programs" />
                      </h3>
                      <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} pt={3}>
                        {!listPrograms ? (
                          <Loading />
                        ) : (
                          listPrograms
                            ?.filter(({ status }) => status !== 'draft') // don't show draft programs
                            .map((program, i) => (
                              <ProgramCard
                                key={i}
                                id={program.id}
                                short_title={program.short_title}
                                title={program.title}
                                title_fr={program.title_fr}
                                short_description={program.short_description}
                                short_description_fr={program.short_description_fr}
                                membersCount={program.members_count}
                                needsCount={program.needs_count}
                                has_saved={program.has_saved}
                                clapsCount={program.claps_count}
                                projectsCount={program.projects_count}
                                banner_url={program.banner_url || '/images/default/default-program.jpg'}
                              />
                            ))
                        )}
                      </Grid>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
          <Alert
            type="success"
            id="editSuccess"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        </Loading>
      </div>
    </Layout>
  );
}

const ExternalLinkIcon = styled(Box)`
  img:hover {
    opacity: 0.8;
  }
`;
const FakeLink = styled(Box)`
  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

UserProfile.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/users/${ctx.query.id}`)
    .catch((err) => console.error(`Couldn't GET user with id=${ctx.query.id}`, err));
  if (res) {
    return { user: res.data };
  }
  isomorphicRedirect(ctx, '/search/members');
  return {};
};
