import Router from 'next/router';
import React from 'react';
import Link from 'next/link';
import Button from '~/components/primitives/Button';
import 'twin.macro';
import { FormattedMessage } from 'react-intl';
function ErrorPage({ statusCode }) {
  return (
    <div tw="bg-gray-100 flex items-center px-5 py-16 lg:p-20 overflow-hidden relative justify-center h-screen">
      <div tw="flex-1 rounded-3xl bg-white shadow-xl p-20 text-gray-800 relative md:flex items-center text-center md:text-left max-w-4xl">
        <div tw="w-full">
          <div tw="mb-10 md:mb-14 text-gray-600 font-light">
            <h1 tw="font-black uppercase text-3xl lg:text-5xl text-primary mb-10">Error</h1>
            <p tw="italic">
              {statusCode ? `An error ${statusCode} occurred on the server.` : 'An error occurred on client.'}
            </p>
            <p>
              <FormattedMessage
                id="err-500.text"
                defaultMessage="If you see this page, please let us know on which url it happened, by sending us an email at"
              />{' '}
              <a href="mailto:hello@jogl.io">hello[at]jogl.io</a>
            </p>
          </div>
          <div tw="mb-20 md:mb-0">
            <Link href="/">
              <a>
                <Button>
                  <FormattedMessage id="err-404.btn" defaultMessage="Back to Home page" />
                </Button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
ErrorPage.getInitialProps = async ({ res, err, asPath }) => {
  // Capture 404 of pages with traling slash and redirect them
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  // If the page has a trailing slash like "/about/" it will remove the trailing slash
  // so => "/about" while keeping the URL params. This is because nextJS doesn't allow
  // trailing slashes
  // TODO: might have to change it when rewrites and redirects will be available in next.
  if (statusCode && statusCode === 404) {
    const [path, query = ''] = asPath.split('?');
    if (path.match(/\/$/)) {
      const withoutTrailingSlash = path.substr(0, path.length - 1);
      if (res) {
        res.writeHead(302, {
          Location: `${withoutTrailingSlash}${query ? `?${query}` : ''}`,
        });
        res.end();
      } else {
        Router.push(`${withoutTrailingSlash}${query ? `?${query}` : ''}`);
      }
    }
  }
  return { statusCode };
};
export default ErrorPage;
