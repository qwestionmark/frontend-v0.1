/* eslint-disable camelcase */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { flexbox, layout, space } from 'styled-system';
import Box from '~/components/Box';
import Feed from '~/components/Feed/Feed';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import A from '~/components/primitives/A';
import Button from '~/components/primitives/Button';
import H2 from '~/components/primitives/H2';
import P from '~/components/primitives/P';
import SpaceAbout from '~/components/Space/SpaceAbout';
import SpaceFaq from '~/components/Space/SpaceFaq';
import SpaceHeader from '~/components/Space/SpaceHeader';
import SpaceHome from '~/components/Space/SpaceHome';
import SpaceMembers from '~/components/Space/SpaceMembers';
import SpaceNeeds from '~/components/Space/SpaceNeeds';
import SpaceProjects from '~/components/Space/SpaceProjects';
import SpaceResources from '~/components/Space/SpaceResources';
import { ContactForm } from '~/components/Tools/ContactForm';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import Loading from '~/components/Tools/Loading';
import NoResults from '~/components/Tools/NoResults';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import useMembers from '~/hooks/useMembers';
import useUserData from '~/hooks/useUserData';
import { Challenge, Faq, Program, Space } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import InviteMember from '~/components/Tools/InviteMember';
import ChallengeCard from '~/components/Challenge/ChallengeCard';
import ReactGA from 'react-ga';
import ReactTooltip from 'react-tooltip';
// import ProgramCard from '~/components/Program/ProgramCard';
// import Image from 'next/image';
import { useSpaceViewAsContext } from '~/contexts/SpaceViewAsContext';
// import { ViewAsDropDown } from '~/components/Space/ViewAsDropDown';
import Image2 from '~/components/Image2';
import 'twin.macro';

const DEFAULT_TAB = 'home';
const SpaceDetails: NextPage<{ space: Space }> = ({ space }) => {
  const router = useRouter();
  const { locale, formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const { members } = useMembers('programs', space?.id, 25); // take 25 first members (@TODO maybe only 6?)
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const tabs = [
    // add certain tabs to array only if admin had selected those tabs to be displayed
    { value: 'home', intlId: 'space.home', defaultMessage: 'Home' },
    { value: 'about', intlId: 'space.tab.about', defaultMessage: 'About' },
    { value: 'members', intlId: 'general.members', defaultMessage: 'Members' },
    { value: 'news', intlId: 'entity.tab.news', defaultMessage: 'News' },
    // ...(space.selected_tabs.programs
    //   ? [{ value: 'programs', intlId: 'general.programs', defaultMessage: 'Programs' }]
    //   : []),
    ...(space.selected_tabs.challenges
      ? [{ value: 'challenges', intlId: 'entity.tab.challenges', defaultMessage: 'Challenges' }]
      : []),
    { value: 'projects', intlId: 'entity.tab.projects', defaultMessage: 'Projects' },
    { value: 'needs', intlId: 'entity.tab.needs', defaultMessage: 'Needs' },
    { value: 'contact', intlId: 'user.btn.contact', defaultMessage: 'Contact' },
    ...(space.selected_tabs.resources
      ? [{ value: 'resources', intlId: 'space.tab.resources', defaultMessage: 'Resources' }]
      : []),
    ...(space.selected_tabs.faqs ? [{ value: 'faq', intlId: 'entity.info.faq', defaultMessage: 'FAQs' }] : []),
  ];
  const { data: dataPosts, revalidate: revalidatePost } = useGet(`/api/feeds/${space?.feed_id}?items=5&page=1`);
  const { data: dataChallenges } = useGet<{ challenges: Challenge }>(`/api/spaces/${space?.id}/challenges`);
  // const { data: dataPrograms } = useGet<{ programs: Program }>(`/api/spaces/${space?.id}/programs`);
  const { data: dataExternalLink } = useGet(`/api/spaces/${space?.id}/links`);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/spaces/${space.id}/faq`);
  const { userData } = useUserData();
  const theme = useTheme();
  const navRef = useRef();
  const { viewAsMode } = useSpaceViewAsContext();

  useEffect(() => {
    // on first load, check if url has a tab param
    if (router.query.tab) {
      // if it does, select this tab
      setSelectedTab(router.query.tab as string);
    }
  }, []);

  space?.contact_email || 'hello@jogl.io';

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'home') {
        router.push(`/space/${router.query.short_title}`, undefined, { shallow: true });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
      title: 'Send message to {userFullName}',
      titleId: 'user.contactModal.title',
      values: { userFullName: first_name + ' ' + last_name },
    });
  };

  return (
    <Layout
      title={space?.title && `${space.title} | JOGL`}
      desc={space?.short_description}
      img={space?.banner_url || '/images/default/default-space.jpg'}
      className="small-top-margin"
    >
      <Box width="100%" position="relative">
        <Img src={space?.banner_url || '/images/default/default-space.jpg'} alt={`${space?.title} banner`} unsized />
        <Box position="absolute" top="10px" left="10px">
          {/* {space?.is_admin && <ViewAsDropDown />} */}
        </Box>
      </Box>
      <Box bg="white" pb={7} px={[3, 4, undefined, undefined, 0]} position="relative">
        <Box>
          <SpaceHeader space={space} members={members} lang={locale} />
        </Box>
      </Box>
      <Grid
        display="grid"
        gridTemplateColumns={['100%', undefined, '20rem calc(100% - 20rem)', undefined, '25% 50% 25%']}
        bg={[theme.colors.lightBlue, undefined, 'white']}
        ref={navRef}
      >
        {/* Header and nav (left col on desktop, top col on mobile */}
        <Box
          alignItems={[undefined, undefined, 'center']}
          bg="white"
          position="sticky"
          top={['40px', undefined, '50px']}
          zIndex={8}
        >
          <NavContainer bg="white">
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="left" />
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="right" />
            <Nav
              as="nav"
              flexDirection={['row', undefined, 'column']}
              spaceX={[3, undefined, 0]}
              width="100%"
              bg="white"
            >
              {tabs.map((item, index) => (
                <Link
                  shallow
                  scroll={false}
                  key={index}
                  href={`/space/${router.query.short_title}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                >
                  <Tab selected={item.value === selectedTab} px={[1, undefined, 3]} py={3} as="button">
                    <FormattedMessage id={item.intlId} defaultMessage={item.defaultMessage} />
                  </Tab>
                </Link>
              ))}
            </Nav>
          </NavContainer>
        </Box>

        {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
        <Box px={[0, undefined, 4]}>
          {selectedTab === 'home' && (
            <TabPanel id="home" px={[3, 4, undefined, 0]} pt={[5, undefined, 0]}>
              <DesktopBorders>
                <SpaceHome
                  space={space}
                  shortDescription={(locale === 'fr' && space?.short_description_fr) || space?.short_description}
                  isAdmin={space.is_admin && viewAsMode === 'owner'}
                  posts={dataPosts}
                  challenges={dataChallenges?.challenges}
                  refreshPost={revalidatePost}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'about' && (
            <TabPanel id="about" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <SpaceAbout
                  description={locale === 'fr' && space?.description_fr ? space?.description_fr : space?.description}
                  shortDescription={space?.short_description}
                  status={space?.status}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'news' && (
            <TabPanel id="news" style={{ margin: '0 auto' }} width="100%" pt={[2, undefined, 0]}>
              {
                //  Show feed, and pass DisplayCreate to admins
                space.feed_id && (
                  <Feed
                    feedId={space.feed_id}
                    displayCreate={(space.is_admin || space.is_owner) && viewAsMode === 'owner'}
                    isAdmin={(space.is_admin || space.is_owner) && viewAsMode === 'owner'}
                  />
                )
              }
            </TabPanel>
          )}
          {/* {selectedTab === 'programs' && (
            <TabPanel id="programs" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <P>
                {formatMessage({
                  id: 'space.programs.text',
                  defaultMessage:
                    '*To participate in this space, participate in at least one of its programs. Check out the projects already submitted, contribute in them or create your own!',
                })}
              </P>
              <Box py={4} position="relative">
                {!dataPrograms ? (
                  <Loading />
                ) : dataPrograms?.program?.length === 0 ? (
                  <NoResults type="program" />
                ) : (
                  <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
                    {dataPrograms?.program
                      ?.filter(({ status }) => status !== 'draft')
                      // don't display draft programs
                      .map((program, index) => (
                        <ProgramCard
                          key={index}
                          id={program.id}
                          banner_url={program.banner_url || '/images/default/default-program.jpg'}
                          short_title={program.short_title}
                          title={program.title}
                          title_fr={program.title_fr}
                          short_description={program.short_description}
                          short_description_fr={program.short_description_fr}
                          membersCount={program.members_count}
                          needsCount={program.needs_count}
                          clapsCount={program.claps_count}
                          projectsCount={program.projects_count}
                          has_saved={program.has_saved}
                        />
                      ))}
                  </Grid>
                )}
              </Box>
            </TabPanel>
          )} */}
          {selectedTab === 'challenges' && (
            <TabPanel id="challenges" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <P>
                {formatMessage({
                  id: 'space.challenges.text',
                  defaultMessage:
                    'To participate in this space, participate in at least one of its challenges. Check out the projects already submitted, contribute in them or create your own!',
                })}
                <br />
                {formatMessage({
                  id: 'space.home.challengeCta',
                  defaultMessage:
                    'Get involved in Challenges by submitting a project, evaluating submissions, & voting',
                })}
                .
              </P>
              <Box py={4} position="relative">
                {!dataChallenges ? (
                  <Loading />
                ) : dataChallenges?.challenges.length === 0 ? (
                  <NoResults type="challenge" />
                ) : (
                  <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
                    {dataChallenges.challenges
                      ?.filter(({ status }) => status !== 'draft')
                      // don't display draft challenges
                      .map((challenge, index) => (
                        <ChallengeCard
                          key={index}
                          id={challenge.id}
                          banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                          short_title={challenge.short_title}
                          title={challenge.title}
                          title_fr={challenge.title_fr}
                          short_description={challenge.short_description}
                          short_description_fr={challenge.short_description_fr}
                          membersCount={challenge.members_count}
                          needsCount={challenge.needs_count}
                          clapsCount={challenge.claps_count}
                          projectsCount={challenge.projects_count}
                          status={challenge.status}
                          has_saved={challenge.has_saved}
                        />
                      ))}
                  </Grid>
                )}
              </Box>
            </TabPanel>
          )}
          {selectedTab === 'projects' && (
            <TabPanel id="projects" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <SpaceProjects spaceId={space.id} />
            </TabPanel>
          )}
          {selectedTab === 'needs' && (
            <TabPanel id="needs" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <SpaceNeeds spaceId={space.id} />
            </TabPanel>
          )}
          {selectedTab === 'members' && (
            <TabPanel id="members" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]} position="relative">
              <SpaceMembers spaceId={space.id} />
            </TabPanel>
          )}
          {selectedTab === 'resources' && (
            <TabPanel id="resources" px={0} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <SpaceResources spaceId={space?.id} isAdmin={space?.is_admin} documents={space?.documents} />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'contact' && (
            <TabPanel id="contact" px={0} pt={[7, undefined, undefined, 0]}>
              <p>*Contact form</p>
            </TabPanel>
          )}
          {selectedTab === 'faq' && (
            <TabPanel id="faq" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <SpaceFaq faqList={faqList} />
              </DesktopBorders>
            </TabPanel>
          )}
          {space?.is_admin && (
            <Box pt={11} px={4} display={['block', undefined, undefined, undefined, 'none']}>
              <H2>{formatMessage({ id: 'member.invite.general', defaultMessage: 'Invite someone to join!' })}</H2>
              <InviteMember itemType="spaces" itemId={space.id} />
            </Box>
          )}
          <Box pt={8} pb={7} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <CtaAndLinks dataExternalLink={dataExternalLink} userData={userData} formatMessage={formatMessage} />
          </Box>
        </Box>

        {/* Aside right col, only on desktop */}
        <Box as="aside" display={['none', undefined, undefined, undefined, 'block']} spaceY={4} bg="white">
          {/* Create account CTA */}
          {/* {!userData && ( // if user is not connected
            <Box borderRadius="1rem" alignItems="center" py={4}>
              <A href="/signup">
                <Button>
                  {formatMessage({ id: 'space.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
                </Button>
              </A>
              <P pt={2} mb={0}>
                {formatMessage({
                  id: 'space.rightCompo.createAccount.text',
                  defaultMessage: 'to participate to the space',
                })}
              </P>
            </Box>
          )} */}
          {/* Supporters bloc */}
          {selectedTab === 'about' &&
            space?.enablers && ( // show only on about page
              <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
                <H2>{formatMessage({ id: 'space.supporters', defaultMessage: 'Supporters' })}</H2>
                <InfoHtmlComponent title="" content={space?.enablers} />
                <a href="mailto:hello@jogl.io">
                  {formatMessage({
                    id: 'space.supporters.interested',
                    defaultMessage: 'Interested in becoming a supporter?',
                  })}
                </a>
              </Box>
            )}
          {/* Meeting info bloc */}
          {(selectedTab === 'home' || selectedTab === 'about') &&
            space?.meeting_information && ( // show only on home page
              <Box bg="white" display={['none', undefined, undefined, undefined, 'flex']} pt={3}>
                <H2>{formatMessage({ id: 'space.meeting_information', defaultMessage: "Let's meet!" })}</H2>
                <InfoHtmlComponent title="" content={space?.meeting_information} />
              </Box>
            )}
          {/* Contact space leaders bloc */}
          {selectedTab === 'faq' && members && (
            <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
              <H2>{formatMessage({ id: 'footer.contactUs', defaultMessage: 'Contact us' })}</H2>
              <Box pt={5} spaceY={4}>
                {members
                  .filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                  .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                    <Box key={index} row spaceX={4} alignItems="center">
                      <Link href={`/user/${id}`}>
                        <a>
                          <Box row spaceX={4} alignItems="center">
                            <img
                              width="50px"
                              height="50px"
                              style={{ borderRadius: '50%', objectFit: 'cover' }}
                              src={logo_url_sm}
                              alt={`${first_name} ${last_name}`}
                            />
                            <P fontWeight="bold" fontSize="1.2rem" mb={0}>
                              {first_name + ' ' + last_name}
                            </P>
                          </Box>
                        </a>
                      </Link>
                      <Box>
                        <ContactButton
                          icon="envelope"
                          size="lg"
                          onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                          onKeyUp={(e) =>
                            // execute only if it's the 'enter' key
                            (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                          }
                          tabIndex={0}
                          data-tip={formatMessage(
                            {
                              id: 'user.contactModal.title',
                              defaultMessage: 'Send message to {userFullName}',
                            },
                            { userFullName: first_name + ' ' + last_name }
                          )}
                          data-for="contactAdmin"
                          // show/hide tooltip on element focus/blur
                          onFocus={(e) => ReactTooltip.show(e.target)}
                          onBlur={(e) => ReactTooltip.hide(e.target)}
                        />
                        <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                      </Box>
                    </Box>
                  ))}
              </Box>
            </Box>
          )}
          {/* Invite someone component */}
          {space?.is_admin && (
            <Box>
              <H2>{formatMessage({ id: 'member.invite.general', defaultMessage: 'Invite someone to join!' })}</H2>
              <InviteMember itemType="spaces" itemId={space.id} />
            </Box>
          )}
          {/* Cta and links section (+ add style to make whole section sticky on top when attaining it */}
          <div tw="hidden flex-col xl:flex xl:sticky xl:top-36">
            {/* Create account CTA */}
            <CtaAndLinks dataExternalLink={dataExternalLink} userData={userData} formatMessage={formatMessage} />
          </div>
        </Box>
      </Grid>
    </Layout>
  );
};

const CtaAndLinks = ({ dataExternalLink, userData, formatMessage }) => {
  return (
    <>
      {!userData && ( // if user is not connected
        <Box row alignItems={['start', undefined, undefined, undefined, 'center']} pt={4} pb={8} flexWrap="wrap">
          <A href="/signup">
            <Button>
              {formatMessage({ id: 'space.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
            </Button>
          </A>
          <P pt={2} pl={2} mb={0}>
            {formatMessage({
              id: 'space.rightCompo.createAccount.text',
              defaultMessage: 'to participate to the space',
            })}
          </P>
        </Box>
      )}
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <Box>
          <H2 pb={4}>{formatMessage({ id: 'general.externalLink.findUs', defaultMessage: 'Find us' })}</H2>
          <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(57px, 1fr))">
            {[...dataExternalLink].map((link, i) => (
              <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                <a href={link.url} target="_blank">
                  <img width="45px" src={link.icon_url} />
                </a>
              </ExternalLinkIcon>
            ))}
          </Grid>
        </Box>
      )}
    </>
  );
};

const ExternalLinkIcon = styled(Box)`
  img:hover {
    opacity: 0.8;
  }
`;

const Tab = styled.a`
  display: inline-block;
  border-bottom: 3px solid transparent;
  border-bottom-color: ${(p) => p.selected && p.theme.colors.primary};
  font-weight: ${(p) => p.selected && 700};
  white-space: nowrap;
  color: inherit;
  text-align: right;
  cursor: pointer;
  &:hover {
    border-bottom-color: ${(p) => p.theme.colors.primary};
    color: inherit;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid transparent;
    &:hover {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
  }
  ${[space, layout]};
`;

const TabPanel = styled.div`
  ${[layout, space]};
  .infoHtml {
    width: 100% !important;
  }
`;

const NavContainer = styled(Box)`
  position: relative;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid ${(p) => p.theme.colors.greys['200']};
  }
  /* To make element sticky on top when reach top of page */
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    position: sticky;
    position: -webkit-sticky;
    top: 150px;
  }

  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    width: 100%;
    justify-content: flex-end;
    padding-right: 20px;
  }
`;
const Nav = styled(Box)`
  overflow-x: scroll;
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    font-size: ${(p) => p.theme.fontSizes.xl};
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding-left: 1.8rem;
    button:last-child {
      padding-right: 3rem;
    }
    button {
      padding: 0 3px 5px;
    }
  }
`;

const OverflowGradient = styled.div`
  ${layout};
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

const ContactButton = styled(FontAwesomeIcon)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const Img = styled(Image2)`
  ${[flexbox, layout, space]};
  width: 100%;
`;

// for desktop tabs that need an englobing div with padding and border
const DesktopBorders = ({ children }) => (
  <Box
    borderLeft={['none', undefined, '2px solid #f4f4f4']}
    borderRight={['none', undefined, undefined, undefined, '2px solid #f4f4f4']}
    pl={[0, undefined, 4]}
    pr={[0, undefined, undefined, undefined, 4]}
  >
    {children}
  </Box>
);

const getSpace = async (api, spaceId) => {
  const res = await api.get(`/api/spaces/${spaceId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

SpaceDetails.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/spaces/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const space = await getSpace(api, res.data.id).catch((err) => console.error(err));
      return { space };
    }
    isomorphicRedirect(ctx, '/');
  }
  // Case short_title is actually an id
  const space = await getSpace(api, query.short_title).catch((err) => console.error(err));
  if (space) {
    return { space };
  } else {
    isomorphicRedirect(ctx, '/');
  }
};

export default SpaceDetails;
