import React, { useState, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '~/components/Layout';
import SpaceForm from '~/components/Space/SpaceForm';
import MembersList from '~/components/Members/MembersList';
import { stickyTabNav, scrollToActiveTab } from '~/utils/utils';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import Grid from '~/components/Grid';
import { NextPage } from 'next';
// import Button from '~/components/primitives/Button';
import { useModal } from '~/contexts/modalContext';
// import Translate from '~/utils/Translate';
// import Alert from '~/components/Tools/Alert';
import ManageFaq from '~/components/Tools/ManageFaq';
import ManageResources from '~/components/Tools/ManageResources';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import ManageBoards from '~/components/Tools/ManageBoards';
import Box from '~/components/Box';
import Loading from '~/components/Tools/Loading';
// import NoResults from '~/components/Tools/NoResults';
// import ProgramAdminCard from '~/components/Program/ProgramAdminCard';
// import { Program } from '~/types';
import ProjectAdminCard from '~/components/Project/ProjectAdminCard';
import H2 from '~/components/primitives/H2';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import { Space } from '~/types';

interface Props {
  space: Space;
}
const SpaceEdit: NextPage<Props> = ({ space: spaceProp }) => {
  const [space, setSpace] = useState(spaceProp);
  const [updatedSpace, setUpdatedSpace] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const api = useApi();
  const { formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const router = useRouter();
  // const { data: dataPrograms, revalidate: programsRevalidate, mutate: mutatePrograms } = useGet<{ programs: Program }>(
  //   `/api/spaces/${space.id}/programs`
  // );
  const { data: projectsData, revalidate: projectsRevalidate, mutate: mutateProjects } = useGet(
    `/api/spaces/${space.id}/projects`
  );
  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit'); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const handleChange = (key, content) => {
    setSpace((prevSpace) => ({ ...prevSpace, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({ ...prevUpdatedSpace, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleChangeFeatured = (key, content) => {
    const featured_obj = key.split('.')[1];
    setSpace((prevSpace) => ({
      ...prevSpace,
      show_featured: { ...prevSpace.show_featured, [featured_obj]: content },
    })); // update fields as user changes them
    setUpdatedSpace((prevUpdatedSpace) => ({
      ...prevUpdatedSpace,
      show_featured: { ...space.show_featured, [featured_obj]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleChangeShowTabs = (key, content) => {
    const selected_tabs = key.split('.')[1];
    setSpace((prevSpace) => ({
      ...prevSpace,
      selected_tabs: { ...prevSpace.selected_tabs, [selected_tabs]: content },
    })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({
      ...prevUpdatedSpace,
      selected_tabs: { ...space.selected_tabs, [selected_tabs]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/spaces/${space.id}`, { space: updatedSpace }).catch((err) => {
      console.error(`Couldn't patch space with id=${space.id}`, err);
      setSending(false);
    });
    if (res) {
      // on success
      setSending(false);
      setUpdatedSpace(undefined); // reset updated space component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
      // router.push({ pathname: `/space/${space.short_title}`, query: { success: 1 } }).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
    }
  };
  return (
    <Layout title={`${space.title} | JOGL`}>
      <div className="programEdit">
        <Box px={4}>
          <h1>
            <FormattedMessage id="space.edit.title" defaultMessage="Edit the space" />
          </h1>
          <Link href={`/space/${space.short_title}`}>
            <a>
              <FontAwesomeIcon icon="arrow-left" />
              <FormattedMessage id="space.edit.back" defaultMessage="Go back" />
            </a>
          </Link>
        </Box>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#general" data-toggle="tab">
            <FormattedMessage id="footer.general" defaultMessage="General" />
          </a>
          <a className="nav-item nav-link" href="#home" data-toggle="tab">
            <FormattedMessage id="program.home" defaultMessage="Home" />
          </a>
          <a className="nav-item nav-link" href="#about" data-toggle="tab">
            <FormattedMessage id="general.tab.about" defaultMessage="About" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="entity.tab.members" defaultMessage="Members" />
          </a>
          {/* <a className="nav-item nav-link" href="#programs" data-toggle="tab">
            <FormattedMessage id="general.programs" defaultMessage="Programs" />
          </a> */}
          <a className="nav-item nav-link" href="#chal-proj" data-toggle="tab">
            {/* <FormattedMessage id="general.challenges" defaultMessage="*Challenges/Projects" /> */}
            Challenges/Projects
          </a>
          <a className="nav-item nav-link" href="#faqs" data-toggle="tab">
            <FormattedMessage id="faq.title" defaultMessage="FAQs" />
          </a>
          <a className="nav-item nav-link" href="#partners" data-toggle="tab">
            <FormattedMessage id="entity.info.enablers" defaultMessage="Partners" />
          </a>
          <a className="nav-item nav-link" href="#Resources" data-toggle="tab">
            <FormattedMessage id="space.resourcesTitle" defaultMessage="Resources" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            {/* Basic infos */}
            <div className="tab-pane active" id="general">
              <SpaceForm
                mode="edit"
                space={space}
                handleChange={handleChange}
                handleChangeShowTabs={handleChangeShowTabs}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </div>
            {/* Home tab infos */}
            <div className="tab-pane" id="home">
              <SpaceForm
                mode="edit_home"
                space={space}
                handleChange={handleChange}
                handleChangeFeatured={handleChangeFeatured}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </div>
            {/* About tab infos */}
            <div className="tab-pane" id="about">
              <SpaceForm
                mode="edit_about"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
              {/* waiting to have boards support for spaces */}
              <ManageBoards itemType="spaces" itemId={space.id} />
            </div>
            {/* Members */}
            <div className="tab-pane" id="members">
              {space.id && <MembersList itemType="spaces" itemId={space.id} isOwner={space.is_owner} />}
            </div>
            {/* Programs */}
            {/* <div className="tab-pane" id="programs">
              {dataPrograms ? (
                <div className="challengesAttachedList">
                  <div className="justify-content-end challengesAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <LinkProgramModal
                              alreadyPresentPrograms={dataPrograms?.program}
                              spaceId={space.id}
                              mutatePrograms={mutatePrograms}
                              closeModal={() => setIsOpen(false)}
                            />
                            // <p>In progress</p>
                          ),
                          title: '*Submit a program',
                          titleId: 'attach.program.button.title',
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      <Translate id="attach.program.button.title" defaultMessage="*Submit a program" />
                    </Button>
                  </div>
                  {!dataPrograms ? (
                    <Loading />
                  ) : dataPrograms?.program?.length === 0 ? (
                    <NoResults type="program" />
                  ) : (
                    <Grid gridCols={1} display={['grid', 'inline-grid']} pt={8}>
                      {dataPrograms?.program?.map((program, i) => (
                        <ProgramAdminCard
                          spaceId={space.id}
                          program={program}
                          key={i}
                          callBack={() => programsRevalidate()}
                        />
                      ))}
                    </Grid>
                  )}
                </div>
              ) : (
                <Loading />
              )}
            </div> */}
            {/* Challenges/Projects */}
            <div className="tab-pane" id="chal-proj">
              <H2>*Challenges</H2>
              <p>Coming soon</p>
              <H2 pt={3}>*Projects</H2>
              {projectsData ? (
                <div className="projectsAttachedList">
                  <Grid gridCols={1} display={['grid', 'inline-grid']} pt={4}>
                    {projectsData?.projects.map((project, i) => (
                      <ProjectAdminCard key={i} project={project} callBack={projectsRevalidate} noChallengeId />
                    ))}
                  </Grid>
                </div>
              ) : (
                <Loading />
              )}
            </div>
            {/* Partners */}
            <div className="tab-pane" id="partners">
              <SpaceForm
                mode="edit_partners"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </div>
            {/* FAQs */}
            <div className="tab-pane" id="faqs">
              <ManageFaq itemType="spaces" itemId={space.id} />
            </div>
            {/* Resources */}
            <div className="tab-pane" id="Resources">
              <H2 pb={3}>{formatMessage({ id: 'entity.tab.documents', defaultMessage: 'Documents' })}</H2>
              <DocumentsManager
                documents={space.documents}
                isAdmin={space.is_admin}
                itemId={space.id}
                itemType="spaces"
              />
              <Box pb={5}></Box>
              <ManageResources itemType="spaces" itemId={space.id} />
            </div>
            {/* Advanced */}
            <div className="tab-pane" id="advanced">
              <ManageExternalLink itemType="spaces" itemId={space.id} />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

SpaceEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/spaces/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch space with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const spaceRes = await api
      .get(`/api/spaces/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch space with id=${getIdRes.data.id}`, err));
    // Check if it got the space and if the user is admin
    if (spaceRes?.data?.is_admin) return { space: spaceRes.data };
  }
  isomorphicRedirect(ctx, '/');
};

// interface PropsModal {
//   alreadyPresentPrograms: any[];
//   spaceId: number;
//   mutatePrograms: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
//   closeModal: () => void;
// }
// export const LinkProgramModal: FC<PropsModal> = ({
//   alreadyPresentPrograms = [],
//   spaceId,
//   mutatePrograms,
//   closeModal,
// }) => {
//   const { data: dataProgramsMine } = useGet('/api/programs/mine');
//   const [selectedProgram, setSelectedProgram] = useState();
//   const [sending, setSending] = useState(false);
//   const [requestSent, setRequestSent] = useState(false);
//   const [isButtonDisabled, setIsButtonDisabled] = useState(true);
//   const api = useApi();

//   // Filter the programs that are already in this program so you don't add it twice!
//   const filteredPrograms = useMemo(() => {
//     // if (dataProgramsMine && alreadyPresentPrograms.length !== 0) {
//     if (dataProgramsMine) {
//       return dataProgramsMine.filter((programMine) => {
//         // Check if my program is found in alreadyPresentPrograms
//         const isMyProgramAlreadyPresent = alreadyPresentPrograms.find((alreadyPresentProgram) => {
//           return alreadyPresentProgram.id === programMine.id;
//         });
//         // We keep only the ones that are not present
//         return !isMyProgramAlreadyPresent;
//       });
//     } else return dataProgramsMine;
//   }, [dataProgramsMine, alreadyPresentPrograms]);

//   const onSubmit = async (e) => {
//     e.preventDefault();
//     setSending(true);
//     // Link this program to the program then mutate the cache of the programs from the parent prop
//     if ((selectedProgram as { id: number })?.id) {
//       await api
//         .put(`/api/spaces/${spaceId}/programs/${(selectedProgram as { id: number }).id}`)
//         .catch(() =>
//           console.error(`Could not PUT/link spaceId=${spaceId} with program programId=${selectedProgram.id}`)
//         );
//       setSending(false);
//       setRequestSent(true);
//       setIsButtonDisabled(true);
//       mutatePrograms({ programs: [...alreadyPresentPrograms, selectedProgram] });
//       setTimeout(() => {
//         // close modal after 3.5sec
//         closeModal();
//       }, 3500);
//     }
//   };
//   const onProgramselect = (e) => {
//     setSelectedProgram(filteredPrograms.find((item) => item.id === parseInt(e.target.value)));
//     setIsButtonDisabled(false);
//   };

//   return (
//     <div>
//       {filteredPrograms && filteredPrograms.length > 0 ? (
//         <form style={{ textAlign: 'left' }}>
//           {filteredPrograms.map((program, index) => (
//             <div className="form-check" key={index} style={{ height: '50px' }}>
//               <input
//                 type="radio"
//                 className="form-check-input"
//                 name="exampleRadios"
//                 id={`program-${index}`}
//                 value={program.id}
//                 onChange={onProgramselect}
//               />
//               <label className="form-check-label" htmlFor={`program-${index}`}>
//                 {program.title}
//               </label>
//             </div>
//           ))}

//           <div className="btnZone">
//             <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} mb={2}>
//               <>
//                 {sending && (
//                   <>
//                     <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
//                     &nbsp;
//                   </>
//                 )}
//                 <FormattedMessage id="attach.program.btnSend" defaultMessage="Submit this program" />
//               </>
//             </Button>
//             {requestSent && (
//               <Alert
//                 type="success"
//                 message={<FormattedMessage id="attach.program.btnSendEnded" defaultMessage="program was submitted" />}
//               />
//             )}
//           </div>
//         </form>
//       ) : (
//         <div className="noProgram" style={{ textAlign: 'center' }}>
//           <FormattedMessage id="attach.program.noProgram" defaultMessage="You do not have a program to add" />
//         </div>
//       )}
//     </div>
//   );
// };

export default SpaceEdit;
