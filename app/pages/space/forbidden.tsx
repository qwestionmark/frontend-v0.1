import Link from 'next/link';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { NextPage } from 'next';
import Layout from '~/components/Layout';

const SpaceForbiddenPage: NextPage = () => {
  return (
    <Layout>
      <div className="container-fluid SpacePage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            <FormattedMessage
              id="space.info.forbidden"
              defaultMessage="Please contact the JOGL team to obtain rights to create a space"
            />
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <Link href="/" as="/">
              <a>
                <button className="btn btn-primary" type="button">
                  <FormattedMessage id="space.info.forbiddenBtn" defaultMessage="Return to Home page" />
                </button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default SpaceForbiddenPage;
