import Link from 'next/link';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { NextPage } from 'next';
import Layout from '~/components/Layout';

const ProgramForbiddenPage: NextPage = () => {
  return (
    <Layout>
      <div className="container-fluid ProgramPage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            <FormattedMessage
              id="program.info.forbidden"
              defaultMessage="Please contact the JOGL team to obtain rights to create a program"
            />
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <Link href="/" as="/">
              <a>
                <button className="btn btn-primary" type="button">
                  <FormattedMessage id="program.info.forbiddenBtn" defaultMessage="Back to Home page" />
                </button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default ProgramForbiddenPage;
