import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import Layout from '~/components/Layout';
import 'twin.macro';
import Button from '~/components/primitives/Button';

export default function Page404() {
  return (
    <Layout title="404 | JOGL" className="no-margin">
      <div tw="bg-gray-100 flex items-center px-5 py-16 lg:p-20 overflow-hidden relative justify-center">
        <div tw="flex-1 rounded-3xl bg-white shadow-xl p-20 text-gray-800 relative md:flex items-center text-center md:text-left max-w-4xl">
          {/* <div tw="w-full md:w-1/2"> */}
          <div tw="w-full">
            {/* <div tw="mb-10 lg:mb-20">logo here</div> */}
            <div tw="mb-10 md:mb-14 text-gray-600 font-light">
              <h1 tw="font-black uppercase text-3xl lg:text-5xl text-primary mb-10">
                <FormattedMessage id="err-404.title" defaultMessage="Error 404, that page doesn’t exist!" />
              </h1>
              {/* <p>The page you're looking for isn't available.</p>
              <p>Try searching again or use the Go Back button below.</p> */}
            </div>
            <div tw="mb-20 md:mb-0">
              <Link href="/">
                <a>
                  <Button>
                    <FormattedMessage id="err-404.btn" defaultMessage="Back to Home page" />
                  </Button>
                </a>
              </Link>
            </div>
          </div>
          {/* <div tw="w-full md:w-1/2 text-center"> */}
          <div tw="w-full text-center">{/* Img here */}</div>
        </div>
        {/* <div tw="w-64 md:w-96 h-96 md:h-full bg-blue-200 bg-opacity-30 absolute -top-64 md:-top-96 right-20 md:right-32 rounded-full pointer-events-none -rotate-45 transform"></div>
        <div tw="w-96 h-full bg-yellow-200 bg-opacity-20 absolute -bottom-96 right-64 rounded-full pointer-events-none -rotate-45 transform"></div> */}
      </div>
    </Layout>
  );
}
