import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { FC, useEffect, useMemo, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Box from '~/components/Box';
import Carousel from '~/components/Carousel';
import ChallengeMiniCard from '~/components/Challenge/ChallengeMiniCard';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import MembersList from '~/components/Members/MembersList';
import NeedCard from '~/components/Need/NeedCard';
import NeedCreate from '~/components/Need/NeedCreate';
import Button from '~/components/primitives/Button';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import ProjectForm from '~/components/Project/ProjectForm';
import Alert from '~/components/Tools/Alert';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import WebHooks from '~/components/Tools/Webhooks/Webhooks';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import useNeeds from '~/hooks/useNeeds';
import { Project } from '~/types/project';
import Translate from '~/utils/Translate';
import { scrollToActiveTab, stickyTabNav } from '~/utils/utils';
import ReactGA from 'react-ga';
import P from '~/components/primitives/P';
import useUser from '~/hooks/useUser';
import { Challenge } from '~/components/Challenge/ChallengeCard';
import { Program } from '~/types';
import 'twin.macro';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';

interface Props {
  project: Project;
}
const ProjectEdit: NextPage<Props> = ({ project: projectProp }) => {
  const [project, setProject] = useState(projectProp);
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [updatedProject, setUpdatedProject] = useState(undefined);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [errors, setErrors] = useState(undefined);
  const urlBack = `/project/${router.query.id}/${project?.short_title}`;
  const { dataNeeds, needsRevalidate } = useNeeds('projects', parseInt(router.query.id as string));
  const { showModal, setIsOpen } = useModal();
  const api = useApi();
  const { user } = useUser();
  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit'); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  // const handleChange = (key, content) => {
  //   mutateProject({ data: { ...project, [key]: content } }, false); // update fields as user changes them
  //   // TODO: have only one place where we manage content
  //   setUpdatedProject((prevUpdatedProject) => ({ ...prevUpdatedProject, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  // };

  const handleChange = (key, content) => {
    setProject((prevProject) => ({ ...prevProject, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProject((prevUpdatedProject) => ({ ...prevUpdatedProject, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/projects/${project.id}`, { project: updatedProject })
      .then(() => {
        setSending(false);
        setUpdatedProject(undefined); // reset updated program component
        setHasUpdated(true); // show update confirmation message
        setTimeout(() => {
          setHasUpdated(false);
        }, 3000); // hide confirmation message after 3 seconds
        // router.push({ pathname: urlBack, query: { success: 1 } }).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
      })
      .catch(() => {
        setSending(false);
      });
  };
  const deleteProj = () => {
    api
      .delete(`api/projects/${project.id}/`)
      .then(() => {
        ReactGA.event({ category: 'Project', action: 'delete', label: `[${user.id},${project.id}]` }); // record event to Google Analytics
        setIsOpen(false); // close modal
        router.push('/search/projects');
      })
      .catch((error) => {
        setErrors(error.toString());
      });
  };
  const onChallengeDelete = (id) => {
    const newProject = { ...project, challenges: project.challenges.filter((challenge) => challenge.id !== id) };
    setProject(newProject); // update fields as user changes them
    // mutateProject({ data: newProject }, false);
  };

  const delBtnTitleId = project?.members_count > 1 ? 'project.archive.title' : 'project.delete.title';
  const delBtnTitle = project?.members_count > 1 ? 'Archive project' : 'Delete project';
  const delBtnTextId = project?.members_count > 1 ? 'project.archive.text' : 'project.delete.text';
  const delBtnText =
    project?.members_count > 1
      ? 'Are you sure you want to archive this project?'
      : 'Are you sure you want to delete this project?  It will be permanently deleted.';

  const errorMessage = errors?.includes('err-') ? (
    <FormattedMessage id={errors} defaultMessage="An error has occurred" />
  ) : (
    errors
  );

  return (
    <Layout title={`${project?.title} | JOGL`} desc={project?.short_description} img={project?.banner_url}>
      {/* <Loading active={!project}> */}
      <div className="projectEdit container-fluid justify-content-center">
        <h1>
          <Translate id="project.edit.title" defaultMessage="Edit my project" />
        </h1>
        <Link href={urlBack}>
          <a>
            {/* go back link */}
            <FontAwesomeIcon icon="arrow-left" />
            <Translate id="project.edit.back" defaultMessage="Go back" />
          </a>
        </Link>
        {/* TODO switch to custom nav 'same as program' */}
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <Translate id="entity.tab.basic_info" defaultMessage="Basic information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <Translate id="entity.tab.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#needs" data-toggle="tab">
            <Translate id="entity.tab.needs" defaultMessage="Needs" />
          </a>
          <a className="nav-item nav-link" href="#documents" data-toggle="tab">
            <Translate id="entity.tab.documents" defaultMessage="Documents" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <Translate id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content">
            {/* Basic info tab */}
            <div className="tab-pane active" id="basic_info">
              <ProjectForm
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                mode="edit"
                project={project}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </div>

            {/* Members tab */}
            <div className="tab-pane" id="members">
              {router.query.id ? (
                <MembersList
                  itemType="projects"
                  itemId={parseInt(router.query.id as string)}
                  isOwner={project?.is_owner}
                />
              ) : // eslint-disable-next-line @rushstack/no-null
              null}
            </div>

            {/* Needs tab */}
            <div className="tab-pane" id="needs">
              {project && (
                <>
                  {!!user && <NeedCreate projectId={project.id} refresh={needsRevalidate} />}
                  <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
                    {dataNeeds &&
                      [...dataNeeds]
                        ?.reverse()
                        .map((need, i) => (
                          <NeedCard
                            key={i}
                            title={need.title}
                            skills={need.skills}
                            resources={need.ressources}
                            hasSaved={need.has_saved}
                            id={need.id}
                            postsCount={need.posts_count}
                            membersCount={need.members_count}
                            publishedDate={need.created_at}
                            dueDate={need.end_date}
                            status={need.status}
                          />
                        ))}
                  </Grid>
                </>
              )}
            </div>

            {/* Documents tab */}
            <div className="tab-pane" id="documents">
              {project && (
                <DocumentsManager
                  documents={project.documents}
                  isAdmin={project.is_admin}
                  itemId={project.id}
                  itemType="projects"
                />
              )}
            </div>

            {/* Advanced tab */}
            <div className="tab-pane" id="advanced">
              <h5>
                <Translate id="attach.myproject.title" defaultMessage="Submit my project to a challenge" />
              </h5>
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <LinkChallengeModal
                        project={project}
                        closeModal={() => setIsOpen(false)}
                        setProject={setProject}
                      />
                    ),
                    title: 'Submit my project to a challenge',
                    titleId: 'attach.myproject.title',
                    maxWidth: '50rem',
                  });
                }}
              >
                <Translate id="attach.myproject.btn" defaultMessage="Submit my project" />
              </Button>
              {project && project.challenges.length !== 0 && (
                <>
                  <Box pt={8}>
                    <h5>
                      <Translate id="project.challenges.attached" defaultMessage="Challenges attached" />
                    </h5>
                  </Box>
                  <Carousel spaceX={12}>
                    {project.challenges.map((challenge, index) => (
                      <ChallengeMiniCard
                        key={index}
                        icon={challenge.banner_url_sm}
                        title={challenge.title}
                        shortTitle={challenge.short_title}
                        status={challenge.project_status}
                        projectId={project.id}
                        projectChallengeId={challenge.id}
                        onChallengeDelete={onChallengeDelete}
                        isEditCard
                      />
                    ))}
                  </Carousel>
                </>
              )}
              <hr />

              <h5>
                <Translate id="hook.setup" defaultMessage="Set up hooks" />
              </h5>
              <p className="hookExplain">
                <Translate
                  id="hook.explanation"
                  defaultMessage="Send custom message to any of you slack's channel, on certain trigger on your project (new need, new post, new member). For this, you will need a special hook url. Please follow {tutorial} to get this url"
                  values={{
                    tutorial: (
                      <a href="https://api.slack.com/messaging/webhooks" target="_blank" rel="noopener noreferrer">
                        <Translate id="hook.tutorial" defaultMessage="this tutorial" />
                      </a>
                    ),
                  }}
                />
              </p>
              <div className="hooksContainer">{project && <WebHooks itemId={router.query.id} />}</div>
              <hr />
              <ManageExternalLink itemType="projects" itemId={router.query.id} />
              <hr />

              <h5>
                <Translate id="project.advancedParam" defaultMessage="Advanced parameters" />
              </h5>
              <div className="deleteBtns">
                {project && project?.members_count > 1 && (
                  <p>
                    <Translate
                      id="project.delete.explain"
                      defaultMessage="To delete a project, you must first remove all members of the project except you."
                    />
                  </p>
                )}
                {project && (
                  <Button
                    onClick={() => {
                      showModal({
                        children: (
                          <>
                            {errors && <Alert type="danger" message={errorMessage} />}
                            <P fonSize="1rem">
                              <Translate id={delBtnTextId} defaultMessage={delBtnText} />
                            </P>
                            <div tw="inline-flex space-x-3">
                              <Button btnType="danger" onClick={deleteProj}>
                                <FormattedMessage id="general.yes" defaultMessage="Yes" />
                              </Button>
                              <Button onClick={() => setIsOpen(false)}>
                                <FormattedMessage id="general.no" defaultMessage="No" />
                              </Button>
                            </div>
                          </>
                        ),
                        title: delBtnTitle,
                        titleId: delBtnTitleId,
                        maxWidth: '30rem',
                      });
                    }}
                    btnType="danger"
                  >
                    <Translate id={delBtnTitleId} defaultMessage={delBtnTitle} />
                  </Button>
                )}
                {project && project.members_count > 1 && (
                  <Button btnType="danger" disabled tw="ml-3">
                    <Translate id="project.delete.title" defaultMessage="Delete project" />
                  </Button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* </Loading> */}
    </Layout>
  );
};

interface PropsModal {
  project: { id: number; challenges: Challenge[] };
  closeModal: () => void;
  setProject: React.Dispatch<any>;
}
const LinkChallengeModal: FC<PropsModal> = ({ project, closeModal, setProject }) => {
  const alreadyLinkedChallenges: any[] = project?.challenges ? project?.challenges : [];
  const { data: dataChallenges } = useGet('/api/challenges');
  const [selectedChallenge, setSelectedChallenge] = useState<
    undefined | { id: number; is_member: boolean; program: Program }
  >();
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();

  // Filter the projects that are already in this challenge so you don't add it twice!
  const filteredChallenges = useMemo(() => {
    if (dataChallenges) {
      return (
        dataChallenges
          // .filter((challenge) => challenge.project_status === 'accepting')
          .filter((challenge) => {
            // Check if my project is found in alreadyLinkedProjects
            const isChallengeAlreadyLinked = alreadyLinkedChallenges.find((alreadyLinkedChallenge) => {
              return alreadyLinkedChallenge.id === challenge.id;
            });
            // We keep only the ones that are not present
            return !isChallengeAlreadyLinked;
          })
      );
    }
  }, [dataChallenges, alreadyLinkedChallenges]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this project to the challenge then mutate the cache of the projects from the parent prop
    if (selectedChallenge?.id) {
      await api
        .put(`/api/challenges/${selectedChallenge?.id}/projects/${project.id}`)
        .catch(() =>
          console.error(`Could not PUT/link challengeId=${selectedChallenge?.id} with project projectId=${project.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      // mutateProject({ data: { ...project, challenges: [...project.challenges, selectedChallenge] } });
      const newProject = { ...project, challenges: [...project.challenges, selectedChallenge] };
      setProject(newProject); // update challenges list
      !selectedChallenge.is_member && api.put(`/api/challenges/${selectedChallenge?.id}/join`); // join the challenge if user is not member already
      api.put(`/api/challenges/${selectedChallenge?.id}/follow`); // then follow it
      api.put(`/api/programs/${selectedChallenge?.program.id}/follow`); // and follow its program
      setTimeout(() => {
        // close modal after 3.5sec
        closeModal();
      }, 3500);
    }
  };
  const onProjectSelect = (e) => {
    setSelectedChallenge(filteredChallenges.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      {filteredChallenges && filteredChallenges.length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredChallenges
            // filter to only get challenges with status accepting, and challenges that are attached to a program (id ≠ -1)
            .filter(({ status, program }) => status === 'accepting' && program.id !== -1)
            .map((project, index) => (
              <div className="form-check" key={index} style={{ height: '60px' }}>
                <input
                  type="radio"
                  className="form-check-input"
                  name="exampleRadios"
                  id={`project-${index}`}
                  value={project.id}
                  onChange={onProjectSelect}
                />
                <label className="form-check-label" htmlFor={`project-${index}`}>
                  {project.title}
                  <br />
                  <Box opacity=".6">
                    <Translate id="entity.info.program.title" defaultMessage="Program: " />"{project.program.title}"
                  </Box>
                </label>
              </div>
            ))}
          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="attach.myproject.btn" defaultMessage="Submit my project" />
              </>
            </Button>
            {requestSent && (
              <Alert
                type="success"
                message={
                  <FormattedMessage
                    id="attach.project.success"
                    defaultMessage="The project has been sent. He will be examined by the challenge team to validate his commitment to the challenge."
                  />
                }
              />
            )}
          </div>
        </form>
      ) : (
        <Loading />
      )}
    </div>
  );
};

ProjectEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/projects/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch project with id=${query.id}`, err));
  // Check if it got the project and if the user is admin
  if (res?.data?.is_admin) return { project: res.data };
  isomorphicRedirect(ctx, '/search/projects');
};

export default ProjectEdit;
