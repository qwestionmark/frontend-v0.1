import ReactGA from 'react-ga';
import Router from 'next/router';
import { FormattedMessage, useIntl } from 'react-intl';
import { NextPage } from 'next';
import { useContext, useState } from 'react';
import ProjectForm from '~/components/Project/ProjectForm';
import Layout from '~/components/Layout';
import { UserContext } from '~/contexts/UserProvider';
import { useApi } from '~/contexts/apiContext';

const ProjectCreate: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const { formatMessage } = useIntl();
  const [newProject, setNewProject] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'active',
    interests: [],
    skills: [],
    banner_url: '',
    is_private: false, // force all new projects to be private (TEMP @TOFIX)
  });
  const [sending, setSending] = useState(false);

  const handleChange = (key, content) => {
    setNewProject((prevProject) => ({ ...prevProject, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/projects/', { project: newProject })
      .then((res) => {
        setSending(false);
        const userId = res.config.headers.userId;
        // record event to Google Analytics
        ReactGA.event({ category: 'Project', action: 'create', label: `[${userId},${res.data.id}]` /* proj id */ });
        // follow the object after having created it
        api.put(`/api/projects/${res.data.id}/follow`);
        // go to the created project edition page
        Router.push(`/project/${res.data.id}/edit`);
      })
      .catch((err) => {
        console.error("Couldn't post new project", err);
        setSending(false);
        // if error response data is that shortTitle is already taken, show message in an alert warning
        if (err.response.data.data === 'ShortTitle is already taken') {
          alert(formatMessage({ id: 'err-4006', defaultMessage: 'Short title is already taken' }));
        }
      });
  };
  return (
    <Layout title={`${formatMessage({ id: 'project.create.title', defaultMessage: 'Create a new project' })} | JOGL`}>
      <div className="projectCreate container-fluid">
        <h1>
          <FormattedMessage id="project.create.title" defaultMessage="Create a new project" />
        </h1>
        <ProjectForm
          mode="create"
          project={newProject}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          sending={sending}
        />
      </div>
    </Layout>
  );
};

export default ProjectCreate;
