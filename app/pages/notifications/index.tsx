import { useContext, useState, useEffect, useCallback } from 'react';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import { UserContext } from '~/contexts/UserProvider';
import A from '~/components/primitives/A';
import P from '~/components/primitives/P';
import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import Button from '~/components/primitives/Button';
import Loading from '~/components/Tools/Loading';
import { displayObjectRelativeDate } from '~/utils/utils';
import { NotifIconWrapper } from '~/utils/notificationsIcons';
import { notifIconTypes } from '../../utils/notificationsIcons';
import { useRouter } from 'next/router';
import tw, { styled } from 'twin.macro';

const NotificationPage = () => {
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);
  const [totalNotificationsCount, setTotalNotificationsCount] = useState(0);
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [offSetCursor, setOffSetCursor] = useState();
  const numberOfNotifPerCall = 30;
  const api = useApi();
  const router = useRouter();
  const { user, isConnected } = useContext(UserContext);

  const loadNotifications = useCallback(() => {
    setLoadingBtn(true);
    const pageScroll = window.pageYOffset; // get whole page offSetTop
    const paginationFilters = !offSetCursor // if we don't have offSetCursor, it means it's first call, so call only first number of wanted notifications
      ? `first: ${numberOfNotifPerCall}`
      : // else, it has been set, so call wanted number of notifications, after the last one (so we have a pagination)
        `first: ${numberOfNotifPerCall} after: "${offSetCursor}"`;
    user &&
      api
        .post('/api/graphql/', {
          query: `
          query {
            user (id: ${user.id} ) {
              id,
              email,
              notifications(${paginationFilters}) {
                unreadCount
                totalCount
                edges {
                  node {
                    id
                    link
                    read
                    subjectLine
                    createdAt
                    category
                  }
                  cursor
                }
              }
            }
          }
        `,
        })
        .then((response) => {
          var userNotifications = response.data.data.user.notifications;
          // set offset cursor to the last notification cursor value, so we can call the next notifications after that one
          setOffSetCursor(userNotifications?.edges[userNotifications.edges.length - 1].cursor);
          // update notification counts
          setUnreadNotificationsCount(userNotifications.unreadCount);
          setTotalNotificationsCount(userNotifications.totalCount);
          // concatenate current notifications with the new ones
          setNotifications([...notifications, ...userNotifications.edges.map((edge) => edge.node)]);
          window.scrollTo(0, pageScroll); // force to stay at pageScroll (else it would focus you at bottom of the list of posts)
          setLoadingBtn(false);
        })
        .catch((err) => {});
  }, [user, notifications]);

  useEffect(() => {
    // get first notifications on first load
    loadNotifications();
    // redirect to homepage if user is not connected
    if (!isConnected) router.push('/');
  }, [user]);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      })
      .catch((err) => {
        // do something
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      })
      .catch((err) => {
        // do something
      });
  };

  return (
    <Layout title="Notifications | JOGL">
      <div tw="px-2 max-w-3xl w-full m-auto">
        <div tw="flex flex-row justify-between items-center py-2">
          <div tw="font-bold">
            <FormattedMessage id="settings.notifications.title" defaultMessage="Notifications" />
          </div>
          <div tw="flex flex-row items-end">
            <div onClick={() => markAllNotificationsAsRead()}>
              <A href="#">
                <FormattedMessage id="settings.notifications.markAsRead" defaultMessage="Mark All as read" />
              </A>
            </div>
            <div tw="px-1">.</div>
            <Link href={`/user/${user?.id}/settings`} passHref>
              <A>
                <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
              </A>
            </Link>
          </div>
        </div>
        {notifications?.length === 0 ? (
          <Loading />
        ) : (
          <div tw="flex flex-col border border-solid border-gray-600">
            {notifications &&
              notifications?.map((notification) => {
                return (
                  <Notification
                    href={notification.link}
                    key={notification.id}
                    hasRead={notification.read}
                    onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                  >
                    <div tw="flex flex-row ">
                      <div tw="flex pr-2">
                        <NotifIconWrapper>{notifIconTypes[notification.category]}</NotifIconWrapper>
                      </div>
                      <div tw="flex flex-wrap flex-row">
                        <P mr={2} mb={0}>
                          {notification.subjectLine}
                        </P>
                        <P mb={0} color="#797979">
                          {displayObjectRelativeDate(notification.createdAt)}
                        </P>
                      </div>
                    </div>
                  </Notification>
                );
              })}
          </div>
        )}
        {notifications.length !== totalNotificationsCount && (
          <div tw="self-center mt-4">
            <Button onClick={() => loadNotifications()} disabled={loadingBtn}>
              {loadingBtn && (
                <>
                  <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                  &nbsp;
                </>
              )}
              <FormattedMessage id="general.load" defaultMessage="Load more" />
            </Button>
          </div>
        )}
      </div>
    </Layout>
  );
};

const Notification = styled.a`
  background: ${({ hasRead }) => (hasRead ? 'white' : '#EDF2FA')};
  ${tw`border-0 border-b border-gray-300 border-solid`}
  ${tw`py-2.5 px-3`}
  &:hover {
    background: ${({ hasRead }) => (hasRead ? '#f6f6f6' : '#e2e7ee')};
    color: inherit;
    text-decoration: none;
  }
`;

export default NotificationPage;
