import Modal from 'react-modal';

const oldFn = Modal.setAppElement;
Modal.setAppElement = (element) => {
  if (element === '#__next') {
    // otherwise it will throw aria warnings.
    return oldFn(document.createElement('div'));
  }
  oldFn(element);
};
export default Modal;
