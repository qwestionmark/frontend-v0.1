import { useTheme as useEmotionTheme } from '@emotion/react';

type ListWithAliases<A> = string[] & Record<keyof A, string>;

/**
 * Helper function to create an array with additional properties from a specific type since this
 * paradigm is used often in `styled-system`.
 * @param list
 * @param keys
 */
const makeListWithAliases = <A>(aliases: Record<keyof A, string>, ...keys: Array<keyof A>): ListWithAliases<A> => {
  const list = keys.reduce<string[]>((prev, curr) => [...prev, aliases[curr]], []);
  return Object.assign(list, aliases);
};

//
// Breakpoints
//

const breakpointsAliases = {
  sm: '40em' /* 1 => '> 640px' or sm  */,
  md: '48em' /* 2 => '> 768px' or md  */,
  lg: '64em' /* 3 => '> 1024px' or lg */,
  xl: '80em' /* 4 => '> 1280px' or xl */,
};

const breakpoints = makeListWithAliases(breakpointsAliases, 'sm', 'md', 'lg', 'xl');

//
// Font sizes
//

const fontSizesAliases = {
  /* 0 => 12px or xs   */ xs: '0.75rem',
  /* 1 => 14px or sm   */ sm: '0.875rem',
  /* 2 => 16px or base */ base: '1rem',
  /* 3 => 18px or lg   */ lg: '1.125rem',
  /* 4 => 20px or xl   */ xl: '1.25rem',
  /* 5 => 22px or 2xl  */ '2xl': '1.375rem',
  /* 6 => 24px or 3xl  */ '3xl': '1.5rem',
  /* 7 => 26px or 4xl  */ '4xl': '1.625rem',
  /* 8 => 30px or 5xl  */ '5xl': '1.875rem',
  /* 9 => 36px or 6xl  */ '6xl': '2.25rem',
  /* 10 => 48px or 7xl */ '7xl': '3rem',
};

const fontSizes = makeListWithAliases(
  fontSizesAliases,
  'xs',
  'sm',
  'base',
  'lg',
  'xl',
  '2xl',
  '3xl',
  '4xl',
  '5xl',
  '6xl',
  '7xl'
);

//
// Fonts
//

const fontsAliases = { primary: "'Roboto', sans-serif", secondary: "'Bebas Neue', sans-serif" };

const fonts = makeListWithAliases(fontsAliases, 'primary', 'secondary');

//
// Sizes
//

// Spaces are for margin, padding, margin-top, margin-right, margin-bottom, margin-left, padding-top, padding-right, padding-bottom, padding-left, grid-gap, grid-column-gap, grid-row-gap
const space = [
  // margin and padding
  /* 0  => 0px   */ '0rem',
  /* 1  => 4px   */ '0.25rem',
  /* 2  => 8px   */ '0.5rem',
  /* 3  => 12px  */ '0.75rem',
  /* 4  => 16px  */ '1rem',
  /* 5  => 20px  */ '1.25rem',
  /* 6  => 24px  */ '1.5rem',
  /* 7  => 32px  */ '2rem',
  /* 8  => 40px  */ '2.5rem',
  /* 9  => 48px  */ '3rem',
  /* 10 => 64px  */ '4rem',
  /* 11 => 80px  */ '5rem',
  /* 12 => 96px  */ '6rem',
  /* 13 => 128px */ '8rem',
  /* 14 => 160px */ '10rem',
  /* 15 => 192px */ '12rem',
  /* 16 => 224px */ '14rem',
  /* 17 => 256px */ '16rem',
];

// Size are for width, height, min-width, max-width, min-height, max-height
// Sizes aliases
const sizes = Object.assign(space, {
  auto: 'auto',
  '1/2': '50%',
  '1/3': '33.333333%',
  '2/3': '66.666667%',
  '1/4': '25%',
  '2/4': '50%',
  '3/4': '75%',
  '1/5': '20%',
  '2/5': '40%',
  '3/5': '60%',
  '4/5': '80%',
  '1/6': '16.666667%',
  '2/6': '33.333333%',
  '3/6': '50%',
  '4/6': '66.666667%',
  '5/6': '83.333333%',
  '1/12': '8.333333%',
  '2/12': '16.666667%',
  '3/12': '25%',
  '4/12': '33.333333%',
  '5/12': '41.666667%',
  '6/12': '50%',
  '7/12': '58.333333%',
  '8/12': '66.666667%',
  '9/12': '75%',
  '10/12': '83.333333%',
  '11/12': '91.666667%',
  full: '100%',
  screen: '100vw',
});

//
// Radii
//

// Radii is for border-radius
const radiiAliases = {
  /* 0 => 0px or none    */ none: '0',
  /* 1 => 2px or sm      */ sm: '0.125rem',
  /* 2 => 4px or default */ default: '0.25rem',
  /* 3 => 6px or md      */ md: '0.375rem',
  /* 4 => 8px or lg      */ lg: '0.5rem',
  /* 5 => 16px or xl     */ xl: '1rem',
  /* 6 => 9999px or full */ full: '9999px',
};

const radii = makeListWithAliases(radiiAliases, 'none', 'sm', 'default', 'md', 'lg', 'xl', 'full');

//
// Colors
//

// some scales are generated with this => https://gka.github.io/palettes/#/9|s|ffc107|ffffe0,ff005e,93003a|0|0
const greysAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#F7F8F9',
  /* 2 => 200 */ '200': '#DCE3E9',
  /* 3 => 300 */ '300': '#CFD8E1',
  /* 4 => 400 */ '400': '#C5CED5',
  /* 5 => 500 */ '500': '#A4B6C5',
  /* 6 => 600 */ '600': '#8694A7',
  /* 7 => 700 */ '700': '#626E7E',
  /* 8 => 800 */ '800': '#38424F',
  /* 9 => 900 */ '900': '#1E242D ',
};

const greysList = makeListWithAliases(greysAliases, '0', '100', '200', '300', '400', '500', '600', '700', '800', '900');

const primariesAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#00265e',
  /* 2 => 200 */ '200': '#003c78',
  /* 3 => 300 */ '300': '#005494',
  /* 4 => 400 */ '400': '#006db0',
  /* 5 => 500 */ '500': '#2987cd',
  /* 6 => 600 */ '600': '#51a2eb',
  /* 7 => 700 */ '700': '#73bfff',
  /* 8 => 800 */ '800': '#93dcff',
  /* 9 => 900 */ '900': '#b3faff',
};

const primariesList = makeListWithAliases(
  primariesAliases,
  '0',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900'
);

const successesAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#002700',
  /* 2 => 200 */ '200': '#003a00',
  /* 3 => 300 */ '300': '#005400',
  /* 4 => 400 */ '400': '#006f0e',
  /* 5 => 500 */ '500': '#008b2b',
  /* 6 => 600 */ '600': '#28a745',
  /* 7 => 700 */ '700': '#4dc460',
  /* 8 => 800 */ '800': '#6de27b',
  /* 9 => 900 */ '900': '#8bff9733',
};

const successesList = makeListWithAliases(
  successesAliases,
  '0',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900'
);

const dangersAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#5c0000',
  /* 2 => 200 */ '200': '#7a0000',
  /* 3 => 300 */ '300': '#9a0018',
  /* 4 => 400 */ '400': '#bb032e',
  /* 5 => 500 */ '500': '#dc3545',
  /* 6 => 600 */ '600': '#fe565e',
  /* 7 => 700 */ '700': '#ff7478',
  /* 8 => 800 */ '800': '#ff9293',
  /* 9 => 900 */ '900': '#ffb0af52',
};

const dangersList = makeListWithAliases(
  dangersAliases,
  '0',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900'
);

const warningsAliases = {
  /* 0 => null, don't use it */ '0': 'white',
  /* 1 => 100 */ '100': '#3d0d00',
  /* 2 => 200 */ '200': '#4d2600',
  /* 3 => 300 */ '300': '#653d00',
  /* 4 => 400 */ '400': '#805600',
  /* 5 => 500 */ '500': '#9e6f00',
  /* 6 => 600 */ '600': '#be8900',
  /* 7 => 700 */ '700': '#dea500',
  /* 8 => 800 */ '800': '#ffc107',
  /* 9 => 900 */ '900': '#ffde39',
};

const warnings = makeListWithAliases(
  warningsAliases,
  '0',
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900'
);

// Colors are for color, background-color, border-color
const colors = {
  // TODO: Add colors from Figma.
  primary: primariesList[5],
  primaries: primariesList,
  secondary: '#6c757d',
  success: successesList[6],
  successes: successesList,
  danger: dangersList[5],
  dangers: dangersList,
  warning: dangersList[8],
  warnings,
  lightBlue: '#F1F4F8',
  grey: greysList[4],
  greys: greysList,
  pink: '#DE3E96',
  lightPink: '#ffc0cb',
  turquoise: '#1CB5AC',
  dark: 'rgba(0, 0, 0, 0.7)',
  orange: '#F0712C',
  lightGrey: '#fafafa80',
  darkBlue: '#4F4BFF',
  violet: '#643CC6',
};

//
// Shadows
//

const shadowsAliases = {
  /* 0 => xs      */ xs: '0 0 0 1px rgba(0, 0, 0, 0.05)',
  /* 1 => sm      */ sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
  /* 2 => default */ default: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
  /* 3 => md      */ md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
  /* 4 => lg      */ lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
  /* 5 => xl      */ xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
  /* 6 => '2xl'   */ '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.25)',
};

const shadows = Object.assign(makeListWithAliases(shadowsAliases, 'xs', 'sm', 'default', 'md', 'lg', 'xl', '2xl'), {
  inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
  outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
  none: 'none',
  light: '1px 2px 8px rgba(0,0,0,0.1)',
});

//
// Theme
//

const { sm, md, lg, xl } = breakpoints;

const theme = {
  fontSizes,
  space,
  breakpoints,
  radii,
  shadows,
  sizes,
  colors,
  fonts,
  mediaQueries: {
    sm: `@media screen and (min-width: ${sm})`,
    md: `@media screen and (min-width: ${md})`,
    lg: `@media screen and (min-width: ${lg})`,
    xl: `@media screen and (min-width: ${xl})`,
  },
  ...breakpoints,
};

export type Theme = typeof theme;

export const useTheme = () => useEmotionTheme<typeof theme>();

export default theme;
