import { CommentDots } from '@emotion-icons/boxicons-regular';
import { Feed } from '@emotion-icons/icomoon';
import { PersonPlusFill } from '@emotion-icons/bootstrap';
import { Mention } from '@emotion-icons/octicons';
import { AdminPanelSettings, Event } from '@emotion-icons/material';
import { UserGroup } from '@emotion-icons/heroicons-outline';
import { SignLanguage } from '@emotion-icons/fa-solid';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import styled from '~/utils/styled';

export const NotifIconWrapper = styled.div`
  ${EmotionIconBase} {
    color: ${(p) => p.theme.colors.primary};
    width: 25px;
    height: 25px;
  }
`;

export const notifIconTypes = {
  administration: <AdminPanelSettings />,
  clap: <SignLanguage />,
  comment: <CommentDots />,
  follow: <PersonPlusFill />,
  feed: <Feed />,
  program: <Event />,
  mention: <Mention />,
  membership: <UserGroup />,
  space: <Event />,
};
