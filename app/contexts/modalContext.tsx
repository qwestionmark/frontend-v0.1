import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Router } from 'next/router';
import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';
import { FormattedMessage } from 'react-intl';
import Modal from 'react-modal';
import { LayoutProps } from 'styled-system';
import Box from '~/components/Box';
import P from '~/components/primitives/P';
import styled from '~/utils/styled';
import styles from './modalContext.module.scss';

interface ShowModal {
  children: ReactNode;
  title: string;
  titleId?: string;
  values?: any;
  maxWidth?: LayoutProps['maxWidth'];
  showTitle?: boolean;
  showCloseButton?: boolean;
  allowOverflow?: boolean;
}

export interface IModalContext {
  setModalChildren: Dispatch<SetStateAction<ReactNode>>;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ children, maxWidth, title, titleId, values, showCloseButton, showTitle }: ShowModal) => void;
  isOpen: boolean;
}
// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const ModalContext = createContext<IModalContext | undefined>(undefined);

export function ModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [modalChildren, setModalChildren] = useState<ReactNode>(
    <div>You haven\&apos;t provided any modalChildren</div>
  );
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [allowOverflow, setAllowOverflow] = useState<boolean>(false);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [modalTitleId, setModalTitleId] = useState<undefined | string>(undefined);
  const [modalTitleValues, setModalTitleValues] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<LayoutProps['maxWidth']>('30rem');

  const showModal = useCallback(
    ({
      children,
      maxWidth = '30rem',
      title,
      titleId,
      values,
      showCloseButton = true,
      showTitle = true,
      allowOverflow = false,
    }: ShowModal) => {
      setModalChildren(children);
      setIsOpen(true);
      setModalTitle(title);
      setModalTitleId(titleId);
      setModalTitleValues(values);
      setShowTitle(showTitle);
      setAllowOverflow(allowOverflow);
      setShowCloseButton(showCloseButton);
      setContentLabel(title);
      setMaxWith(maxWidth);
    },
    []
  );
  const closeModal = (value: boolean = true) => setIsOpen(!value);
  // Memoize value so it doesn't re-render the tree
  const value: IModalContext = useMemo(() => ({ setIsOpen, showModal, setModalChildren, isOpen, closeModal }), [
    setIsOpen,
    closeModal,
    showModal,
    setModalChildren,
    isOpen,
  ]);
  // force closing modal when soft changing route
  Router.events.on('routeChangeStart', closeModal);
  return (
    <ModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={() => setIsOpen(false)}
        className={styles.modal}
        overlayClassName={styles.overlay}
      >
        <Box maxWidth={maxWidth} width={['90vw', '80vw']}>
          {(showTitle || showCloseButton) && (
            <Box justifyContent="space-between" alignItems="center" row p={4} pr={2} borderBottom="1px solid lightgrey">
              <P flexGrow={1} color="#212529" fontWeight="500" fontSize={'1.3rem'} mb={0}>
                {/* if rawTitle if false, show intl translation title */}
                {showTitle && modalTitle && modalTitleId && (
                  <FormattedMessage id={modalTitleId} defaultMessage={modalTitle} values={modalTitleValues} />
                )}
                {/* else, show raw title passed to modal */}
                {showTitle && modalTitle && !modalTitleId && modalTitle}
              </P>
              {showCloseButton && (
                <Box as="button" onClick={() => setIsOpen(false)}>
                  {/* <CloseButton icon={['far', 'times-circle']} /> */}
                  <CloseButton icon="times" />
                </Box>
              )}
            </Box>
          )}
          {/* To be able to scroll in the modal content (not it's header), we need wrap modalChildren with a ModalFrame that has a set height and overflow-y:auto, and then by a div with overflow:scroll */}
          <ModalFrame overflowY={allowOverflow ? 'visible' : 'auto'}>
            {/* Can allow content to overflow for some modals */}
            <Box p={4} overflow={allowOverflow ? 'visible' : 'scroll'}>
              {modalChildren}
            </Box>
          </ModalFrame>
        </Box>
      </Modal>
      {children}
    </ModalContext.Provider>
  );
}

const CloseButton = styled(FontAwesomeIcon)`
  color: #868686;
  font-size: 1.5rem;
  &:hover {
    color: #212529;
  }
`;

const ModalFrame = styled(Box)`
  max-height: calc(95vh - 5rem);
`;

export const useModal = () => useContext(ModalContext);
export default ModalContext;
