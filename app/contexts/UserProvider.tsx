import cookie from 'js-cookie';
import Router from 'next/router';
import React, { createContext, Dispatch, FC, SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import { Credentials, User } from '~/types';

interface IUserContext {
  checkUserTokens: () => Promise<unknown>;
  frontLogout: (redirectPath?: string, modalOnly?: boolean) => void;
  signIn: (email: any, password: any) => Promise<unknown>;
  logout: (redirectPath?: any) => Promise<void>;
  isConnected: boolean;
  credentials: Credentials;
  user: User | undefined;
}
export const UserContext = createContext<IUserContext | undefined>(undefined);

export const frontLogout = (redirectPath = '/signin', modalOnly = false) => {
  // Delete all the auth cookies
  cookie.remove('access-token');
  cookie.remove('client');
  cookie.remove('uid');
  cookie.remove('userId');
  // to support logging out from all windows
  // This will work with NextJS since it's call in the browser only.
  window.localStorage.setItem('logout', Date.now().toString());
  Router.push(redirectPath);
};
interface ProviderProps {
  credentials: Credentials;
  setCredentials: Dispatch<SetStateAction<Credentials>>;
}
const UserProvider: FC<ProviderProps> = ({ children, credentials, setCredentials }) => {
  const api = useApi();
  const [isConnected, setIsConnected] = useState(
    !!credentials.accessToken && !!credentials.uid && !!credentials.client
  );
  const { data: user, error: userError, mutate: userMutate } = useGet(
    credentials.userId && `/api/users/${credentials.userId}`
  );

  if (userError) {
    // TODO Redirect to login page?
    console.error({ userError });
  }
  useEffect(() => {
    if (!!credentials.accessToken && !!credentials.uid && !!credentials.client) {
      setIsConnected(true);
    } else {
      setIsConnected(false);
    }
  }, [credentials.accessToken, credentials.uid, credentials.client]);

  const logout = useCallback(
    (redirectPath = '/signin') =>
      api
        .delete('/api/auth/sign_out')
        .then(() => {
          frontLogout(redirectPath);
          setIsConnected(false);
          userMutate(undefined, false);
        })
        .catch((error) => {
          if (error.response.status === 404) {
            frontLogout(redirectPath);
            setIsConnected(false);
            userMutate(undefined, false);
          }
        }),
    [api]
  );

  const checkUserTokens = useCallback(
    () =>
      new Promise((resolve, reject) => {
        // Control if user is connected and always available
        if (credentials.accessToken) {
          api
            .get('/api/auth/validate_token')
            .then((res) => {
              // Validating tokens
              if (res.status === 401) {
                // Tokens are not valid
                logout();
                reject(new Error('Tokens are not valid'));
              }
              if (res.status === 200) {
                // Valid tokens !
                resolve();
              }
            })
            .catch((error) => reject(error));
        } else {
          reject();
        }
      }),
    [api, credentials.accessToken, logout]
  );

  const signIn = useCallback(
    async (email, password) =>
      new Promise((resolve, reject) => {
        api
          .post('/api/auth/sign_in', { email, password })
          .then((res) => {
            const expires = 7; // In days
            cookie.set('access-token', res.headers[Object.keys(res.headers)[0]], { expires });
            cookie.set('uid', res.headers.uid, { expires });
            cookie.set('client', res.headers.client, { expires });
            cookie.set('userId', res.data.data.id, { expires });
            // Sync login between pages
            window.localStorage.setItem('login', Date.now().toString());
            setCredentials({
              accessToken: res.headers[Object.keys(res.headers)[0]],
              client: res.headers.client,
              uid: res.headers.uid,
              userId: res.data.data.id,
            });
            userMutate(res.data.data, false);
            // Eventually redirect to an other page here
            resolve(res.data.data);
          })
          .catch((error) => {
            // TODO: Comment what this line does
            const { response: { data: { errors = [] } = {} } = {} } = error;
            // console.warn(errors);
            reject(error);
          });
      }),
    [api, setCredentials]
  );

  const providerValue = useMemo(
    () => ({ checkUserTokens, frontLogout, signIn, logout, isConnected, credentials, user }),
    [checkUserTokens, signIn, logout, isConnected, credentials, user]
  );

  return <UserContext.Provider value={providerValue}>{children}</UserContext.Provider>;
};
export default UserProvider;
