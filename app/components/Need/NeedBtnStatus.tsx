import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import ReactGA from 'react-ga';
import useUser from '~/hooks/useUser';

export default function NeedBtnStatus({ need }) {
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { user } = useUser();

  const changeStatus = () => {
    // set new need status
    const newStatus = need.status === undefined || need.status === 'active' ? 'completed' : 'active';
    setSending(true);
    api
      .patch(`/api/needs/${need.id}`, { status: newStatus })
      .then(() => {
        setSending(false);
        // send event to google analytics (only if user is closing the need)
        newStatus === 'completed' &&
          ReactGA.event({ category: 'Need', action: 'close', label: `[${user.id},${need.id}]` });
        document.querySelector('.btn.cancel').click(); // force click on cancel button to go back to normal view
      })
      .catch(() => setSending(false));
  };
  if (need && need.is_owner) {
    return (
      <button className="btn btn-warning btnStatus" onClick={changeStatus} type="button" disabled={sending}>
        {need.status === 'completed' && sending && (
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
            {' '}
            &nbsp;
          </span>
        )}
        {need.status === 'completed' && !sending && <FormattedMessage id="need.card.reopen" defaultMessage="Reopen" />}
        {need.status !== 'completed' && sending && (
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
            {' '}
            &nbsp;
          </span>
        )}
        {need.status !== 'completed' && !sending && <FormattedMessage id="need.card.close" defaultMessage="Close" />}
      </button>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
