import { FormattedMessage } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useApi } from '~/contexts/apiContext';
import { useRouter } from 'next/router';

export default function NeedDelete({ need }) {
  const api = useApi();
  const router = useRouter();
  const deleteNeed = () => {
    if (need) {
      api
        .delete(`api/needs/${need.id}`)
        .then(() => {
          // after deletion, show delete confirmation modal + redirect to project page
          alert('Deleted');
          router.push(`/project/${need.project.id}#needs`);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };
  return (
    <div onClick={deleteNeed} onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteNeed()} tabIndex={0}>
      <FontAwesomeIcon icon="trash" className="postDelete" />{' '}
      <FormattedMessage id="feed.object.delete" defaultMessage="Delete" />
    </div>
  );
}
