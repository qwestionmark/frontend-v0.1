// import Image from 'next/image';
import Link from 'next/link';
import React, { FC, ReactNode } from 'react';
import { LayoutProps, SpaceProps } from 'styled-system';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box, { BoxProps } from '../Box';
import Image2 from '../Image2';

interface IContainer {
  noMobileBorder: boolean;
}
const Container = styled(Box)<IContainer>`
  box-shadow: ${(p) => p.theme.shadows.default};
  border-radius: ${(p) => p.theme.radii.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  background: white;
  @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
    // ! Do not use !important
    box-shadow: ${(p) => (p.noMobileBorder ? 'none!important' : p.theme.shadows.default)};
    border: ${(p) => (p.noMobileBorder ? 'none!important' : p.theme.shadows.default)};
    border-bottom: ${(p) => (p.noMobileBorder ? `1px solid lightgrey!important` : 'none!important')};
    border-radius: ${(p) => (p.noMobileBorder ? '0px!important' : p.theme.radii.xl)};
  }
`;

const BannerImgContainer = styled(Box)<{ small: boolean }>`
  img {
    width: 100%;
    object-fit: cover;
    height: ${(p) => (p.small ? p.theme.sizes[13] : p.theme.sizes[14])};
    border-top-left-radius: ${(p) => p.theme.radii.xl};
    border-top-right-radius: ${(p) => p.theme.radii.xl};
    @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
      height: ${(p) => (p.small ? p.theme.sizes[12] : p.theme.sizes[13])};
    }
    &:hover {
      opacity: 0.9;
    }
  }
`;
interface CardProps extends BoxProps {
  imgUrl?: string;
  href?: string;
  isImgSmall?: boolean;
  children: ReactNode;
  noMobileBorder?: boolean;
  containerStyle?: any;
  spaceY?: SpaceProps['marginTop'];
  width?: LayoutProps['width'];
}

const Card: FC<CardProps> = ({
  imgUrl,
  children,
  href,
  isImgSmall,
  spaceY = [3, 4],
  noMobileBorder,
  width,
  ...props
}) => {
  const theme = useTheme();
  return (
    <Container noMobileBorder={noMobileBorder} {...props.containerStyle} width={width}>
      {/* if card has image and image is also a link */}
      {imgUrl && href && (
        <Link href={href}>
          <a>
            <BannerImgContainer small={isImgSmall}>
              <Image2 src={imgUrl} unsized />
            </BannerImgContainer>
          </a>
        </Link>
      )}
      <Box
        py={4}
        px={noMobileBorder ? [0, 4] : 4}
        flex="1"
        spaceY={spaceY}
        borderTop={imgUrl && `1px solid ${theme.colors.greys['300']}`}
        {...props}
      >
        {children}
      </Box>
    </Container>
  );
};
export default Card;
