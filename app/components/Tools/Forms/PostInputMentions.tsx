import { Component } from 'react';
import { MentionsInput, Mention } from 'react-mentions';
import algoliasearch from 'algoliasearch';

export default class PostInputMentions extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      content: '',
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.value);
  }

  searchAlgoliaObjects(objType, query, callback) {
    const appId = process.env.ALGOLIA_APP_ID; // algolia app id
    const token = process.env.ALGOLIA_TOKEN; // algolia token
    const client = algoliasearch(appId, token); // initialize algolia client
    const index = objType === 'user' ? client.initIndex('User') : client.initIndex('Project'); // set client index depending on objType
    index
      .search(query, {
        hitsPerPage: 10,
      })
      .then((content) => {
        const resultFormat =
          objType === 'user'
            ? // if objType is user, render a user specific object
              content.hits.map((obj) => ({
                id: obj.objectID, // user id
                display: `${obj.first_name} ${obj.last_name}`, // user full name
                name: obj.nickname, // user nickname
                imageUrl: obj.logo_url_sm, // user profile image
              }))
            : // else render project specific object
              content.hits
                .filter(({ status }) => status !== 'draft') // filter draft projects
                .map((obj) => ({
                  id: obj.objectID, // project id
                  display: obj.title, // project title
                  name: obj.short_title, // project short title
                  imageUrl: obj.banner_url_sm ? obj.banner_url_sm : '/images/default/default-project.jpg', // project banner image
                }));
        return resultFormat;
      })
      .then(callback);
  }

  renderSuggestion(objType, suggestion) {
    const type = objType === 'user' ? '@' : '#';
    return (
      <div className="suggestion-item">
        <img src={suggestion.imageUrl} alt="suggestion" />
        {suggestion.display}
        <span>{type + suggestion.name}</span>
      </div>
    );
  }

  render() {
    const { placeholder } = this.props;
    return (
      <div className="postInputMentions">
        <MentionsInput
          value={this.props.content}
          onChange={this.handleChange}
          className="suggestionsContainer"
          displayTransform={
            (_id, display) => display // return user full name, or entity title
          }
          markup="((__type__:__id__:__display__))"
          placeholder={placeholder}
        >
          <Mention
            type="@"
            trigger="@"
            renderSuggestion={this.renderSuggestion.bind(this, 'user')}
            data={this.searchAlgoliaObjects.bind(this, 'user')}
          />
          <Mention
            type="#"
            trigger="#"
            renderSuggestion={this.renderSuggestion.bind(this, 'project')}
            data={this.searchAlgoliaObjects.bind(this, 'project')}
          />
        </MentionsInput>
      </div>
    );
  }
}
