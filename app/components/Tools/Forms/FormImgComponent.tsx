// import Image from 'next/image';
import { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Image2 from '~/components/Image2';
import Alert from '~/components/Tools/Alert';
import BtnUploadFile from '~/components/Tools/BtnUploadFile';
import TitleInfo from '~/components/Tools/TitleInfo';
import { ItemType } from '~/types';
// import "./FormImgComponent.scss";
interface Props {
  defaultImg: string;
  id: string;
  imageUrl: string;
  itemId: number;
  itemType: ItemType;
  mandatory: boolean;
  maxSizeFile: number;
  onChange: (value: any) => void;
  title: string;
  tooltipMessage?: string;
  type: string;
  content?: any;
}
export default class FormImgComponent extends Component<Props> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      error: '',
      imageUrl: this.props.imageUrl,
      uploading: false,
    };
  }

  static get defaultProps() {
    return {
      defaultImg: '',
      id: '',
      imageUrl: '',
      itemId: '',
      itemType: '',
      mandatory: false,
      maxSizeFile: 3145728,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      title: 'Title',
      type: '',
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ imageUrl: nextProps.imageUrl });
  }

  handleChange(result) {
    if (this.props.id !== '') {
      if (result.error !== '') {
        this.setState({ error: result.error });
      } else {
        this.setState({ error: '' });
        if (result.url !== '') {
          this.props.onChange(this.props.id, result.url);
        }
      }
    } else {
      console.warn('id is missing (key to set)');
    }
  }

  render() {
    const { defaultImg, itemId, itemType, mandatory, title, tooltipMessage, type } = this.props;
    const { error, imageUrl, uploading } = this.state;

    let imgTodisplay = defaultImg;
    if (imageUrl) {
      imgTodisplay = imageUrl;
    }

    return (
      <div className="formImg">
        <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />
        <div className="btnUploadZone">
          {error !== '' && <Alert type="danger" message={error} />}
          {uploading ? (
            <div className="spinner-border text-secondary" role="status">
              <span className="sr-only">
                <FormattedMessage id="general.loading" defaultMessage="Loading..." />
              </span>
            </div>
          ) : (
            <BtnUploadFile
              uploadNow
              fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
              itemId={itemId}
              itemType={itemType}
              type={type}
              maxSizeFile={4194304}
              imageUrl={imageUrl}
              multiple={false}
              text={<FormattedMessage id="info-1002" defaultMessage="Choose a file" />}
              onChange={this.handleChange}
            />
          )}
        </div>
        <div className={`preview ${type}`}>
          <Image2 src={imgTodisplay} unsized loading="eager" />
        </div>
      </div>
    );
  }
}
