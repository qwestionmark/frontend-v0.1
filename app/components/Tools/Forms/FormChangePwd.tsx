import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import cookie from 'js-cookie';
import Alert from '~/components/Tools/Alert';
import { ApiContext } from '~/contexts/apiContext';

class FormChangePwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
      password: '',
      password_confirmation: '',

      error: '',
      /* fireRedirect: false, */
      sending: false,
      success: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  checkPassword(pwd, pwd_confirm) {
    if (pwd !== pwd_confirm) {
      this.setState({ error: 'err-4001' });
      return false;
    }
    if (pwd.length < 8) {
      this.setState({ error: 'err-4002' });
      return false;
    }
    return true;
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value, error: '' });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { old_password, password, password_confirmation } = this.state;
    const jsonToSend = {
      current_password: old_password,
      password,
      password_confirmation,
    };
    if (this.checkPassword(password, password_confirmation)) {
      this.setState({ error: '', sending: true });
      const api = this.context;
      api
        .put('/api/auth/password', jsonToSend)
        .then((res) => {
          cookie.set('access-token', res.headers[Object.keys(res.headers)[0]]);
          cookie.set('uid', res.headers.uid);
          this.setState({ sending: false, success: true });
        })
        .catch((error) => {
          const errorMessage = error.response.data.error;
          this.setState({ sending: false, error: errorMessage });
        });
    }
  }

  render() {
    const { error, sending, success } = this.state;
    const { intl } = this.props;
    const errorMessage = error.includes('err-') ? (
      <FormattedMessage id={error} defaultMessage="password not valid" />
    ) : (
      error
    );

    let formStyle;
    let messageStyle;
    let title;
    let msg;
    if (!this.props.isSent) {
      formStyle = { display: 'block' };
      messageStyle = { display: 'none' };
      title = {
        text: 'Password reset',
        id: 'auth.changePwd.title',
      };
      msg = {
        text: 'Please enter a new password.',
        id: 'auth.changePwd.description',
      };
    }

    return (
      <div className="form-content">
        <div className="form-header">
          <h2 className="form-title" id="signModalLabel">
            <FormattedMessage id={title.id} defaultMessage={title.text} />
          </h2>
          <p>
            <FormattedMessage id={msg.id} defaultMessage={msg.text} />
          </p>
        </div>
        <div className="form-body" style={formStyle}>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label className="form-check-label" htmlFor="old_password">
                <FormattedMessage id="auth.changePwd.oldPwd" defaultMessage="Actual password" />
              </label>
              <input
                type="password"
                name="old_password"
                id="old_password"
                className="form-control"
                placeholder={intl.formatMessage({ id: 'auth.changePwd.oldPwd.placeholder', defaultMessage: '123456' })}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                <FormattedMessage id="auth.changePwd.pwd" defaultMessage="New password" />
              </label>
              <input
                type="password"
                name="password"
                id="password"
                className="form-control"
                placeholder={intl.formatMessage({
                  id: 'auth.changePwd.pwd.placeholder',
                  defaultMessage: 'New password',
                })}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                <FormattedMessage id="auth.changePwd.pwdConfirm" defaultMessage="New password confirm" />
              </label>
              <input
                type="password"
                name="password_confirmation"
                id="password_confirmation"
                className="form-control"
                placeholder={intl.formatMessage({
                  id: 'auth.changePwd.pwd.placeholder',
                  defaultMessage: 'New password',
                })}
                onChange={this.handleChange}
              />
            </div>

            {error !== '' && <Alert type="danger" message={errorMessage} />}
            {success && (
              <Alert
                type="success"
                message={<FormattedMessage id="info-4001" defaultMessage="New Password has been save !" />}
              />
            )}

            <button type="submit" className="btn btn-primary btn-block" disabled={!!sending}>
              {sending && (
                <>
                  <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
                  <span className="sr-only">
                    {intl.formatMessage({ id: 'general.loading', defaultMessage: 'Loading...' })}
                  </span>
                  &nbsp;
                </>
              )}
              <FormattedMessage id="auth.changePwd.btnConfirm" defaultMessage="Update my password" />
            </button>
          </form>
        </div>
        <div className="form-message" style={messageStyle}>
          <img src="/images/envelope.svg" alt="Message sent envelope" />
        </div>
      </div>
    );
  }
}
FormChangePwd.contextType = ApiContext;
export default injectIntl(FormChangePwd);
