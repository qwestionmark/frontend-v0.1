import { FormattedMessage, useIntl } from 'react-intl';
import AsyncCreatableSelect from 'react-select/async-creatable';
import algoliasearch from 'algoliasearch/lite';
import TitleInfo from '~/components/Tools/TitleInfo';
// import "./FormSkillsComponent.scss";

const FormSkillsComponent = ({
  content = [],
  errorCodeMessage = '',
  id = 'skills',
  mandatory = false,
  type = 'default',
  onChange = (value, skills) => console.warn(`onChange doesn't exist to update ${value} of ${skills}`),
  placeholder = '',
  title = 'Title',
}) => {
  const intl = useIntl();

  const handleChange = (skillsList) => {
    // convert react-select array (skillsList) into an array with just the skills labels
    const skills = skillsList?.map(({ label }) => label);
    onChange(id, skills);
  };

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('Skill'); // Skill
  let algoliaSkills = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['skill_name'], // skill_name
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaSkills = content.hits;
        } else {
          algoliaSkills = [''];
        }
        algoliaSkills = algoliaSkills.map(({ skill_name }) => {
          return { value: skill_name, label: skill_name };
        }); // skill_name
        resolve(algoliaSkills);
      });
  };
  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const currentSkills = content.map((skill) => {
    if (skill.skill_name) {
      return { label: skill.skill_name, value: skill.skill_name };
    }
    return { label: skill, value: skill };
  });

  const customStyles = {
    loadingIndicator: (provided) => ({
      ...provided,
      display: 'none',
    }),
  };

  return (
    <div className="formSkills">
      <TitleInfo title={title} mandatory={mandatory} />

      <div className="content skill-container" id="skills">
        <label className="form-check-label" htmlFor={`show${id}`}>
          <FormattedMessage
            id={`help.skills.${type}`}
            defaultMessage="Type your skills in the box bellow. To validate a skill, click on it or press Enter."
          />
        </label>
        <AsyncCreatableSelect
          name={id}
          cacheOptions
          isMulti
          defaultValue={currentSkills && currentSkills}
          defaultOptions={false}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) =>
            `${intl.formatMessage({ id: 'general.add', defaultMessage: `Add` })} "${inputValue}"`
          }
          blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          noOptionsMessage={() => null}
          loadOptions={loadOptions}
          onChange={handleChange}
          placeholder={placeholder}
          // allowCreateWhileLoading={true}
          styles={customStyles}
        />
        {errorCodeMessage && (
          <div className="invalid-feedback">
            <FormattedMessage id={errorCodeMessage} defaultMessage="Value is not valid" />
          </div>
        )}
      </div>
    </div>
  );
};

export default FormSkillsComponent;
