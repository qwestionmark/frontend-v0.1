import { FC, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import BtnUploadFile from '~/components/Tools/BtnUploadFile';
import DocumentsList from '~/components/Tools/Documents/DocumentsList';
import Loading from '~/components/Tools/Loading';
import { ItemType } from '~/types';
import Box from '~/components/Box';
import { Document } from '~/types/common';
import { useApi } from '~/contexts/apiContext';

interface Props {
  itemId: number;
  itemType: ItemType;
  isAdmin: boolean;
  documents: Document[];
  showTitle?: boolean;
}

const DocumentsManager: FC<Props> = ({ itemId, itemType, documents, isAdmin, showTitle }) => {
  const [loading, setLoading] = useState(false);
  const [documentsList, setDocumentsList] = useState(documents);
  const api = useApi();

  const refresh = () => {
    setLoading(true);
    api
      .get(`/api/${itemType}/${itemId}`)
      .then((res) => {
        res.data && setDocumentsList(res.data.documents);
        setLoading(false);
      })
      .catch((err) => {
        console.error(`Couldn't GET ${itemType} with itemId=${itemId}`, err);
        setLoading(false);
      });
  };

  return (
    <>
      {documentsList.length !== 0 && showTitle && (
        <Box fontWeight="bold">
          <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
        </Box>
      )}
      <div className="documentsManager">
        {isAdmin && (
          <BtnUploadFile
            itemId={itemId}
            itemType={itemType}
            multiple
            refresh={refresh}
            text={<FormattedMessage id="info-1003" defaultMessage="Choose a file" />}
            type="documents"
            uploadNow
          />
        )}
        <Loading active={loading}>
          <DocumentsList
            documents={documentsList}
            itemId={itemId}
            isAdmin={isAdmin}
            itemType={itemType}
            refresh={refresh}
            cardType="cards"
          />
        </Loading>
      </div>
    </>
  );
};

export default DocumentsManager;
