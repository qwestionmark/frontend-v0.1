// eslint-disable-next-line no-unused-vars
import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import Button from '../primitives/Button';
import Box from '../Box';
import ReactGA from 'react-ga';
import CreatableSelect from 'react-select/creatable';

export default function InviteMember({ itemType = '', itemId }) {
  const [inviteSend, setInviteSend] = useState(false);
  const [emailsList, setEmailsList] = useState([]);
  const [error, setError] = useState(false);
  const api = useApi();
  const { formatMessage } = useIntl();
  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    content?.map(function (invitee_mail) {
      invitee_mail && tempEmailsList.push(invitee_mail.value);
    });
    setEmailsList(tempEmailsList);
    console.log(emailsList);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (
      itemType === 'projects' ||
      itemType === 'communities' ||
      itemType === 'challenges' ||
      itemType === 'programs' ||
      itemType === 'spaces'
    ) {
      if (itemId) {
        const params = { stranger_emails: emailsList };
        api
          .post(`/api/${itemType}/${itemId}/invite`, params)
          .then((res) => {
            const userId = res.config.headers.userId;
            // send event to google analytics
            ReactGA.event({ category: 'Invite', action: 'invite user', label: `[${userId},${itemId},${itemType}]` });
            setInviteSend(true);
            setTimeout(() => {
              resetState();
            }, 1500);
          })
          .catch(() => {
            setError(true);
            setTimeout(() => {
              setError(false);
            }, 8000);
          });
      } else {
        console.warn('itemId is missing');
      }
    } else {
      console.warn('itemType not compatible');
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Box pb={2} pt={2}>
        <CreatableSelect
          isClearable
          isMulti
          noOptionsMessage={() => null}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={formatMessage({
            id: 'member.invite.mail.placeholder.multiple',
            defaultMessage: 'Add one or multiple emails',
          })}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => inputValue}
          onChange={handleChangeEmail}
          // TODO check if email is valid before adding it to the list
        />
      </Box>
      <Button type="submit" disabled={emailsList.length === 0}>
        {inviteSend ? (
          <FormattedMessage id="member.invite.btnSendEnded" defaultMessage="Invitation sent" />
        ) : (
          <FormattedMessage id="member.invite.btnSend" defaultMessage="Send invitation" />
        )}
      </Button>
      {error && (
        <div className="alert alert-danger" role="alert">
          <FormattedMessage id="err-" defaultMessage="An error has occurred" />
        </div>
      )}
    </form>
  );
}
