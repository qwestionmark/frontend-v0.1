import { FC } from 'react';
import { useIntl } from 'react-intl';
import { typography, color } from 'styled-system';
import styled from '~/utils/styled';

const Text = styled.p`
  ${[typography, color]};
  font-size: 1.5rem;
  font-weight: bold;
  color: ${(p) => p.theme.colors.greys['500']};
`;
interface Props {
  type?: 'results' | 'group' | 'challenge' | 'project' | 'program' | 'need' | 'followings' | 'post' | 'board' | 'saved';
}
const NoResults: FC<Props> = ({ type = 'results' }) => {
  const intl = useIntl();
  return <Text>{intl.formatMessage({ id: `list.noResult.${type}`, defaultMessage: `No ${type}` })}</Text>;
};

export default NoResults;
