import { FormattedMessage, useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRouter } from 'next/router';
import { useModal } from '~/contexts/modalContext';
import { copyLink } from '~/utils/utils';
import Alert from '../Alert';
import ReactGA from 'react-ga';
import useUser from '~/hooks/useUser';
import { ItemType } from '~/types';
import { FC } from 'react';

interface Props {
  type: ItemType;
  specialObjId?: number;
}

const ShareBtns: FC<Props> = ({ type, specialObjId }) => {
  const router = useRouter();
  const { formatMessage } = useIntl();
  const { user } = useUser();
  const { showModal } = useModal();
  const typeIntl = { id: type, defaultMessage: type };

  const onShareButtonClick = (e) => {
    // set objectId depending on type
    const objectId =
      type === 'post' || type === 'need'
        ? specialObjId
        : type === 'challenge' || type === 'program' || type === 'space'
        ? router.query.short_title
        : router.query.id;
    // make objUrlPath the router asPath, except if we are sharing need or posts, in which case the url is the one from the single need or post page
    const objUrlPath = type === 'need' ? `/need/${objectId}` : type === 'post' ? `/post/${objectId}` : router.asPath;
    // check if browser/phone support the native share api (meaning we are in a mobile)
    if (navigator.share) {
      navigator
        .share({
          title: `${type}`,
          text: `Check out this ${type} on the JOGL platform: `,
          url: process.env.ADDRESS_FRONT + objUrlPath,
        })
        .catch(console.error);
    } else {
      // else open modal with defined sharing methods
      if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
        // if function is launched via keypress, execute only if it's the 'enter' key
        showModal({
          children: (
            <Modal
              type={type}
              objectId={objectId}
              objUrlPath={objUrlPath}
              userId={user?.id}
              formatMessage={formatMessage}
            />
          ),
          title: 'Share this {type}',
          titleId: 'general.shareBtns.modalTitle',
          values: { type: formatMessage(typeIntl) },
        });
      }
    }
    // send event to google analytics
    ReactGA.event({ category: 'Button', action: 'Share', label: `[${user?.id},${objectId},${type}]` });
  };

  return (
    <div className="shareBtns">
      <button
        className={
          type === 'post' || type === 'need'
            ? 'btn-postcard'
            : type === 'program' || type === 'challenge' || type === 'space'
            ? 'share-button-bg special'
            : 'share-button-bg'
        }
        type="button"
        title={`Share this ${type}`}
        onClick={onShareButtonClick}
        onKeyUp={onShareButtonClick}
        tabIndex={0}
      >
        {type === 'project' || type === 'community' ? (
          <FontAwesomeIcon icon="share-alt" />
        ) : (
          <FontAwesomeIcon icon="share" size="lg" />
        )}
        {type !== 'program' && type !== 'challenge' && type !== 'space' && (
          <span>{formatMessage({ id: 'general.shareBtns.btnTitle', defaultMessage: 'Share' })}</span>
        )}
      </button>
    </div>
  );
};

const Modal = ({ type, objectId, objUrlPath, userId, formatMessage }) => {
  const objUrl = process.env.ADDRESS_FRONT + encodeURIComponent(objUrlPath);
  const openWindow = (socialMedia) => {
    // send event to google analytics
    ReactGA.event({
      category: 'Button',
      action: `Desktop share via ${socialMedia}`,
      label: `[${userId},${objectId},${type}]`,
    });
    let shareUrl;
    if (socialMedia === 'facebook') shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${objUrl}`;
    if (socialMedia === 'twitter')
      shareUrl = `https://twitter.com/intent/tweet/?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&hashtags=JOGL&url=${objUrl}`;
    if (socialMedia === 'linkedin')
      shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${objUrl}&title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&amp;&source=${objUrl}`;
    if (socialMedia === 'reddit')
      shareUrl = `https://reddit.com/submit/?url=${objUrl}&resubmit=true&amp;title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;`;
    if (socialMedia === 'whatsapp')
      shareUrl = `https://api.whatsapp.com/send?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform: ${objUrl}&preview_url=true`;
    if (socialMedia === 'telegram')
      shareUrl = `https://telegram.me/share/url?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;url=${objUrl}`;
    window.open(shareUrl, 'share-dialog', 'width=626,height=436');
  };
  return (
    <div className="share-dialog is-open">
      <div className="targets">
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('facebook')}
          aria-label="Share on Facebook"
        >
          <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'facebook']} /> Facebook
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('twitter')}
          aria-label="Share on Twitter"
        >
          <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'twitter']} /> Twitter
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('linkedin')}
          aria-label="Share on LinkedIn"
        >
          <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'linkedin']} /> LinkedIn
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('reddit')}
          aria-label="Share on Reddit"
        >
          <div className="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'reddit-alien']} /> Reddit
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('whatsapp')}
          aria-label="Share on WhatsApp"
        >
          <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'whatsapp']} /> Whatsapp
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('telegram')}
          aria-label="Share on Telegram"
        >
          <div className="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'telegram-plane']} /> Telegram
          </div>
        </button>
        <button type="button" className="resp-sharing-button__link" rel="noopener" aria-label="Share by E-Mail">
          <a
            href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&body=${objUrl}`}
            target="_self"
          >
            <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
              <FontAwesomeIcon icon="envelope" /> Email
            </div>
          </a>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => copyLink(objectId, type, undefined)}
          rel="noopener"
          aria-label="Copy link"
        >
          <div className="resp-sharing-button resp-sharing-button--link resp-sharing-button--large">
            <FontAwesomeIcon icon="link" /> {formatMessage({ id: 'general.copyLink', defaultMessage: 'Copy link' })}
          </div>
        </button>
      </div>
      <Alert
        id="copyConfirmation"
        postId={objectId}
        type="success"
        message={<FormattedMessage id="general.copyLink.conf" defaultMessage="The link has been copied" />}
      />
    </div>
  );
};

export default ShareBtns;
