import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component, ReactNode } from 'react';
import { FormattedMessage, injectIntl, IntlShape } from 'react-intl';
import { ApiContext } from '~/contexts/apiContext';
import { ItemType } from '~/types';

interface Props {
  fileTypes: string[];
  itemId: number;
  itemType: ItemType;
  maxSizeFile: number;
  onIconUpload: (result: any) => void;
  setListFiles: (listFiles: any) => void;
  refresh: () => void;
  type: string;
  imageUrl: string;
  text: string | ReactNode;
  textUploading: string;
  uploadNow: boolean;
  intl: IntlShape;
}
interface State {
  uploading: boolean;
}
class BtnUploadIcon extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      fileTypes: [''],
      itemId: '',
      itemType: '',
      maxSizeFile: 31457280,
      onIconUpload: (result) => console.warn('Oops, the function onIconUpload is missing !', result),
      setListFiles: (listFiles) => console.warn('Oops, the function setListFiles is missing !', listFiles),
      type: '',
      imageUrl: '',
      text: <FormattedMessage id="info-1000" defaultMessage="Attach file" />,
      textUploading: <FormattedMessage id="info-1001" defaultMessage="Uploading..." />,
      uploadNow: false,
    };
  }

  handleChange(event) {
    event.preventDefault();
    if (event.target.files) {
      if (!this.validFilesSize(event.target.files)) {
        this.props.onIconUpload({
          error: (
            <>
              <FormattedMessage id="err-1001" defaultMessage="File's size too large" />
              &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}
              Mo)
            </>
          ),
          url: '',
        });
      } else {
        this.props.onIconUpload(event.target.files[0]);
        document.getElementById('btnUpload').value = '';
      }
    }
  }

  validFilesSize(files) {
    const { maxSizeFile } = this.props;
    let nbFilesValid = 0;
    if (files) {
      for (let index = 0; index < files.length; index++) {
        if (files[index].size <= maxSizeFile) {
          nbFilesValid++;
        }
      }
    }
    return nbFilesValid === files.length; // return true or false depending on condition
  }

  render() {
    const { text, textUploading, fileTypes } = this.props;
    const { uploading } = this.state;
    if (uploading) {
      // if "uploading" true, make button and input disabled (unclickable)
      // TODO: Don't use querySelector, maybe use ref or conditional rendering.
      document.querySelector('.upload-btn-wrapper .btn').disabled = true;
      document.querySelector('.upload-btn-wrapper input').disabled = true;
    }
    return (
      <>
        <div className="upload-btn-wrapper">
          <div className="btn">
            {uploading ? ( // render differently depending on uploading state
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
                {textUploading}
              </>
            ) : (
              <>
                <FontAwesomeIcon icon={['far', 'file']} />
                {text}
              </>
            )}
          </div>
          <input id="btnUpload" type="file" accept={fileTypes.join()} multiple={false} onChange={this.handleChange} />
        </div>
      </>
    );
  }
}
BtnUploadIcon.contextType = ApiContext;
export default injectIntl<undefined, Props>(BtnUploadIcon);
