import { Component } from 'react';
import $ from 'jquery';
import { linkifyQuill } from '~/utils/utils';
import 'react-quill/dist/quill.snow.css';
// import "./InfoHtmlComponent.scss";

export default class InfoHtmlComponent extends Component {
  static get defaultProps() {
    return {
      title: 'Title',
      content: '',
    };
  }

  componentDidMount() {
    $('.tab-pane#bout .infoHtml a[href]').attr('target', '_blank');
  }

  render() {
    let { content } = this.props;

    if (content) {
      content = linkifyQuill(content);
      return (
        <div className="infoHtml">
          <div className="content">
            <div dangerouslySetInnerHTML={{ __html: content }} className="rendered-quill-content ql-editor" />
          </div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
}
