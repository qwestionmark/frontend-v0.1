// TODO This component over-fetches way to much data just to get the clap count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import ReactGA from 'react-ga';
import useUser from '~/hooks/useUser';

interface Props {
  type?: 'text' | 'btn';
  source?: 'algolia' | 'api';
  clapState: boolean;
  clapCount: number;
  itemId: number;
  itemType: ItemType;
  refresh?: () => {};
}
const BtnClap = ({
  type = 'btn',
  source = 'api',
  clapState: propsClapState = false,
  clapCount: propsClapCount = 0,
  itemId = undefined,
  itemType = undefined,
  refresh = undefined,
}: Props) => {
  const [clapState, setClapState] = useState(propsClapState);
  const [clapCount, setClapCount] = useState(propsClapCount);
  const [isSending, setIsSending] = useState(false);
  const [action, setAction] = useState<'clap' | 'unclap'>(propsClapState ? 'unclap' : 'clap');
  const api = useApi();
  const { user } = useUser();
  useEffect(() => {
    // if data also comes from algolia (and not only api), get clap state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchClaps = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/clap`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET claps", err);
              }
            });
          if (res?.data?.has_clapped) {
            setClapState(res.data.has_clapped);
          }
        };
        fetchClaps();
      }
      setClapState(false);
    }
    return () => {
      // This will prevent to setClapState on unmounted BtnClap
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  useEffect(() => {
    setAction(clapState ? 'unclap' : 'clap');
  }, [clapState]);

  const changeStateClap = (e) => {
    // toggle display of comments unless we don't want the comments to show at all (in post card)
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setIsSending(true);
      // send event to google analytics
      if (user) ReactGA.event({ category: 'Button', action: action, label: `[${user.id},${itemId},${itemType}]` });
      const newClapCount = clapState ? clapCount - 1 : clapCount + 1;
      if (action === 'clap') {
        api
          .put(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(true);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      } else {
        api
          .delete(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(false);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      }
    }
  };
  if (type === 'text') {
    return (
      <div
        onClick={changeStateClap}
        onKeyUp={changeStateClap}
        className={clapState ? 'btn-postcard btn text-primary' : 'btn-postcard btn text-dark'}
        disabled={isSending}
        role="button"
        tabIndex={0}
      >
        {isSending ? (
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
        ) : (
          <FontAwesomeIcon icon="sign-language" size="lg" />
        )}
        &nbsp;
        <FormattedMessage id="post.clapAction" defaultMessage="Clap" />
      </div>
    );
  }
  return (
    <>
      <button
        onClick={changeStateClap}
        onKeyUp={changeStateClap}
        className={
          clapState ? 'btn-clap btn btn-sm btn-primary text-light' : 'btn-clap btn btn-sm btn-secondary text-dark'
        }
        type="button"
        id="btnClap"
        disabled={isSending}
        // data-tip={action}
        // data-for="clapBtn"
      >
        <FontAwesomeIcon icon="sign-language" size="lg" />
        <span>{clapCount}</span>
        {isSending && (
          <span className="spinInsideClap">
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          </span>
        )}
      </button>
      {/* <ReactTooltip id="clapBtn" delayHide={300} effect="solid" /> */}
    </>
  );
};

export default BtnClap;
