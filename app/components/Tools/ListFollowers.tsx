import React, { FC, useEffect, useState } from 'react';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import Grid from '../Grid';
import MemberCard from '../Members/MemberCard';
import UserCard from '../User/UserCard';

interface Props {
  itemId: number;
  itemType: ItemType;
}
const ListFollowers: FC<Props> = ({ itemId, itemType }) => {
  const [listFollowers, setListFollowers] = useState([]);
  const [loading, setLoading] = useState(true);
  const api = useApi();
  useEffect(() => {
    setLoading(true);
    api
      .get(`/api/${itemType}/${itemId}/followers`)
      .then((res) => {
        const listUser = res.data.followers;
        setLoading(false);
        setListFollowers(listUser);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [api, itemId, itemType]); // = componentDidMount

  return (
    <div className="listFollowers">
      <Loading active={loading}>
        <Grid gridGap={[2, 4]} gridCols={[1, undefined, undefined, 2]} display={['grid', 'inline-grid']}>
          {listFollowers?.map((follower, i) => (
            <UserCard
              key={i}
              id={follower.id}
              firstName={follower.first_name}
              lastName={follower.last_name}
              nickName={follower.nickname}
              shortBio={follower.short_bio}
              logoUrl={follower.logo_url}
              canContact={follower.can_contact}
              hasFollowed={follower.has_followed}
              projectsCount={follower.projects_count}
              mutualCount={follower.mutual_count}
            />
          ))}
        </Grid>
      </Loading>
    </div>
  );
};
export default ListFollowers;
