// TODO This component over-fetches way to much data just to get the save count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import React, { FC, useEffect, useState } from 'react';
import { useApi } from '~/contexts/apiContext';
import { DataSource, ItemType } from '~/types';
import styled from '~/utils/styled';
import ReactTooltip from 'react-tooltip';
import { useIntl } from 'react-intl';
import ReactGA from 'react-ga';
import useUser from '~/hooks/useUser';

const Bookmark = styled(FontAwesomeIcon)`
  cursor: pointer;
  opacity: 0.8;
`;
interface Props {
  source?: DataSource;
  saveState: boolean;
  itemId: number;
  itemType: ItemType;
  refresh?: () => void;
}
const BtnSave: FC<Props> = ({ source = 'api', saveState: propsSaveState = false, itemId, itemType, refresh }) => {
  const [saveState, setSaveState] = useState(propsSaveState);
  const [action, setAction] = useState<'save' | 'unsave'>(propsSaveState ? 'unsave' : 'save');
  const [icon, setIcon] = useState(['far', 'bookmark']);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { formatMessage } = useIntl();
  const { user } = useUser();
  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchSaves = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/save`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET saves", err);
              }
            });
          if (res?.data?.has_saved) {
            setSaveState(res.data.has_saved);
          }
        };
        fetchSaves();
      }
      setSaveState(false);
    }
    return () => {
      // This will prevent to setSaveState on unmounted BtnSave
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  useEffect(() => {
    setAction(saveState ? 'unsave' : 'save');
    setIcon(saveState ? ['fas', 'bookmark'] : ['far', 'bookmark']);
  }, [saveState]);

  const changeStateSave = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      if (action === 'save') {
        api
          .put(`/api/${itemType}/${itemId}/save`)
          .then(() => {
            // send event to google analytics
            ReactGA.event({ category: 'Button', action: action, label: `[${user.id},${itemId},${itemType}]` });
            setSaveState(true);
            setSending(false);
          })
          .catch(() => {
            setSending(false);
          });
      } else {
        api
          .delete(`/api/${itemType}/${itemId}/save`)
          .then(() => {
            setSaveState(false);
            setSending(false);
          })
          .catch(() => {});
      }
      if (refresh) {
        refresh();
      }
    }
  };
  return (
    <>
      {sending ? (
        <span>
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          &nbsp;
        </span>
      ) : (
        <>
          <Bookmark
            icon={icon}
            size="lg"
            onClick={changeStateSave}
            onKeyUp={changeStateSave}
            // show/hide tooltip on element focus/blur
            onFocus={(e) => ReactTooltip.show(e.target)}
            onBlur={(e) => ReactTooltip.hide(e.target)}
            onMouseEnter={() => {
              // change icon depending on follow state
              setIcon(saveState ? ['far', 'bookmark'] : ['fas', 'bookmark']);
            }}
            tabIndex={0}
            onMouseLeave={() => {
              // change icon depending on follow state
              setIcon(saveState ? ['fas', 'bookmark'] : ['far', 'bookmark']);
            }}
            data-tip={formatMessage({ id: `general.${action}`, defaultMessage: action })}
            data-for="save"
            aria-describedby={`save ${itemType}`}
          />
          <ReactTooltip id="save" delayHide={300} effect="solid" role="tooltip" />
        </>
      )}
    </>
  );
};

export default BtnSave;
