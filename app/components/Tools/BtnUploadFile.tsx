import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component, ReactNode } from 'react';
import { FormattedMessage, injectIntl, IntlShape } from 'react-intl';
import { ApiContext } from '~/contexts/apiContext';
import { ItemType } from '~/types';

interface Props {
  fileTypes: string[];
  itemId: number;
  itemType: ItemType;
  maxSizeFile: number;
  multiple: boolean;
  onChange: (result: any) => void;
  setListFiles: (listFiles: any) => void;
  refresh?: () => void;
  type: string;
  imageUrl: string;
  text: string | ReactNode;
  textUploading: string;
  uploadNow: boolean;
  intl: IntlShape;
}
interface State {
  uploading: boolean;
}
class BtnUploadFile extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      fileTypes: [''],
      itemId: '',
      itemType: '',
      maxSizeFile: 31457280,
      multiple: true,
      onChange: (result) => console.warn('Oops, the function onChange is missing !', result),
      setListFiles: (listFiles) => console.warn('Oops, the function setListFiles is missing !', listFiles),
      type: '',
      imageUrl: '',
      text: <FormattedMessage id="info-1000" defaultMessage="Attach file" />,
      textUploading: <FormattedMessage id="info-1001" defaultMessage="Uploading..." />,
      uploadNow: false,
    };
  }

  handleChange(event) {
    event.preventDefault();
    const { intl, itemType } = this.props;
    if (event.target.files) {
      if (!this.validFilesSize(event.target.files)) {
        this.props.onChange({
          error: (
            <>
              <FormattedMessage id="err-1001" defaultMessage="File's size too large" />
              &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}
              Mo)
            </>
          ),
          url: '',
        });
      } else if (!this.props.uploadNow) {
        this.props.setListFiles(event.target.files);
        if (itemType === 'needs')
          alert(
            intl.formatMessage({
              id: 'need.docs.uploaded.onCreate',
              defaultMessage: 'File(s) uploaded! Please save the need to see them',
            })
          );
        // TODO: Do not use getElementById, maybe use ref.
        document.getElementById('btnUpload').value = '';
      } else {
        this.uploadFiles(event.target.files);
      }
    }
  }

  uploadFiles(files) {
    const { intl, itemId, itemType, multiple, type } = this.props;
    if (!itemId || !itemType || !type || !files) {
      this.props.onChange({ error: <FormattedMessage id="err-" defaultMessage="An error has occured" />, url: '' });
    } else {
      this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button

      const bodyFormData = new FormData();
      if (multiple) {
        Array.from(files).forEach((file) => {
          bodyFormData.append(`${type}[]`, file);
        });
      } else {
        bodyFormData.append(type, files[0]);
      }

      const config = {
        headers: { 'Content-Type': 'multipart/form-data' },
      };
      const api = this.context;
      api
        .post(`/api/${itemType}/${itemId}/${type}`, bodyFormData, config)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ uploading: false }); // set to false to show user upload has been successful
            if (itemType === 'needs')
              alert(
                intl.formatMessage({
                  id: 'need.docs.uploaded.onUpdate',
                  defaultMessage: 'File(s) uploaded! Please update the need, and reload page to see changes',
                })
              );
            if (res.data.url) {
              this.props.onChange({ error: '', url: res.data.url }); // on success, show new uploading image in the front
            } else if (this.props.refresh !== undefined) {
              this.props.refresh();
            }
          } else {
            this.setState({ uploading: false }); // stop uploading if error
            this.props.onChange({
              error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
              url: '',
            });
          }
        })
        .catch((err) => {
          this.setState({ uploading: false }); // stop uploading if error
          console.error({ err });
          // this.props.onChange({ error: `${error.response.data.status} : ${error.response.data.error}`, url: "" });
        });
    }
  }

  removeImg() {
    const { itemId, itemType, type } = this.props;
    const randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // each static media has a code at the end of url, so making an array of those codes for images 1 to 12
    // make logo_url randomly equals to one of our default image (1 to 12)
    const imgUrl =
      type === 'avatar' // set imgUrl depending if it's user profile or a banner
        ? `/images/default/users/default-user-${randomNumber}.png` // if type is avatar, reset image to a random default user avatar
        : ''; // else make it empty so it shows object default image
    this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button
    const api = this.context;
    api.delete(`/api/${itemType}/${itemId}/${type}`).then((res) => {
      if (res) {
        this.setState({ uploading: false }); // set to false to show user upload has been successful
        this.props.onChange({ error: '', url: imgUrl }); // set new imgUrl
      }
    });
    // .catch(error=>{});
  }

  validFilesSize(files) {
    const { maxSizeFile } = this.props;
    let nbFilesValid = 0;
    if (files) {
      for (let index = 0; index < files.length; index++) {
        if (files[index].size <= maxSizeFile) {
          nbFilesValid++;
        }
      }
    }
    return nbFilesValid === files.length; // return true or false depending on condition
  }

  // validFilesType(files) {
  //   const fileTypes = this.props.fileTypes;
  //   var nbFilesValid = 0;
  //   if(files){
  //     for(var index = 0; index < files.length; index++){
  //       for(var i = 0; i < fileTypes.length; i++) {
  //         if(files[index].type === fileTypes[i]) {
  //           nbFilesValid++;
  //           break;
  //         }
  //       }
  //     }
  //   }
  //   return (nbFilesValid === files.length) ? true : false // return true or false depending on condition
  // }

  render() {
    const { multiple, text, textUploading, fileTypes } = this.props;
    const { uploading } = this.state;
    if (uploading) {
      // if "uploading" true, make button and input disabled (unclickable)
      // TODO: Don't use querySelector, maybe use ref or conditional rendering.
      document.querySelector('.upload-btn-wrapper .btn').disabled = true;
      document.querySelector('.upload-btn-wrapper input').disabled = true;
    }
    return (
      <>
        <div className="upload-btn-wrapper">
          <div className="btn">
            {uploading ? ( // render differently depending on uploading state
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
                {textUploading}
              </>
            ) : (
              <>
                <FontAwesomeIcon icon={['far', 'file']} />
                {text}
              </>
            )}
          </div>
          <input
            id={!uploading ? 'btnUpload' : ''}
            type="file"
            accept={fileTypes.join()}
            multiple={!!multiple}
            onChange={this.handleChange}
          />
        </div>
        {/* {(
          (itemType !== "users" && imageUrl !== "") // if it's not a user (so all other object) and has a stored image (not empty)
          || (itemType === "users" && !imageUrl.includes('/static/')) // OR if it's a user and he has a real stored image (not a default one)
        ) && // then show buttn to remove image
          <>
            <div onClick={() => this.removeImg()} id="btnRemove">{intl.formatMessage({ id: "general.removeImage.btn" })}</div>
          </>
        } */}
      </>
    );
  }
}
BtnUploadFile.contextType = ApiContext;
export default injectIntl<undefined, Props>(BtnUploadFile);
