import React, { FC } from 'react';
import { FormattedMessage } from 'react-intl';

interface Props {
  active?: boolean;
  height?: string;
}
const Loading: FC<Props> = ({ active = true, height = '5rem', children }) => {
  if (active) {
    return (
      <div style={{ height, display: 'flex' }}>
        <div className="d-flex justify-content-center" style={{ margin: 'auto' }}>
          <div className="spinner-border text-secondary text-center" role="status">
            <span className="sr-only">
              <FormattedMessage id="general.loading" defaultMessage="Loading..." />
            </span>
          </div>
        </div>
      </div>
    );
  }
  return <>{children}</>;
};
export default Loading;
