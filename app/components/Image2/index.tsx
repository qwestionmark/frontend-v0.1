import Image from 'next/image';

const Image2 = ({
  src,
  alt = undefined,
  unsized,
  className = undefined,
  quality = 75,
  loading = 'lazy',
  width,
  height,
}) => {
  var isSafari,
    isIE = false;
  if (typeof window !== 'undefined') {
    isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    isIE = /*@cc_on!@*/ false || !!document.documentMode;
  }
  if (isSafari || isIE) {
    return (
      <img src={src} alt={alt} width="100%" {...(className && { className: className })} {...(alt && { alt: alt })} />
    );
  }
  return (
    <Image
      src={src}
      unsized={unsized}
      quality={quality}
      loading={loading}
      {...(className && { className: className })} // add class in dom only if it's defined
      {...(alt && { alt: alt })} // add alt in dom only if it's defined
      {...(width && { width: width })} // add width only if it's defined
      {...(height && { height: height })} // add height only if it's defined
    />
  );
};

export default Image2;
