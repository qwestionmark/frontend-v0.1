import { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { withApi } from '~/contexts/apiContext';

class UserDownloadInfos extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    const { api, userId } = this.props;
    api
      .get(`/api/users/${userId}`)
      .then((res) => {
        const personalDatas = res.data;
        const data = `text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(personalDatas))}`;
        const a = document.querySelector('.dlHiddenLink');
        a.href = `data:${data}`;
        a.download = 'data.json';
        a.innerHTML = 'download JSON';
        a.click();
      })
      .catch((err) => {
        console.error(`Couldn't GET user id=${userId}`, err);
      });
  }

  render() {
    return (
      <div>
        <button type="button" className="btn btn-primary dlInfosBtn" onClick={this.handleClick}>
          <FormattedMessage id="settings.account.dlinfos.text" defaultMessage="Download my personal information" />
        </button>
        <a href="#" className="dlHiddenLink" />
      </div>
    );
  }
}
export default withApi(UserDownloadInfos);
