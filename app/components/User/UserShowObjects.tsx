/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Loading from '~/components/Tools/Loading';
import CommunityList from '~/components/Community/CommunityList';
import ProjectList from '~/components/Project/ProjectList';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import ChallengeCard from '../Challenge/ChallengeCard';
import ProgramCard from '../Program/ProgramCard';
import styled from '~/utils/styled';
import { layout } from 'styled-system';
import Box from '../Box';
import NoResults from '../Tools/NoResults';
import UserCard from './UserCard';

// import "Components/User/UserProfile.scss";
// import "Components/User/UserFollowings.scss";

const OverflowGradient = styled.div`
  ${layout};
  width: 3rem;
  height: 100%;
  position: absolute;
  right: 0;
  background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%);
`;

const Nav = styled.nav`
  margin-top: 0px;
  justify-content: flex-start;
  border-bottom: 1px solid #dee2e6;
  position: sticky;
  position: -webkit-sticky;
  top: -1rem;
  background: white;
  margin-bottom: 1rem;
  z-index: 1;
  .nav-item {
    color: #5c5d5d;
    border: none !important;
    border-bottom: 2px solid transparent !important;
    font-size: 18px;
    font-weight: 500;
    width: fit-content;
    margin-bottom: 0;
    &.active,
    &:hover {
      border-bottom-color: #2987cd !important;
    }
  }
`;

export default function UserShowObjects({ list, type = 'following' }) {
  const [listChallenges, setListChallenges] = useState([]);
  const [listCommunities, setListCommunities] = useState([]);
  const [listProjects, setListProjects] = useState([]);
  const [listUsers, setListUsers] = useState([]);
  const [listNeeds, setListNeeds] = useState([]);
  const [listPrograms, setListPrograms] = useState([]);
  const [loading, setLoading] = useState(true);
  const [noResult, setNoResult] = useState(false);
  const [selectedType, setSelectedType] = useState('users');

  useEffect(() => {
    setLoading(true);
    setNoResult(false);
    if (list) {
      setLoading(false);
      setListChallenges(list?.challenges);
      setListCommunities(list?.communities);
      setListProjects(list?.projects);
      setListUsers(list?.users);
      setListNeeds(list?.needs);
      setListPrograms(list?.programs);
      if (
        list.challenges?.length === 0 &&
        list.communities?.length === 0 &&
        list.projects?.length === 0 &&
        list.users?.length === 0 &&
        list.needs?.length === 0 &&
        list.programs?.length === 0
      ) {
        setNoResult(true);
      }
    }
  }, [list]);
  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    listUsers?.length > 0
      ? setSelectedType('users') // if user follows other users, add active class to this tab
      : listProjects?.length > 0
      ? setSelectedType('projects') // if user doesn't follow objects from previous tab, but from projects, add active class to this tab
      : listCommunities?.length > 0
      ? setSelectedType('communities') // same as up for communities
      : listChallenges?.length > 0
      ? setSelectedType('challenges') // same as up for challenges
      : listNeeds?.length > 0
      ? setSelectedType('needs') // same as up for needs
      : listPrograms?.length > 0 && setSelectedType('programs'); // else add active class to programs tab
  }, [listChallenges, listCommunities, listUsers, listProjects, listNeeds, listPrograms]);

  return (
    <Loading active={loading} height="300px">
      {!noResult && (
        <Box position="relative" minHeight="70px">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Nav
            className="nav modal-nav-tabs container-fluid"
            style={{ overflow: 'scroll', flexWrap: 'nowrap', paddingRight: '1rem' }}
          >
            {listUsers?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'users' && 'active'}`}
                href="#users"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.users" defaultMessage="Users" />
              </a>
            )}
            {listProjects?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'projects' && 'active'}`}
                href="#projects"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.projects" defaultMessage="Projects" />
              </a>
            )}
            {listNeeds?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'needs' && 'active'}`}
                href="#needs"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.needs" defaultMessage="Needs" />
              </a>
            )}
            {listChallenges?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'challenges' && 'active'}`}
                href="#challenges"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.challenges" defaultMessage="Challenges" />
              </a>
            )}
            {listPrograms?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'programs' && 'active'}`}
                href="#programs"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.programs" defaultMessage="Programs" />
              </a>
            )}
            {listCommunities?.length > 0 && (
              <a
                className={`nav-item nav-link ${selectedType === 'communities' && 'active'}`}
                href="#communities"
                data-toggle="tab"
              >
                <FormattedMessage id="user.profile.tab.following.communities" defaultMessage="Communities" />
              </a>
            )}
          </Nav>
        </Box>
      )}

      <div className="tabContainer">
        <div className="tab-content">
          {listProjects?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'projects' && 'active'}`} id="projects">
              <ProjectList
                listProjects={
                  // if we are in followings modal, don't show draft projects, else we are in the "my objects modal", so we can show draft projects
                  type === 'followings' ? listProjects?.filter(({ status }) => status !== 'draft') : listProjects
                }
              />
            </div>
          )}
          {listUsers?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'users' && 'active'}`} id="users">
              <Grid gridGap={[2, 4]} gridCols={[1, 2]} display={['grid', 'inline-grid']} py={[2, 4]}>
                {listUsers?.map((user, i) => (
                  <UserCard
                    key={i}
                    id={user.id}
                    firstName={user.first_name}
                    lastName={user.last_name}
                    nickName={user.nickname}
                    shortBio={user.short_bio}
                    logoUrl={user.logo_url}
                    canContact={user.can_contact}
                    hasFollowed={user.has_followed}
                    skills={user.skills}
                    projectsCount={user.stats.projects_count}
                    mutualCount={user.stats.mutual_count}
                  />
                ))}
              </Grid>
            </div>
          )}
          {listNeeds?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'needs' && 'active'}`} id="needs">
              <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
                {!listNeeds ? (
                  <Loading />
                ) : (
                  listNeeds.map((need, i) => (
                    <NeedCard
                      key={i}
                      title={need.title}
                      project={need.project}
                      skills={need.skills}
                      resources={need.ressources}
                      hasSaved={need.has_saved}
                      id={need.id}
                      postsCount={need.posts_count}
                      publishedDate={need.created_at}
                      membersCount={need.members_count}
                      dueDate={need.end_date}
                      status={need.status}
                    />
                  ))
                )}
              </Grid>
            </div>
          )}
          {listChallenges?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'challenges' && 'active'}`} id="challenges">
              <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
                {listChallenges.map((challenge, i) => (
                  <ChallengeCard
                    key={i}
                    id={challenge.id}
                    short_title={challenge.short_title}
                    title={challenge.title}
                    title_fr={challenge.title_fr}
                    short_description={challenge.short_description}
                    short_description_fr={challenge.short_description_fr}
                    membersCount={challenge.members_count}
                    needsCount={challenge.needs_count}
                    has_saved={challenge.has_saved}
                    clapsCount={challenge.claps_count}
                    status={challenge.status}
                    programId={challenge.program.id}
                    programShortTitle={challenge.program.short_title}
                    programTitle={challenge.program.title}
                    programTitleFr={challenge.program.title_fr}
                    projectsCount={challenge.projects_count}
                    banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  />
                ))}
              </Grid>
            </div>
          )}
          {listPrograms?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'programs' && 'active'}`} id="programs">
              <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
                {listPrograms.map((program, i) => (
                  <ProgramCard
                    key={i}
                    id={program.id}
                    short_title={program.short_title}
                    title={program.title}
                    title_fr={program.title_fr}
                    short_description={program.short_description}
                    short_description_fr={program.short_description_fr}
                    membersCount={program.members_count}
                    needsCount={program.needs_count}
                    has_saved={program.has_saved}
                    clapsCount={program.claps_count}
                    projectsCount={program.projects_count}
                    banner_url={program.banner_url || '/images/default/default-program.jpg'}
                  />
                ))}
              </Grid>
            </div>
          )}
          {listCommunities?.length > 0 && (
            <div className={`tab-pane ${selectedType === 'communities' && 'active'}`} id="communities">
              <div className="communityList">
                <CommunityList listCommunities={listCommunities} />
              </div>
            </div>
          )}
          {noResult && <NoResults type={type} />}
        </div>
      </div>
    </Loading>
  );
}
