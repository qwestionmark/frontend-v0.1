import styled from '~/utils/styled';
import Box from '../Box';
import { PaperPlane } from '@emotion-icons/boxicons-regular';
import tw from 'twin.macro';

export const AvatarContainer = styled(Box)<{ avatarSize?: string }>`
  width: ${(p) => p.avatarSize};
  height: ${(p) => p.avatarSize};
  @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
    width: 5rem;
    height: 5rem;
  }
  @media (max-width: 480px) {
    width: 4rem;
    height: 4rem;
  }
  img {
    width: ${(p) => p.avatarSize};
    height: ${(p) => p.avatarSize};
    object-fit: cover;
    border-radius: 50%;
    @media (max-width: ${(p) => p.theme.breakpoints.sm}) {
      width: 5rem;
      height: 5rem;
    }
    @media (max-width: 480px) {
      width: 4rem;
      height: 4rem;
    }
    &:hover {
      opacity: 0.9;
    }
  }
`;

export const ContactButton = styled(PaperPlane)`
  ${tw`rounded-full text-gray-700`}
  box-shadow: inset 0 0 0 1px grey;
  padding: 0.34rem;
  :hover {
    ${tw`bg-gray-100 cursor-pointer`}
    box-shadow: inset 0 0 0 2px grey;
  }
`;

export const ShortBio = styled(Box)`
  overflow: hidden;
  word-break: break-word;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

export const CardContainer = styled(Box)`
  @media (max-width: 480px) {
    flex-direction: column;
  }
  @media (min-width: 480px) and (max-width: 640px) {
    flex-direction: row;
  }
  @media (min-width: 900px) {
    flex-direction: row;
  }
`;
