// eslint-disable-next-line no-unused-vars
import { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import AsyncSelect from 'react-select/async';
import CreatableSelect from 'react-select/creatable';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from '~/utils/styled';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert';
import ReactGA from 'react-ga';

export default function MembersModal({ itemType = '', itemId }) {
  const [usersList, setUsersList] = useState([]);
  const [emailsList, setEmailsList] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const { closeModal } = useModal();
  const { formatMessage } = useIntl();

  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <UserLabel row>
      <img src={logo_url} />
      <div>{label}</div>
    </UserLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('User'); // User
  let algoliaMembers = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'nickname', 'first_name', 'last_name', 'logo_url_sm'],
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaMembers = content.hits;
        } else {
          algoliaMembers = [''];
        }
        algoliaMembers = algoliaMembers.map((user) => {
          return { value: user.id, label: `${user.first_name} ${user.last_name}`, logo_url: user.logo_url_sm };
        });
        resolve(algoliaMembers);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    content?.map(function (invitee_mail) {
      invitee_mail && tempEmailsList.push(invitee_mail.value);
    });
    setUsersList([]);
    setEmailsList(tempEmailsList);
  };

  const handleChangeUser = (content) => {
    const tempUsersList = [];
    content?.map(function (email) {
      email && tempUsersList.push(email.value);
    });
    setEmailsList([]);
    setUsersList(tempUsersList);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      itemType === 'projects' ||
      itemType === 'communities' ||
      itemType === 'challenges' ||
      itemType === 'programs' ||
      itemType === 'spaces'
    ) {
      if (itemId) {
        // set params depending if we are inviting JOGL members or non-JOGL people
        const params = emailsList.length !== 0 ? { stranger_emails: emailsList } : { user_ids: usersList };
        setSending(true);
        api
          .post(`/api/${itemType}/${itemId}/invite`, params)
          .then((res) => {
            setInviteSend(true);
            setSending(false);
            const userId = res.config.headers.userId;
            ReactGA.event({ category: 'Invite', action: 'invite user', label: `[${userId},${itemId},${itemType}]` });
            setTimeout(() => {
              closeModal(); // close modal after 4.5sec
              resetState();
            }, 4500);
          })
          .catch(() => {
            setError(true);
            setSending(false);
            setTimeout(() => {
              setError(false);
            }, 8000);
          });
      } else {
        console.warn('itemId is missing');
      }
    } else {
      console.warn('itemType not compatible');
    }
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };

  // show different confirmation message depending on number of users added, and if it's via email or jogl user
  const confMsg =
    usersList.length === 1 // if 1 user has been invited
      ? formatMessage({
          id: 'member.invite.added',
          defaultMessage: 'Member was added! Please refresh the page to see the changes',
        })
      : usersList.length > 1 // if more than 1 user have been invited
      ? formatMessage({
          id: 'member.invite.added.plural',
          defaultMessage: 'Members were added! Please refresh the page to see the changes',
        }) // if we invited people via their email
      : formatMessage({
          id: 'member.invite.btnSendEnded',
          defaultMessage: 'Invitation sent',
        });

  return (
    <form>
      <div className="form-group">
        <label htmlFor="joglUser">
          <FormattedMessage id="member.invite.user.label" defaultMessage="JOGL user" />
        </label>
        <div className="input-group">
          <AsyncSelect
            isMulti
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={formatMessage({
              id: 'member.invite.user.placeholder',
              defaultMessage: 'Select user(s) to add',
            })}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: null, IndicatorSeparator: null }}
            isClearable
          />
        </div>
      </div>
      <Box className="text-center or" p={2}>
        <FormattedMessage id="member.invite.or" defaultMessage="Or" />
      </Box>
      <div className="form-group">
        <label htmlFor="byEmail">
          <FormattedMessage id="member.invite.mail.label" defaultMessage="Invite people outside JOGL" />
        </label>
        <CreatableSelect
          isClearable
          isMulti
          noOptionsMessage={() => null}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={formatMessage({
            id: 'member.invite.mail.placeholder.multiple',
            defaultMessage: 'Add one or multiple emails',
          })}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => inputValue}
          onChange={handleChangeEmail}
          className="react-select-emails"
          // TODO check if email is valid before adding it to the list
        />
      </div>
      <div className="text-center btnZone">
        {!inviteSend ? (
          <Button
            type="button"
            onClick={handleSubmit}
            disabled={
              (emailsList.length === 0 && usersList.length === 0) ||
              (emailsList.length !== 0 && usersList.length !== 0) ||
              sending
            }
          >
            {sending && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id="member.invite.btnSend" defaultMessage="Invite" />
          </Button>
        ) : (
          <Alert type="success" message={confMsg} />
        )}
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          <FormattedMessage id="err-" defaultMessage="An error has occurred" />
        </div>
      )}
    </form>
  );
}

const UserLabel = styled(Box)`
  align-items: center;
  div {
    font-size: 15px;
    color: color('gray-dark');
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;
