import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import React, { FC, Fragment, useMemo } from 'react';
import { FormattedDate, FormattedMessage, useIntl } from 'react-intl';
import H1 from '~/components/primitives/H1';
import BtnClap from '~/components/Tools/BtnClap';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import { useModal } from '~/contexts/modalContext';
import useMembers from '~/hooks/useMembers';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import { renderOwnerNames } from '~/utils/utils';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ListFollowers from '../Tools/ListFollowers';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import QuickSearchBar from '../Tools/QuickSearchBar';
import ReactGA from 'react-ga';
import { useTheme } from '@emotion/react';
import Chips from '../Chip/Chips';
// import Image from 'next/image';
import Image2 from '../Image2';

interface Props {
  project: Project;
}
const ProjectHeader: FC<Props> = ({ project }) => {
  const { members, membersError } = useMembers('projects', project.id, undefined);
  const { showModal, setIsOpen } = useModal();
  const { formatMessage, locale } = useIntl();
  const theme = useTheme();
  const { userData } = useUserData();

  const participatingToChallenges = (allChallenges) => {
    // filter challenges to display only the ones that you have been accepted.
    const challenges = allChallenges.filter((challenge) => challenge.project_status === 'accepted');
    return (
      <p className="card-by">
        <FormattedMessage
          id="project.info.participatingToChallenges"
          defaultMessage="Participating to challenge(s): "
        />
        {challenges.map((challenge, index) => {
          // filter to get only owners, and then map
          const rowLen = challenges.length;
          return (
            <Fragment key={index}>
              <Link href={`/challenge/${challenge.short_title}`}>
                <a>{locale === 'fr' && challenge.title_fr ? challenge.title_fr : challenge.title}</a>
              </Link>
              {/* add comma, except for last item */}
              {rowLen !== index + 1 && <span>, </span>}
            </Fragment>
          );
        })}
      </p>
    );
  };
  const showMembersModal = (e) => {
    if (members && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      // capture the opening of the modal as a special modal page view to google analytics
      ReactGA.modalview(`/viewMembers/project/${project.id}`);
      showModal({
        children: (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid gridGap={[2, 4]} gridCols={[1, 2]} display={['grid', 'inline-grid']} py={[0, 2, 4]}>
              {members.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  logoUrl={member.logo_url}
                  skills={member.skills}
                  canContact={member.can_contact}
                  hasFollowed={member.has_followed}
                  projectsCount={member.stats.projects_count}
                  mutualCount={member.stats.mutual_count}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
          </>
        ),
        title: 'Members',
        titleId: 'entity.tab.members',
        maxWidth: '60rem',
      });
    }
  };
  const showFollowersModal = (e) => {
    follower_count && // open modal only if project has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="projects" />,
        title: 'Followers',
        titleId: 'entity.tab.followers',
        maxWidth: '60rem',
      });
  };
  let { members_count, follower_count, banner_url } = project;
  const {
    has_followed,
    id,
    is_member,
    is_owner,
    short_description,
    short_title,
    title,
    interests,
    skills,
    has_clapped,
    has_saved,
    claps_count,
    users_sm,
    challenges,
    creator,
    created_at,
    updated_at,
    is_pending,
    is_private,
  } = project;
  if (follower_count === undefined) follower_count = 0;
  if (members_count === undefined) members_count = 0;
  if (!banner_url) {
    banner_url = '/images/default/default-project.jpg';
  }
  const activeChallenges = useMemo(() => {
    if (challenges) {
      return challenges.filter((challenge) => challenge.project_status !== 'pending');
    }
    return undefined;
  }, [challenges]);
  return (
    <div className="row projectHeader">
      <Box alignItems="center" width={'full'} mb={5} px={4}>
        <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
          <H1 fontSize={['2.55rem', undefined, '3rem']}>{title}</H1>
          {userData && (
            <Box row>
              <Box pl={4} pr={2}>
                <BtnSave itemType="projects" itemId={id} saveState={has_saved} />
              </Box>
              <BtnFollow followState={has_followed} itemType="projects" itemId={id} />
            </Box>
          )}
        </Box>
        {/* edit button */}
        {project.is_admin && (
          <Box mt={5}>
            <Link href={`/project/${project.id}/edit`}>
              <a style={{ display: 'flex', justifyContent: 'center' }}>
                <FontAwesomeIcon icon="edit" />
                <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
              </a>
            </Link>
          </Box>
        )}
      </Box>
      <div className="col-lg-7 col-md-12 projectHeader--banner">
        <Image2 src={banner_url} alt={title} unsized quality={100} />
      </div>
      <div className="col-lg-5 col-md-12 projectHeader--info">
        <p className="infos">#{short_title}</p>
        <p className="info">{short_description}</p>
        {created_at && (
          <Box color={theme.colors.secondary}>
            <FormattedMessage id="general.created_at" defaultMessage="Created on: " />
            <FormattedDate value={created_at} year="numeric" month="long" day="2-digit" />
          </Box>
        )}
        {/* show updated date only if it exists and if it's not the same day as created date (OR if only updated_at exists and not created_at) */}
        {((updated_at && created_at && created_at.substr(0, 10) !== updated_at.substr(0, 10)) ||
          (updated_at && !created_at)) && (
          <Box color={theme.colors.secondary}>
            <FormattedMessage id="general.updated_at" defaultMessage="Last update: " />
            <FormattedDate value={updated_at} year="numeric" month="long" day="2-digit" />
          </Box>
        )}
        <Box pt={4} /> {/* empty div with paddingTop */}
        {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator)}
        {activeChallenges.length !== 0 && participatingToChallenges(activeChallenges)}
        <InfoInterestsComponent
          place="entity_header"
          content={interests}
          displayCount="4"
          title={formatMessage({ id: 'general.sdgs', defaultMessage: "SDG's" })}
        />
        <Box mb={3}>
          <Box color={theme.colors.secondary}>
            {formatMessage({ id: 'user.profile.skills', defaultMessage: 'Skills' })}
          </Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/projects/?refinementList[skills][0]=${skill}`,
            }))}
            overflowText="seeMore"
            // color={theme.colors.primary}
            color="#F2F4F8"
            showCount={6}
          />
        </Box>
        <div className="projectStats">
          <span className="text" onClick={showFollowersModal} onKeyUp={showFollowersModal} tabIndex={0}>
            <strong>{follower_count}</strong>&nbsp;{textWithPlural('follower', follower_count)}
          </span>
          <span className="text" onClick={showMembersModal} onKeyUp={showMembersModal} tabIndex={0}>
            <strong>{members_count || 0}</strong>&nbsp;{textWithPlural('member', members_count)}
          </span>
        </div>
        <Box row spaceX={4} py={4} alignItems="center">
          {!is_owner && (
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              isPrivate={is_private}
              itemType="projects"
              itemId={id}
              textJoin={<FormattedMessage id="project.info.btnJoin" defaultMessage="Join project" />}
              textUnjoin={<FormattedMessage id="project.info.btnUnjoin" defaultMessage="Leave project" />}
            />
          )}
          <BtnClap itemType="projects" itemId={id} clapState={has_clapped} clapCount={claps_count} />
        </Box>
        <ShareBtns type="project" />
      </div>
    </div>
  );
};
export default ProjectHeader;
