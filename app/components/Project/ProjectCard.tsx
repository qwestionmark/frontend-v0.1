import Link from 'next/link';
import React, { FC } from 'react';
import { WidthProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import Chips from '../Chip/Chips';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  clapsCount: number;
  postsCount?: number;
  members_count: number;
  needs_count: number;
  has_saved: boolean;
  banner_url: string;
  width?: WidthProps['width'];
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
}
const ProjectCard: FC<Props> = ({
  id,
  title,
  shortTitle = undefined,
  short_description,
  clapsCount,
  postsCount,
  members_count,
  needs_count,
  has_saved,
  banner_url = '/images/default/default-project.jpg',
  width,
  cardFormat,
  source,
  skills,
}) => {
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  const projUrl = `/project/${id}/${shortTitle}`;
  return (
    <Card imgUrl={banner_url} isImgSmall={cardFormat === 'compact'} href={projUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={projUrl} passHref>
          <Title pr={2}>
            <H2 fontSize={TitleFontSize}>{title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="projects" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <CardDesc flex="1">{short_description}</CardDesc>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            href: `/search/projects/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={`/project/${id}/${shortTitle}`}
          // color={theme.colors.primary}
          color="#F2F4F8"
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
          <CardData value={members_count} title={textWithPlural('member', members_count)} />
          <CardData value={needs_count} title={textWithPlural('need', needs_count)} />
          {postsCount > 0 && <CardData value={postsCount} title={textWithPlural('post', postsCount)} />}
          <CardData value={clapsCount} title={textWithPlural('clap', clapsCount)} />
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);
const CardDesc = styled(Box)`
  overflow: hidden;
  word-break: break-word;
  display: -webkit-box;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
`;

export default ProjectCard;
