import React, { FC } from 'react';
import { useIntl } from 'react-intl';
import Box from '../Box';
import H2 from '~/components/primitives/H2';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import P from '../primitives/P';

interface Props {
  description: string;
  shortDescription: string;
  status: string;
}

const SpaceAbout: FC<Props> = ({ description, shortDescription, status }) => {
  const { formatMessage } = useIntl();

  return (
    <>
      {/* space short intro */}
      <H2>{formatMessage({ id: 'space.home.programActive', defaultMessage: '*Short intro' })}</H2>
      <P fontSize="17.5px">{shortDescription}</P>
      {/* space full description */}
      <H2 pt={5}>{formatMessage({ id: 'general.tab.about', defaultMessage: 'About' })}</H2>
      <InfoHtmlComponent title="" content={description} />
      {/* display space dates info, and status */}
      <Box pt={2} row>
        <strong>{formatMessage({ id: 'attach.status', defaultMessage: 'Status: ' })}</strong>&nbsp;
        {formatMessage({ id: `entity.info.status.${status}`, defaultMessage: status })}
      </Box>
    </>
  );
};
export default SpaceAbout;
