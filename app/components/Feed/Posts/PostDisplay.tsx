// ! This component is too complicated, there should a fixed number of post types
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
// import Image from 'next/image';
import React, { FC, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { LayoutProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import CommentDisplay from '~/components/Feed/Comments/CommentDisplay';
import PostUpdate from '~/components/Feed/Posts/PostUpdate';
import Grid from '~/components/Grid';
import UserCard from '~/components/User/UserCard';
import Alert from '~/components/Tools/Alert';
import BtnClap from '~/components/Tools/BtnClap';
import DocumentsList from '~/components/Tools/Documents/DocumentsList';
import Loading from '~/components/Tools/Loading';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import { Post, User } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import { useTheme } from '~/utils/theme';
import Translate from '~/utils/Translate';
import { displayObjectDate, linkify, reportContent, copyLink } from '~/utils/utils';
import CommentCreate from '../Comments/CommentCreate';
import PostDelete from './PostDelete';
import Image2 from '~/components/Image2';
// import Button from '~/components/primitives/Button';
// import A from '~/components/primitives/A';

interface Props {
  post: Post;
  user: User;
  feedId?: number | string;
  refresh?: () => void;
  isSingle?: boolean;
  isAdmin?: boolean;
  cardNoComments?: boolean;
  width?: LayoutProps['width'];
  isHomeFeed?: boolean;
}
const PostDisplay: FC<Props> = ({
  post: postProp,
  user,
  feedId,
  refresh,
  isAdmin,
  cardNoComments = false,
  width,
  isSingle,
}) => {
  const modal = useModal();
  const [showComments, setShowComments] = useState(!cardNoComments);
  // TODO: rename this state to a more intelligible one.
  const [isEditing, setIsEditing] = useState(false);
  const [viewMore, setViewMore] = useState(false);
  const [loading, setLoading] = useState(true);
  const [postDeleted, setPostDeleted] = useState(false);
  const api = useApi();
  const theme = useTheme();
  const { formatMessage } = useIntl();
  const { data: post, revalidate: revalidatePost } = useGet<Post>(`/api/posts/${postProp.id}`, {
    initialData: postProp,
  });
  const changeDisplayComments = () => {
    // toggle display of comments unless we don't want the comments to show at all (in post card)
    if (!cardNoComments) {
      setShowComments((prevState) => !prevState);
    } else {
      // else, open single post page in a new page
      const win = window.open(`/post/${post.id}`, '_blank');
      win.focus();
    }
  };

  const isEdit = () => {
    setIsEditing((prevState) => !prevState);
  };

  const showClapModal = () => {
    api.get(`/api/posts/${post.id}/clappers`).then((res) => {
      modal.showModal({
        children: <ClappersModal clappedUsers={res.data.clappers} />,
        title: 'Clappers',
        titleId: 'post.clappers',
        maxWidth: '30rem',
      });
    });
  };

  const callViewMore = () => {
    if (!cardNoComments) {
      setViewMore((prevState) => !prevState);
    } else {
      // else, open single post page in a new page
      var win = window.open(`/post/${post.id}`, '_blank');
      win.focus();
    }
  };

  useEffect(() => {
    setLoading(false);
  }, []);

  const onPostDelete = () => {
    setPostDeleted(true);
    // refresh();
  };
  if (loading) {
    return <Loading height="18rem" active={loading} />;
  }
  if (postDeleted) {
    return (
      <Box
        p={4}
        border="1px solid black"
        borderColor={theme.colors.successes[200]}
        color={theme.colors.successes[200]}
        backgroundColor={theme.colors.successes[900]}
        borderRadius={2}
      >
        <Translate id="feed.object.delete.conf" defaultMessage="The post has been deleted" />
      </Box>
    );
  }
  // const user = post !== undefined || !user ? userProp : user;
  if (post !== undefined) {
    const postId = post.id;
    const creatorId = post.creator.id;
    const objImg = post.creator.logo_url ? post.creator.logo_url : '/images/default/default-user.png';
    const postFromName =
      post.from.object_type === 'need' // if post is a need
        ? formatMessage({ id: 'post.need', defaultMessage: '[Need]: ' }) + post.from.object_name
        : post.from.object_name;

    const commentsCount = post.comments.length;
    const userImgStyle = { backgroundImage: `url(${objImg})` };

    const maxChar = 280; // maximum character in a post to be displayed by default;
    const isLongText = !viewMore && post.content.length > maxChar;
    const postImages = post.documents.filter(
      (document) => document.content_type === 'image/jpeg' || document.content_type === 'image/png'
    );
    const contentWithLinks = linkify(post.content);
    return (
      // @TODO best would be to apply props in Card only if isSingle is true
      // <Card maxWidth="700px" width={width} margin="auto">
      <Card width={width}>
        <div className={`post post-${postId} ${cardNoComments && 'postCard'}`}>
          <div className="topContent">
            <div className="topBar">
              <div className="left">
                <div className="userImgContainer">
                  <Link href={`/user/${creatorId}`}>
                    <a>
                      <div className="userImg" style={userImgStyle} />
                    </a>
                  </Link>
                </div>
                <div className="topInfo">
                  <Link href={`/user/${creatorId}`}>
                    <a>{`${post.creator.first_name} ${post.creator.last_name}`}</a>
                  </Link>

                  <div className="date">{displayObjectDate(post.created_at)}</div>
                  <div className="from">
                    <FormattedMessage id="post.from" defaultMessage="From: " />
                    <Link href={`/${post.from.object_type}/${post.from.object_id}`}>
                      <a>{postFromName}</a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="post-manage right d-flex flex-row">
                <div className="btn-group dropright">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle"
                    data-display="static"
                    data-flip="false"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    •••
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    {user && user.id === creatorId && !isEditing && (
                      <Box row as="button" onClick={() => setIsEditing((prevState) => !prevState)}>
                        <FontAwesomeIcon icon="edit" className="postUpdate" />
                        <FormattedMessage id="feed.object.update" defaultMessage="Update" />
                      </Box>
                    )}
                    {
                      user &&
                        user.id === creatorId && ( // if it's user's post
                          <PostDelete postId={postId} type="post" origin="self" refresh={onPostDelete} />
                        ) // set origin to self (it's your post)
                    }
                    {
                      user &&
                        user.id !== creatorId &&
                        isAdmin && ( // if user is admin and it's NOT his post
                          <PostDelete
                            postId={postId}
                            type="post"
                            origin="other"
                            feedId={feedId}
                            refresh={onPostDelete}
                          />
                        ) // set origin to other (not your post)
                    }
                    {user && user.id !== creatorId && (
                      <Box row as="button" onClick={() => reportContent('post', postId, undefined, api)}>
                        {' '}
                        {/* on click, launch report function (hide if user is creator */}
                        <FontAwesomeIcon icon="flag" className="postFlag" />
                        <FormattedMessage id="feed.object.report" defaultMessage="Report" />
                      </Box>
                    )}
                    <Box
                      row
                      as="button"
                      textAlign="left"
                      lineHeight="25px"
                      onClick={() => copyLink(postId, 'post', undefined)}
                    >
                      {/* on click, launch copyLink function */}
                      <FontAwesomeIcon icon="link" />
                      <FormattedMessage id="feed.object.copyLink" defaultMessage="Copy post Link" />
                    </Box>
                  </div>
                </div>
              </div>
            </div>
            {!isEditing ? ( // if post is not being edited, show post content
              <div className={`postTextContainer ${isLongText && 'hideText'}`}>
                {' '}
                {/* add hideText class to hide text if it's too long */}
                <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
                {isLongText && ( // show "view more" link if text is too long
                  <button type="button" className="viewMore" onClick={callViewMore}>
                    ...
                    <FormattedMessage id="general.showmore" defaultMessage="Show More" />
                  </button>
                )}
              </div>
            ) : (
              // else show post edition component
              <PostUpdate
                postId={postId}
                content={post.content}
                closeOrCancelEdit={isEdit}
                refresh={revalidatePost}
                userImg={user.logo_url_sm}
                userId={user.id}
              />
            )}
            {postImages.length > 0 /* If post has image, display it */ && (
              <Image2 src={postImages[0].url} alt="preview" unsized className="postImage" />
            )}

            <DocumentsList
              documents={post.documents}
              postId={post.id}
              cardType="feed"
              isEditing={isEditing}
              refresh={revalidatePost}
            />
          </div>
          <div className="actionBar">
            {!(commentsCount === 0 && post.claps_count === 0) && ( // show post stats unless post has 0 clap and comment
              <div className="postStats">
                {/* on clap icon click, show modal */}
                {post.claps_count > 0 ? (
                  <Box as="button" onClick={showClapModal}>
                    {
                      `${post.claps_count} ${textWithPlural(
                        'clap',
                        post.claps_count
                      )}` /* show claps count only if there are */
                    }
                  </Box>
                ) : (
                  <span /> // display empty span if not post
                )}
                {commentsCount > 0 ? (
                  <Box as="button" onClick={changeDisplayComments}>
                    {
                      commentsCount > 0 &&
                        `${commentsCount} ${textWithPlural(
                          'comment',
                          commentsCount
                        )}` /* show comments count only if there are */
                    }
                  </Box>
                ) : (
                  <span /> // display empty span if not comment
                )}
              </div>
            )}
            <Box row marginTop="10px" paddingTop="8px" borderTop="1px solid #d3d3d3">
              <BtnClap
                itemType="posts"
                itemId={post.id}
                type="text"
                clapState={post.has_clapped}
                refresh={revalidatePost}
              />
              <button className="btn-postcard btn" onClick={changeDisplayComments} type="button">
                <FontAwesomeIcon icon="comments" size="lg" />
                <FormattedMessage id="post.commentAction" defaultMessage="Comment" />
              </button>
              <ShareBtns type="post" specialObjId={post.id} />
            </Box>
            {showComments && (
              <CommentDisplay
                comments={post.comments}
                postId={postId}
                refresh={revalidatePost}
                user={user}
                isAdmin={isAdmin}
                isSingle={isSingle}
              />
            )}
            {!cardNoComments && <CommentCreate postId={postId} refresh={revalidatePost} user={user} />}
            {/* {cardNoComments && (
              <Button>
                <A href={`/post/${post.id}`}>
                  View comments
                </A>
              </Button>
            )} */}
          </div>
          {/* "Report" and "CopyLink" confirmation alerts (hidden by default) */}
          <Alert
            id="copyConfirmation"
            postId={postId}
            type="success"
            message={<FormattedMessage id="feed.object.copyLink.conf" defaultMessage="The post link has been copied" />}
          />
          <Alert
            id="reportPostConfirmation"
            postId={postId}
            type="success"
            message={<FormattedMessage id="feed.object.reportPost.conf" defaultMessage="The post has been reported" />}
          />
        </div>
      </Card>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
const ClappersModal = ({ clappedUsers }) => (
  <>
    {/* Show modal content only if there are clappers */}
    {clappedUsers.length !== 0 ? (
      <Grid gridGap={[2, 4]} gridCols={1} display={['grid', 'inline-grid']}>
        {clappedUsers?.map((user, i) => (
          <UserCard
            key={i}
            id={user.id}
            firstName={user.first_name}
            lastName={user.last_name}
            nickName={user.nickname}
            shortBio={user.short_bio}
            logoUrl={user.logo_url}
            canContact={user.can_contact}
            // projectsCount={user.projects_count}
            hasFollowed={user.has_followed}
          />
        ))}
      </Grid>
    ) : (
      <Loading />
    )}
  </>
);

export default PostDisplay;
