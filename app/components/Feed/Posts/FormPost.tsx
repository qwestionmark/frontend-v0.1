import { Component } from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import BtnUploadFile from '~/components/Tools/BtnUploadFile';
import PostInputMentions from '~/components/Tools/Forms/PostInputMentions';
import Button from '~/components/primitives/Button';
import Box from '~/components/Box';
import ReactTooltip from 'react-tooltip';
import 'twin.macro';

interface Props {
  handleChange: (content: any) => any;
  handleSubmit: () => void;
  handleChangeDoc: (document: any) => void;
  handleCancel: () => void;
}
interface State {}
class FormPost extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  static get defaultProps() {
    return {
      content: '',
      action: 'create',
      uploading: false,
      handleChange: () => console.warn('Missing function'),
      handleSubmit: () => console.warn('Missing function'),
      handleChangeDoc: () => console.warn('Missing function'),
      handleCancel: () => console.warn('Missing function'),
      documents: [],
    };
  }

  handleChange(content) {
    this.props.handleChange(content);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit();
  }

  handleChangeDoc(documents) {
    this.props.handleChangeDoc(documents);
  }

  handleCancel() {
    this.props.cancelEdit();
  }

  attachDocuments(documents) {
    const arrayDocuments = this.props.documents;
    for (let i = 0; i < documents.length; i++) {
      arrayDocuments.push(documents[i]);
    }
    this.props.handleChangeDoc(arrayDocuments);
  }

  deleteDocument(document) {
    const { documents } = this.props;
    documents.forEach((documentToInspect, index) => {
      if (documentToInspect === document) {
        documents.splice(index, 1);
      }
    });
    this.props.handleChangeDoc(documents);
  }

  renderPreviewDocuments(documents) {
    if (documents.length !== 0) {
      return (
        <div className="preview">
          <ul className="listDocuments">
            {documents.map((document, index) => (
              <li key={index}>
                {document.name}
                <button
                  type="button"
                  className="close"
                  aria-label="Close"
                  data-tip="Delete"
                  data-for="documentFeedEdit_delete"
                  onClick={() => this.deleteDocument(document)}
                  // show/hide tooltip on element focus/blur
                  onFocus={(e) => ReactTooltip.show(e.target)}
                  onBlur={(e) => ReactTooltip.hide(e.target)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <ReactTooltip id="documentFeedEdit_delete" effect="solid" />
              </li>
            ))}
          </ul>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  render() {
    const { action, content, documents, uploading, userImg, intl } = this.props;
    const submitBtnText = action === 'create' ? 'Publish' : 'Update';
    const postTypeClass = action === 'create' ? 'postCreate' : 'postUpdate';
    const userLogo = userImg || '/images/default/default-user.png';
    return (
      <div className={postTypeClass}>
        <form onSubmit={this.handleSubmit}>
          <div className="inputBox">
            {action === 'create' && ( // if post action is create, display image on the left
              <div className="userImgContainer">
                <div className="userImg" style={{ backgroundImage: `url(${userLogo})` }} />
              </div>
            )}
            <PostInputMentions
              content={content}
              onChange={this.handleChange}
              placeholder={intl.formatMessage({
                id: 'post.placeholder',
                defaultMessage: "What's on your mind?",
              })}
            />
          </div>
          <div className="actionBar">
            <BtnUploadFile setListFiles={this.attachDocuments.bind(this)} />
            <Box row>
              {action === 'update' && (
                <Button btnType="secondary" type="button" onClick={this.handleCancel} tw="mr-2">
                  <FormattedMessage id="feed.object.cancel" defaultMessage="Cancel" />
                </Button>
              )}
              <button type="submit" className="btn btn-primary" disabled={!!uploading || content === ''}>
                {uploading && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id={`post.create.btn${submitBtnText}`} defaultMessage={submitBtnText} />
              </button>
            </Box>
          </div>
        </form>
        {documents && this.renderPreviewDocuments(documents)}
      </div>
    );
  }
}
export default injectIntl(FormPost);
