import { useState, useEffect, useCallback, FC } from 'react';
import { FormattedMessage } from 'react-intl';
import PostDisplay from '~/components/Feed/Posts/PostDisplay';
import PostCreate from './Posts/PostCreate';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import useUserData from '~/hooks/useUserData';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
interface Props {
  displayCreate: boolean;
  feedId: number | string;
  isAdmin: boolean;
  needToJoinMsg?: boolean;
}
const Feed: FC<Props> = ({ displayCreate = false, feedId, isAdmin = false, needToJoinMsg = 'undefined' }) => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [loadBtnCount, setLoadBtnCount] = useState(1);
  const [hideLoadBtn, setHideLoadBtn] = useState(true);
  const api = useApi();
  const { userData, userDataError } = useUserData();
  useEffect(() => {
    loadPosts(loadBtnCount);
    if (loadBtnCount === 1) {
      setLoading(true);
    }
  }, []);
  const loadPosts = useCallback(
    (currentPage) => {
      const itemsPerQuery = 5; // number of users per query calls (load more btn click)
      if (currentPage > 1) setLoadingBtn(true);
      const pageScroll = window.pageYOffset; // get whole page offSetTop
      api
        .get(`/api/feeds/${feedId}?items=${itemsPerQuery}&page=${currentPage}&order=desc`)
        .then((res) => {
          const totalPages = res.headers['total-pages']; // get total pages from response headers
          const newPosts = res.data;
          setPosts((prevPosts) => [...prevPosts, ...newPosts]);
          setLoading(false);
          setLoadingBtn(false);
          setLoadBtnCount(currentPage + 1);
          currentPage > 1 && window.scrollTo(0, pageScroll); // when clicking the load more button (not the first time it launch this function), force page to stay at pageScroll (else it would focus you at bottom of the list of posts)
          // if (currentPage == totalPages) {
          if (currentPage === parseInt(totalPages, 10)) {
            // hide btn when the last "page" has been called}
            setHideLoadBtn(true);
          } else setHideLoadBtn(false);
        })
        .catch(() => {
          setLoading(false);
          setLoadingBtn(false);
        });
    },
    [api, feedId]
  );

  const getFeedApi = () => {
    api
      .get(`/api/feeds/${feedId}?items=15`)
      .then((res) => {
        setPosts(res.data);
      })
      .catch((err) => {
        console.error(`Couldn't GET feed with id=${feedId}`, err);
      });
  };

  const refresh = () => {
    // on refresh, reset feed and get only 5 latest posts
    getFeedApi();
  };

  const joinMessage = needToJoinMsg
    ? {
        id: 'general.needToJoinMsg.private',
        defaultMessage: 'Want to post? Ask to join with the button, then wait for an admin to approve your request!',
      }
    : { id: 'general.needToJoinMsg', defaultMessage: 'Want to post? Click on the join button then refresh the page' };

  return (
    <div className="feed">
      {needToJoinMsg !== 'undefined' && !displayCreate && (
        <Alert
          type="info"
          message={<FormattedMessage id={joinMessage.id} defaultMessage={joinMessage.defaultMessage} />}
        />
      )}
      {displayCreate && userData && (
        <PostCreate feedId={feedId} refresh={refresh} userImg={userData.logo_url_sm} userId={userData.id} />
      )}
      <Loading active={loading} height="200px">
        {posts.length !== 0 &&
          posts.map((post, index) => (
            <PostDisplay post={post} key={index} feedId={feedId} refresh={refresh} user={userData} isAdmin={isAdmin} />
          ))}
        {((posts.length > 5 && !hideLoadBtn) || !hideLoadBtn) && (
          <Button
            btnType="button"
            style={{ display: 'flex', margin: 'auto', justifyContent: 'center', alignItems: 'center' }}
            onClick={() => loadPosts(loadBtnCount)}
            disabled={loadingBtn}
          >
            {loadingBtn && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id="general.load" defaultMessage="Load more" />
          </Button>
        )}
      </Loading>
    </div>
  );
};
export default Feed;
