import { useState } from 'react';
import FormComment from './FormComment';
import { findMentions, noHTMLMentions, transformMentions } from '~/components/Feed/Mentions';
import { useApi } from '~/contexts/apiContext';
import ReactGA from 'react-ga';

export default function CommentUpdate({
  postId,
  content: contentProp = '',
  commentId = 1,
  user = '',
  refresh,
  closeOrCancelEdit,
  index,
}) {
  const [content, setContent] = useState(noHTMLMentions(contentProp));
  const api = useApi();
  const [uploading, setUploading] = useState(false);
  const handleChange = (newContent) => {
    setContent(newContent);
  };
  const handleSubmit = () => {
    const mentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    const updateJson = {
      comment: {
        content: contentNoMentions,
      },
    };
    if (mentions) {
      updateJson.comment.mentions = mentions;
    }
    setUploading(true);
    api
      .patch(`/api/posts/${postId}/comment/${commentId}`, updateJson)
      .then(() => {
        // record event to Google Analytics
        ReactGA.event({ category: 'Post', action: 'comment update', label: `[${user.id},${postId},${commentId}]` });
        window.gtag('event', 'comment update', { category: 'Post', userId: user.id, postId, commentId });
        closeOrCancelEdit(); // close comment update box after update
        refresh();
        setUploading(false);
        // send event to google analytics
      })
      .catch((err) => {
        console.error(`Couldn't PATCH post comment in post with id=${postId}`, err);
        setUploading(false);
      });
  };
  return (
    <FormComment
      action="update"
      content={content}
      handleChange={handleChange}
      handleSubmit={handleSubmit}
      cancelEdit={() => closeOrCancelEdit(index)}
      index={index}
      uploading={uploading}
      user={user}
    />
  );
}
