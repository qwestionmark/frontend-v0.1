/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import { WidthProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import Chips from '../Chip/Chips';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  clapsCount: number;
  postsCount?: number;
  members_count: number;
  has_saved: boolean;
  banner_url?: string;
  width?: WidthProps['width'];
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
}
const CommunityCard: FC<Props> = ({
  id,
  title,
  shortTitle,
  short_description,
  clapsCount,
  postsCount,
  members_count,
  has_saved,
  banner_url = '/images/default/default-group.jpg',
  width,
  cardFormat,
  source,
  skills,
}) => {
  const groupUrl = `/community/${id}/${shortTitle}`;
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  return (
    <Card imgUrl={banner_url} isImgSmall={cardFormat === 'compact'} href={groupUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={groupUrl} passHref>
          <Title pr={2}>
            <H2 fontSize={TitleFontSize}>{title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="communities" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <CardDesc flex="1">{short_description}</CardDesc>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            href: `/search/groups/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={`/community/${id}/${shortTitle}`}
          // color={theme.colors.primary}
          color="#F2F4F8"
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
          <CardData value={members_count} title={textWithPlural('member', members_count)} />
          {postsCount > 0 && <CardData value={postsCount} title={textWithPlural('post', postsCount)} />}
          <CardData value={clapsCount} title={textWithPlural('clap', clapsCount)} />
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);
const CardDesc = styled(Box)`
  overflow: hidden;
  word-break: break-word;
  display: -webkit-box;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
`;

export default CommunityCard;
