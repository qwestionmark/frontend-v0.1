/* eslint-disable camelcase */
import { FC, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Link from 'next/link';
// import MembersList from "~/components/Members/MembersList";
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from '~/components/Tools/Forms/FormResourcesComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormToggleComponent from '~/components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import communityFormRules from './communityFormRules.json';
import { toAlphaNum } from '~/components/Tools/Nickname';
import Alert from '../Tools/Alert';
import Box from '../Box';
import { Community } from '~/types';
// import "./CommunityForm.scss";

interface Props {
  mode: 'edit' | 'create';
  community: Community;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const CommunityForm: FC<Props> = ({
  mode,
  community,
  sending = false,
  hasUpdated = false,
  handleChange,
  handleSubmit,
}) => {
  const validator = new FormValidator(communityFormRules);
  const [stateValidation, setStateValidation] = useState({});
  const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } =
    stateValidation || '';
  const intl = useIntl();

  const generateSlug = (grouTitle) => {
    let proposalShortName = grouTitle.trim();
    proposalShortName = toAlphaNum(proposalShortName);
    return proposalShortName;
  };

  const handleChangeGroup = (key, content) => {
    let proposalShortName;
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      proposalShortName = generateSlug(content);
      handleChange('short_title', proposalShortName);
    }
    /* Validators start */
    const state = {};
    state[key] = content;
    // Check proposalShortName
    if (key === 'title') {
      state.short_title = proposalShortName;
    }
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      // Update short_title too only if title has been changed
      if (key === 'title') {
        newStateValidation.valid_short_title = validation.short_title;
      }
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    handleChange(key, content);
  };

  const handleSubmitGroup = () => {
    /* Validators control before submit */
    let firsterror = true;
    const validation = validator.validate(community);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  const renderBtnsForm = () => {
    const textAction = mode === 'edit' ? 'Update' : 'Create';
    const urlBack = mode === 'edit' ? `/community/${community.id}/${community.short_title}` : '/search/groups';

    return (
      <>
        <Box row justifyContent="center" mt={5} mb={3}>
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary">
                <FormattedMessage id="entity.form.back" defaultMessage="Back" />
              </button>
            </a>
          </Link>
          <button
            type="button"
            className="btn btn-primary"
            onClick={handleSubmit}
            style={{ marginLeft: '10px' }}
            disabled={sending}
          >
            {sending && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
          </button>
        </Box>
        {hasUpdated && (
          <Alert
            type="success"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        )}
      </>
    );
  };

  return (
    <form className="communityForm">
      <FormDefaultComponent
        content={community.title}
        errorCodeMessage={valid_title ? valid_title.message : ''}
        id="title"
        isValid={valid_title ? !valid_title.isInvalid : undefined}
        onChange={handleChangeGroup}
        mandatory
        title={intl.formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
        placeholder={intl.formatMessage({
          id: 'community.form.title.placeholder',
          defaultMessage: 'An Awesome Group',
        })}
      />
      {/* {mode === "create" && */}
      <FormDefaultComponent
        content={community.short_title}
        errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
        id="short_title"
        isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
        mandatory
        onChange={handleChangeGroup}
        pattern={/[A-Za-z0-9]/g}
        title={intl.formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
        prepend="#"
        placeholder={intl.formatMessage({
          id: 'community.form.short_title.placeholder',
          defaultMessage: 'AnAwesomeGroup',
        })}
      />
      <FormTextAreaComponent
        content={community.short_description}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        id="short_description"
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory
        maxChar={240}
        onChange={handleChangeGroup}
        title={intl.formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
        rows={3}
        placeholder={intl.formatMessage({
          id: 'community.form.short_description.placeholder',
          defaultMessage: 'The group briefly explained',
        })}
      />
      {mode === 'edit' && (
        <FormWysiwygComponent
          id="description"
          title={intl.formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
          placeholder={intl.formatMessage({
            id: 'community.form.description.placeholder',
            defaultMessage: 'Describe your group in detail, with formatted text, images...',
          })}
          content={community.description}
          show
          onChange={handleChangeGroup}
        />
      )}
      {mode === 'edit' && (
        <FormImgComponent
          type="banner"
          id="banner_url"
          imageUrl={community.banner_url}
          itemId={community.id}
          itemType="communities"
          title={intl.formatMessage({ id: 'community.info.banner_url', defaultMessage: 'Group banner' })}
          content={community.banner_url}
          defaultImg="/images/default/default-group.jpg"
          onChange={handleChangeGroup}
        />
      )}
      <FormInterestsComponent
        content={community.interests}
        errorCodeMessage={valid_interests ? valid_interests.message : ''}
        mandatory
        onChange={handleChangeGroup}
        title={intl.formatMessage({ id: 'entity.info.interests', defaultMessage: 'Interests' })}
      />
      <FormSkillsComponent
        content={community.skills}
        errorCodeMessage={valid_skills ? valid_skills.message : ''}
        id="skills"
        type="group"
        isValid={valid_skills ? !valid_skills.isInvalid : undefined}
        mandatory
        onChange={handleChangeGroup}
        title={intl.formatMessage({ id: 'entity.info.skills', defaultMessage: 'Skills' })}
        placeholder={intl.formatMessage({
          id: 'general.skills.placeholder',
          defaultMessage: 'Big data, Web Development, Open Science...',
        })}
      />
      <FormResourcesComponent
        content={community.ressources}
        id="ressources"
        type="group"
        mandatory={false}
        onChange={handleChangeGroup}
        title={intl.formatMessage({ id: 'entity.info.resources', defaultMessage: 'Resources' })}
        placeholder={intl.formatMessage({
          id: 'general.resources.placeholder',
          defaultMessage: '3D printers, Biolab, Makerspace...',
        })}
      />
      {mode === 'edit' && (
        <FormToggleComponent
          id="is_private"
          warningMsg="By choosing 'public', any member can join your group without your approval."
          warningMsgId="community.info.publicPrivateToggleMsg"
          title={intl.formatMessage({ id: 'entity.info.is_private', defaultMessage: 'Confidentiality' })}
          choice1={<FormattedMessage id="general.private" defaultMessage="Private" />}
          choice2={<FormattedMessage id="general.public" defaultMessage="Public" />}
          color1="#27B40E"
          color2="#F9530B"
          isChecked={community.is_private}
          onChange={handleChangeGroup}
        />
      )}
      {renderBtnsForm()}
    </form>
  );
};
export default CommunityForm;
