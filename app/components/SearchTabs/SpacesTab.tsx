import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const SpacesTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="Space"
        onSearchStateChange={setSearchState}
        refinements={
          [
            // { attribute: 'skills' },
            // { attribute: 'challenge.title', searchable: false },
            // { attribute: 'program.title', searchable: false },
            // { attribute: 'status', searchable: false, showmore: false },
          ]
        }
        sortByItems={[
          {
            label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
            index: 'Space',
          },
        ]}
      />
    </div>
  );
};
