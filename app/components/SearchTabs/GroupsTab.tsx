import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const GroupsTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="Community"
        onSearchStateChange={setSearchState}
        refinements={[{ attribute: 'skills' }, { attribute: 'ressources' }]}
        sortByItems={[
          {
            label: formatMessage({
              id: 'general.filter.newly_updated',
              defaultMessage: 'Newly updated',
            }),
            index: 'Community_updated_at',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date2',
              defaultMessage: 'Date added (newest)',
            }),
            index: 'Community_id_des',
          },
          {
            label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
            index: 'Community',
          },
          {
            label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
            index: 'Community_members',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date1',
              defaultMessage: 'Date added (oldest)',
            }),
            index: 'Community_id_asc',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.alpha1',
              defaultMessage: 'Alphabetical (A-Z)',
            }),
            index: 'Community_title_asc',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.alpha2',
              defaultMessage: 'Alphabetical (Z-A)',
            }),
            index: 'Community_title_desc',
          },
        ]}
      />
    </div>
  );
};
