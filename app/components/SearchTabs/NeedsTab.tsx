import React from 'react';
import { useIntl } from 'react-intl';
import Search from '~/components/Search/Search';
import { useSearchStateContext } from '~/contexts/searchStateContext';

export const NeedsTab: React.FC = () => {
  const { formatMessage } = useIntl();
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <div className="tab-pane active" id={index}>
      <Search
        searchState={searchState}
        index="Need"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'skills' },
          { attribute: 'ressources' },
          { attribute: 'project.title' },
          { attribute: 'status', searchable: false, showmore: false },
        ]}
        sortByItems={[
          {
            label: formatMessage({
              id: 'general.filter.newly_updated',
              defaultMessage: 'Newly updated',
            }),
            index: 'Need_updated_at',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date2',
              defaultMessage: 'Date added (newest)',
            }),
            index: 'Need_id_des',
          },
          {
            label: formatMessage({
              id: 'general.filter.object.date1',
              defaultMessage: 'Date added (oldest)',
            }),
            index: 'Need_id_asc',
          },
          { label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }), index: 'Need' },
          {
            label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
            index: 'Need_members',
          },
        ]}
      />
    </div>
  );
};
