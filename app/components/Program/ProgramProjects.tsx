import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { layout } from 'styled-system';
import { useApi } from '~/contexts/apiContext';
import useChallenges from '~/hooks/useChallenges';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import styled from '~/utils/styled';
import Box from '../Box';
import Filters from '../Filters';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const OverflowGradient = styled.div`
  ${layout};
  width: 3rem;
  height: 100%;
  position: absolute;
  right: 0;
  /* background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%); */
  background: ${(p) =>
    `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
`;

const ProgramProjects = ({ programId }) => {
  const projectsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const [projectsEndpoints, setProjectsEndpoint] = useState(
    `/api/programs/${programId}/projects?items=${projectsPerQuery}`
  );
  const { data: dataProjects, response } = useGet<{ projects: Project[] }>(projectsEndpoints);
  const [projects, setProjects] = useState([]);
  const { dataChallenges, challengesError } = useChallenges('programs', programId);
  const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData } = useUserData();
  const [currentPage, setCurrentPage] = useState(2);
  const [hasLoadOnce, setHasLoadOnce] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const api = useApi();
  useEffect(() => {
    // set dataProjects from first api call, only once, or when filtering projects by challenge
    if (!hasLoadOnce && dataProjects?.projects) setProjects(dataProjects.projects);
  }, [dataProjects]);

  const loadMore = () => {
    setIsLoading(true);
    api.get(`/api/programs/${programId}/projects?items=${projectsPerQuery}&page=${currentPage}`).then((res) => {
      setIsLoading(false);
      const nextProjects = res.data.projects;
      if (projects) setProjects([...projects, ...nextProjects]);
      setHasLoadOnce(true); // set to true so it doesn't setProjects from first call
      setCurrentPage(currentPage + 1); // increment current page count
    });
  };

  const onFilterChange = (e) => {
    const id = e.target.name;
    const itemsNb = projects.length > projectsPerQuery ? projects.length : projectsPerQuery;
    if (id) {
      setSelectedChallengeFilterId(Number(id));
      setProjectsEndpoint(`/api/challenges/${id}/projects?items=${itemsNb}`);
    } else {
      setSelectedChallengeFilterId(undefined);
      setProjectsEndpoint(`/api/programs/${programId}/projects?items=${projectsPerQuery}`);
    }
  };

  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.projects.text"
            defaultMessage="To participate to this program, participate to as many of its projects. Check out the projects already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toJoinProject}'}
              values={{
                toJoinProject: <FormattedMessage id="program.signinCta.project" defaultMessage="to join a project" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel={{ id: 'challenge.list.all.title', defaultMessage: 'All challenges' }}
            content={dataChallenges
              // filter to hide draft challenges
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
          />
        </Box>
        {!dataProjects ? (
          <Loading />
        ) : projects?.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
            {projects
              ?.filter(({ challenges }) =>
                // display only projects that have been accepted to the challenge (go through all project's challenges and return true if one of them has project_status === "accepted" )
                Object.values(challenges).some((challenge) => challenge.project_status === 'accepted')
              )
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  clapsCount={project.claps_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                />
              ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          response?.headers['total-count'] > projectsPerQuery && currentPage <= response?.headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={loadMore} disabled={isLoading}>
                {isLoading && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="general.load" defaultMessage="Load more" />
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default ProgramProjects;
