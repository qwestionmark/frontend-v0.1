/* eslint-disable camelcase */
import React, { useState, FC } from 'react';
import { FormattedMessage, FormattedDate, useIntl } from 'react-intl';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { flexbox, space, typography, layout, display } from 'styled-system';
import styled from '~/utils/styled';
import BtnFollow from '../Tools/BtnFollow';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import Box from '~/components/Box';
import { useRouter } from 'next/router';
import A from '../primitives/A';
import BtnSave from '../Tools/BtnSave';
import { useTheme } from '~/utils/theme';
import H1 from '~/components/primitives/H1';
import { textWithPlural } from '~/utils/managePlurals';
import useUserData from '~/hooks/useUserData';
import { Program } from '~/types';

// import "./ProgramHeader.scss";

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
}
`;
interface Props {
  program: Program;
  lang: string;
}
const ProgramHeader: FC<Props> = ({ program, lang = 'en' }) => {
  const { formatMessage } = useIntl();
  const router = useRouter();
  const { userData } = useUserData();
  const [has_followed, set_has_followed] = useState(program?.has_followed);
  const [has_saved, set_has_saved] = useState(program?.has_saved);
  const theme = useTheme();

  const Stats = ({ value, title }) => (
    <Box justifyContent="center" alignItems="center">
      <div>{value}</div>
      <div>{title}</div>
    </Box>
  );

  const {
    id,
    needs_count,
    // status,
    members_count,
    logo_url,
    title,
    title_fr,
    projects_count,
  } = program;

  return (
    // <Box p={[4, undefined, 2]} width="100%">
    <Box width="100%" pt={4}>
      <Box flexDirection={['row', undefined, 'column']} justifyContent="center">
        <Img
          src={logo_url || 'images/jogl-logo.png'}
          style={{ objectFit: 'contain', backgroundColor: 'white' }}
          mr={4}
          mb={[undefined, undefined, 2]}
          width={['5rem', '7rem', '9rem', '10rem']}
          height={['5rem', '7rem', '9rem', '10rem']}
          alignSelf="center"
          alt="program logo"
        />
        <Box alignItems="center">
          <Box alignItems="center" alignSelf="center">
            <H1 fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} textAlign={['left', 'center']}>
              {/* display different field depending on language */}
              {lang === 'fr' && title_fr ? title_fr : title}
            </H1>
            <Box row pt={3} alignSelf={['flex-start', 'center']}>
              {userData && (
                <>
                  <Box pr={5}>
                    <BtnSave itemType="programs" itemId={id} saveState={has_saved} />
                  </Box>
                  <BtnFollow followState={has_followed} itemType="programs" itemId={id} />
                </>
              )}
              <ShareBtns type="program" />
            </Box>
          </Box>
          {program.is_admin && (
            <Box mt={5}>
              <Link href={`/program/${program.short_title}/edit`}>
                <a style={{ display: 'flex', justifyContent: 'center' }}>
                  <FontAwesomeIcon icon="edit" />
                  <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
                </a>
              </Link>
            </Box>
          )}
        </Box>
      </Box>
      <Box
        textAlign="center"
        alignItems="center"
        justifyContent="space-around"
        spaceX={[4, 6]}
        row
        pt={[6, undefined, 8]}
        pb={2}
        px={[0, undefined, 2]}
      >
        <A href={`/program/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
          <Stats value={projects_count} title={textWithPlural('project', projects_count)} />
        </A>
        <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8}></Box>
        <A href={`/program/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
          <Stats value={needs_count} title={textWithPlural('need', needs_count)} />
        </A>
        <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8}></Box>
        <A href={`/program/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
          <Stats value={members_count} title={textWithPlural('participant', members_count)} />
        </A>
      </Box>
    </Box>
  );
};

export default ProgramHeader;
