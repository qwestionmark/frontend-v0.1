// eslint-disable-next-line no-unused-vars
import { useState, FC } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import AsyncSelect from 'react-select/async';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from '~/utils/styled';

const UserLabel = styled(Box)`
  align-items: center;
  div {
    font-size: 15px;
    color: color('gray-dark');
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;

interface Props {
  programId: number;
  boardId: number;
  onMembersAdded: () => void;
}
const InviteBoardMembers: FC<Props> = ({ programId, boardId, onMembersAdded }) => {
  const [usersArray, setUsersArray] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const modal = useModal();
  const { formatMessage } = useIntl();
  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <UserLabel row>
      <img src={logo_url} />
      <div>{label}</div>
    </UserLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('User'); // User
  let algoliaMembers = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'nickname', 'first_name', 'last_name', 'logo_url_sm'],
        hitsPerPage: 4,
      })
      .then((content) => {
        if (content) {
          algoliaMembers = content.hits;
        } else {
          algoliaMembers = [''];
        }
        algoliaMembers = algoliaMembers.map((user) => {
          return { value: user.id, label: `${user.first_name} ${user.last_name}`, logo_url: user.logo_url_sm };
        });
        resolve(algoliaMembers);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChangeUser = (user) => {
    const newBoardMember = [];
    if (user) {
      newBoardMember.push(user.value);
      setUsersArray(newBoardMember);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    usersArray.map((userId) => {
      api
        .post(`/api/programs/${programId}/boards/${boardId}/users/${userId}`)
        .then((res) => {
          setInviteSend(true);
          setTimeout(() => {
            // close
            modal.setIsOpen(false);
            resetState();
            onMembersAdded();
          }, 1400);
        })
        .catch(() => {
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 5000);
        });
    });
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <div className="input-group">
          <AsyncSelect
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={formatMessage({
              id: 'member.invite.user.placeholder',
              defaultMessage: 'Select user(s) to add',
            })}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: null, IndicatorSeparator: null }}
            isClearable
          />
        </div>
      </div>
      <div className="text-center btnZone">
        <button type="submit" className="btn btn-primary btn-block" disabled={!usersArray || usersArray.length === 0}>
          {inviteSend ? (
            <FormattedMessage id="member.invite.btnSendEnded" defaultMessage="Invitation sent" />
          ) : (
            <FormattedMessage id="member.invite.btnSend" defaultMessage="Invite" />
          )}
        </button>
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          <FormattedMessage id="err-" defaultMessage="An error has occurred" />
        </div>
      )}
    </form>
  );
};

export default InviteBoardMembers;
