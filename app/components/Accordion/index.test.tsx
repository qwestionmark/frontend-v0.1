import { render, screen } from '../../test/testUtils';
import user from '@testing-library/user-event';
import Accordion from './index';

const accordionValues = [
  { content: 'Content1', title: 'Title1' },
  { content: 'Content2', title: 'Title2' },
];
describe('Accordion component', () => {
  it('matches snapshot', () => {
    const { asFragment } = render(<Accordion values={accordionValues} />, {});
    expect(asFragment()).toMatchSnapshot();
  });
  it("Doesn't show the content when it's not clicked", () => {
    render(<Accordion values={accordionValues} />, {});
    expect(screen.getByText(accordionValues[1].content)).not.toBeVisible();
  });
  it('Clicks on a title, displays content', () => {
    const { getByText } = render(<Accordion values={accordionValues} />, {});
    const $button = getByText(accordionValues[1].title);
    user.click($button);
    expect(screen.getByText(accordionValues[1].content)).toBeVisible();
  });
});
