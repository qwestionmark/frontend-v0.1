import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Accordion as AccordionReach, AccordionButton, AccordionItem, AccordionPanel } from '@reach/accordion';
import React, { FC, useState } from 'react';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box from '../Box';
import Card from '../Card';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import '@reach/accordion/styles.css';

interface Props {
  values: { title: string; content: string }[];
  hasHtmlContent?: boolean;
}
const Accordion: FC<Props> = ({ values, hasHtmlContent = false }) => {
  const [index, setIndex] = useState(0);
  const theme = useTheme();
  return (
    <CustomAccordion index={index} onChange={(value) => setIndex(value)}>
      {values.map((value, i) => (
        <AccordionItem key={i}>
          <CustomAccordionButton>
            <Card>
              <Box
                flexDirection="row"
                justifyContent="space-between"
                spaceY={0}
                fontWeight="700"
                color={i === index ? theme.colors.primary : theme.colors.dark}
              >
                <span>{value.title}</span>
                <span>
                  <FontAwesomeIcon icon={i === index ? faAngleUp : faAngleDown} />
                </span>
              </Box>
              <AccordionPanel>
                {!hasHtmlContent ? value.content : <InfoHtmlComponent title="" content={value.content} />}
              </AccordionPanel>
            </Card>
          </CustomAccordionButton>
        </AccordionItem>
      ))}
    </CustomAccordion>
  );
};

const CustomAccordion = styled(AccordionReach)`
  > * + * {
    margin-top: ${(p) => p.theme.space[3]};
  }
`;
const CustomAccordionButton = styled(AccordionButton)`
  width: 100%;
  padding: ${(p) => p.theme.space[2]} 0 0;
  text-align: left;
`;
export default Accordion;
