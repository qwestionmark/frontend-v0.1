import React from 'react';
import Link from 'next/link';
// import Image from 'next/image';
import { FormattedMessage } from 'react-intl';
import P from '../primitives/P';
import Image2 from '../Image2';
import 'twin.macro';
import Button from '../primitives/Button';
import { MediumSquare, Twitter, Facebook, Instagram, Linkedin, Gitlab } from '@emotion-icons/boxicons-logos';

// import "./Footer.scss";
const Footer = () => {
  return (
    <footer id="footer">
      <div className="footer-top">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-4 col-md-4 col-lg-2 footCol" style={{ marginTop: '0px' }}>
              <Image2 src="/images/logo_JOGL-02.png" alt="jogl logo rocket" unsized quality={50} />
            </div>
            <div className="col-12 col-sm-8 col-lg-4 footCol">
              <h3>
                <FormattedMessage id="footer.aboutJOGL" defaultMessage="About JOGL" />
              </h3>
              <p tw="pb-3">
                <FormattedMessage
                  id="footer.JOGLdescription"
                  defaultMessage="Just One Giant Lab (JOGL), is a non profit headquarted in Paris, France. JOGL is a decentralized open research and innovation laboratory. The goal of JOGL is to catalyze the collective creation of knowledge and solutions to resolve humanity's most urgent challenges."
                />
              </p>
              <hr />
              <div tw="flex flex-col">
                <FormattedMessage
                  id="footer.donate.text"
                  defaultMessage="Donate to JOGL to support inclusive and open science."
                />
                <a className="custom-dbox-popup" target="_blank" href="https://donorbox.org/donate-to-jogl">
                  <Button tw="mt-4">
                    <FormattedMessage id="footer.donate.btn" defaultMessage="Donate" />
                  </Button>
                </a>
              </div>
            </div>
            <div className="col-12 col-sm-4 col-md-4 col-lg-3 footer-contact footCol">
              <h3>
                <FormattedMessage id="footer.general" defaultMessage="General" />
              </h3>
              <p>
                <a href="https://jogl.io" target="_blank" rel="noopener">
                  <FormattedMessage id="footer.aboutJOGL" defaultMessage="About JOGL" />
                </a>
              </p>
              <p>
                <a href="https://jogl.tawk.help/" target="_blank" rel="noopener">
                  <FormattedMessage id="faq.title" defaultMessage="FAQs" />
                </a>
              </p>
              <p>
                <Link href="/ethics-pledge">
                  <a>
                    <FormattedMessage id="footer.ethicsPledge" defaultMessage="Awareness and Ethics Pledge" />
                  </a>
                </Link>
              </p>
              <p>
                <Link href="/data">
                  <a>
                    <FormattedMessage id="footer.data" defaultMessage="User data policy" />
                  </a>
                </Link>
              </p>
              <p>
                <Link href="/terms">
                  <a>
                    <FormattedMessage id="footer.termsConditions" defaultMessage="Terms and Conditions" />
                  </a>
                </Link>
              </p>
              <p>
                <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener">
                  <FormattedMessage id="footer.contribute" defaultMessage="Contribute to the code" />
                </a>
              </p>
              <p>
                <a href="https://jogl.io/#jobs" target="_blank" rel="noopener">
                  <FormattedMessage id="footer.jobs" defaultMessage="Jobs opportunities" />
                </a>
              </p>
            </div>
            <div className="col-12 col-sm-8 col-lg-3 footer-contact footCol">
              <h3>Contact</h3>
              <div className="d-flex flex-row">
                <p>Centre de Recherches Interdisciplinaires (CRI) - 8 bis rue Charles V, 75004, Paris</p>
              </div>
              <div className="contact">
                <a href="mailto:hello@jogl.io">
                  <Button>
                    <FormattedMessage id="footer.contactUs" defaultMessage="Contact us" />
                  </Button>
                </a>
                <p className="press">
                  <FormattedMessage id="footer.pressContact" defaultMessage="Press contact: " />
                  <a href="mailto:press@jogl.io">press[at]jogl.io</a>
                </p>
              </div>
              <div tw="gap-3 flex flex-wrap mb-2">
                <a href="https://twitter.com/justonegiantlab" target="_blank" rel="noopener">
                  <Twitter size={30} title="Twitter icon" tw="text-gray-500 hover:text-brands-twitter" />
                </a>
                <a href="https://www.facebook.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Facebook size={30} title="Facebook icon" tw="text-gray-500 hover:text-brands-facebook" />
                </a>
                <a href="https://www.linkedin.com/company/jogl/about/" target="_blank" rel="noopener">
                  <Linkedin size={30} title="Linkedin icon" tw="text-gray-500 hover:text-brands-linkedin" />
                </a>
                <a href="https://www.instagram.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Instagram size={30} title="Instagram icon" tw="text-gray-500 hover:text-brands-instagram" />
                </a>
                <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener">
                  <Gitlab size={30} title="Gitlab icon" tw="text-gray-500 hover:text-brands-gitlab" />
                </a>
                <a href="https://medium.com/justonegiantlab/latest" target="_blank" rel="noopener">
                  <MediumSquare size={30} title="medium icon" tw="text-gray-500 hover:text-black" />
                </a>
              </div>
              <P pt={4} fontSize="93%">
                <FormattedMessage id="footer.bugOrRequest.text" defaultMessage="Found a bug? Have a idea?" />
                &nbsp;
                <a href="https://gitlab.com/JOGL/JOGL/-/issues/new" target="_blank" rel="noopener">
                  <FormattedMessage id="footer.bugOrRequest.link" defaultMessage="Click here!" />
                </a>
              </P>
              <P fontSize="93%">
                <FormattedMessage id="footer.whatNewChangelog.text" defaultMessage="What's new on JOGL?" />
                &nbsp;
                <a
                  href="https://gitlab.com/JOGL/frontend-v0.1/-/blob/master/CHANGELOG.md"
                  target="_blank"
                  rel="noopener"
                >
                  <FormattedMessage id="footer.whatNewChangelog.link" defaultMessage="Check the changelog" />
                </a>
              </P>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-7 col-md-9 footer-bottom--copyright">
              <p>Just One Giant Lab - {new Date().getFullYear()} | v0.9.6</p>
              <p>
                <FormattedMessage
                  id="footer.CC"
                  defaultMessage="Content on this site is licensed under the Creative Commons Attribution 4.0 International licence (CC-BY 4.0)"
                />
              </p>
            </div>
            <div className="col-12 col-sm-5 col-md-3 footer-bottom--right">
              <button type="button" className="btn btn-warning repport">
                <a href="mailto:support@jogl.io" target="_blank" rel="noopener">
                  <FormattedMessage id="footer.help" defaultMessage="Need help?" />
                </a>
              </button>
              <a href="https://www.algolia.com/" target="_blank" rel="noopener">
                <Image2 src="/images/search-by-algolia.svg" alt="Search by algolia" width="168px" height="24px" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default React.memo(Footer);
