import React, { FC, Fragment, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Chip from '~/components/Chip/Chip';
import Box from '../Box';
import A from '../primitives/A';

interface Props {
  data: any;
  overflowLink?: string;
  overflowText?: string;
  color: string;
  showCount: number;
  smallChips?: boolean;
}

const Chips: FC<Props> = ({ data, overflowLink, overflowText, color, showCount = 100, smallChips = false }) => {
  const overFlowingChipsLength = data.length <= showCount ? undefined : data.length - showCount;
  const [showCount2, setShowCount2] = useState(showCount);
  return (
    <Box row flexWrap="wrap">
      {/* chips list */}
      {[...data].splice(0, showCount2).map((item, i) => (
        <Fragment key={i}>
          <Box>
            <A href={item.href} key={i} noStyle>
              {/* set different maxLength depending on if chip is small (in cards) or other factors */}
              <Chip color={color} maxLength={smallChips ? 15 : overflowLink ? 23 : 40} smallChips={smallChips}>
                {item.title}
              </Chip>
            </A>
          </Box>
        </Fragment>
      ))}
      {/* moreChips links to an object */}
      {overFlowingChipsLength && overflowLink && (
        <A href={overflowLink} noStyle>
          <Box style={{ cursor: 'pointer' }}>
            <Chip color={color} smallChips={smallChips} hasOpacity>
              + {overFlowingChipsLength.toString()}
            </Chip>
          </Box>
        </A>
      )}
      {/* if moreChips allow you to see more/less, or jumps you to another div */}
      {overFlowingChipsLength && showCount === showCount2 && overflowText && (
        <Box
          onClick={() => overflowText === 'seeMore' && setShowCount2(data.length)}
          className={overflowText === 'userSkill' ? 'moreSkills' : 'userResource' && 'moreResources'}
          style={{ cursor: 'pointer' }}
        >
          <Chip color={color} smallChips={smallChips} hasOpacity>
            + {overFlowingChipsLength.toString()}
          </Chip>
        </Box>
      )}
      {/* show "see less" if user has clicked "see more" */}
      {overFlowingChipsLength && showCount !== showCount2 && (
        <Box onClick={() => setShowCount2(showCount)} style={{ cursor: 'pointer' }}>
          <Chip color={color} smallChips={smallChips} hasOpacity>
            <FormattedMessage id="general.showless" defaultMessage="Show less" />
          </Chip>
        </Box>
      )}
    </Box>
  );
};
export default Chips;
