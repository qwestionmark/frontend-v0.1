import { connectCurrentRefinements } from 'react-instantsearch-dom';
import { FormattedMessage } from 'react-intl';

const ClearFiltersMobile = ({ items, refine }) => {
  function closeFilters() {
    refine(items);
    document.body.classList.remove('filtering');
    // containerRef.current.scrollIntoView();
    // force click on "see results" button
    document.querySelector('.container-filters-footer .button-primary').click();
  }

  return (
    <div className="ais-ClearRefinements">
      <button className="ais-ClearRefinements-button" onClick={closeFilters}>
        <FormattedMessage id="algolia.filters.clear2" defaultMessage="Reset filters" />
      </button>
    </div>
  );
};

export default connectCurrentRefinements(ClearFiltersMobile);
