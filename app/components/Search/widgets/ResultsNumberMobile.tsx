import { connectStats } from 'react-instantsearch-dom';
import { FormattedMessage } from 'react-intl';
import { formatNumber } from '~/utils/utils';

const ResultsNumberMobile = ({ nbHits }) => (
  <div>
    <strong>{formatNumber(nbHits)}</strong> <FormattedMessage id="algolia.results" defaultMessage="results" />
  </div>
);

export default connectStats(ResultsNumberMobile);
