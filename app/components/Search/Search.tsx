import React from 'react';
import { useIntl } from 'react-intl';
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
  ClearRefinements,
  // ToggleRefinement,
  Hits,
  HitsPerPage,
  Panel,
  SearchBox,
} from 'react-instantsearch-dom';
import algoliasearch from 'algoliasearch/lite';
import UserCard from '../User/UserCard';
import ProjectCard from '~/components/Project/ProjectCard';
import NeedCard from '~/components/Need/NeedCard';
import CommunityCard from '~/components/Community/CommunityCard';
import ChallengeCard from '~/components/Challenge/ChallengeCard';
import SpaceCard from '~/components/Space/SpaceCard';

import {
  ClearFiltersMobile,
  NoSearchResults,
  ResultsNumberMobile,
  SaveFiltersMobile,
} from '~/components/Search/widgets';
import Box from '../Box';
import { defaultSdgsInterests } from '~/utils/utils';

// import "../../Components/Main/AlgoliaResultsPages.scss";
// import "./Search.scss";

const searchClient = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_TOKEN);

const Search = ({ index, refinements = [], sortByItems = [], onSearchStateChange, searchState = {} }) => {
  const intl = useIntl();

  const openFilters = () => {
    document.querySelector('.searchPageAll').classList.add('filtering');
    window.scrollTo(0, 0);
  };

  const closeFilters = () => {
    document.querySelector('.searchPageAll').classList.remove('filtering');
  };

  // change nb of hits per page depending of index (grid)
  const nBOfHitsPerPage = [18, 36, 72];

  return (
    <InstantSearch
      indexName={index}
      searchClient={searchClient}
      searchState={searchState}
      onSearchStateChange={onSearchStateChange}
    >
      <div className="d-flex searchContainer">
        <div className="container-wrapper">
          <section className="container-filters">
            <div className="container-header">
              <p className="filters">{intl.formatMessage({ id: 'algolia.filters', defaultMessage: 'Filters' })}</p>
              <div className="clear-filters" data-layout="desktop">
                <ClearRefinements
                  translations={{
                    reset: (
                      <>
                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 11 11">
                          <g fill="none" fillRule="evenodd" opacity=".4">
                            <path d="M0 0h11v11H0z" />
                            <path
                              fill="#000"
                              fillRule="nonzero"
                              d="M8.26 2.75a3.896 3.896 0 1 0 1.102 3.262l.007-.056a.49.49 0 0 1 .485-.456c.253 0 .451.206.437.457 0 0 .012-.109-.006.061a4.813 4.813 0 1 1-1.348-3.887v-.987a.458.458 0 1 1 .917.002v2.062a.459.459 0 0 1-.459.459H7.334a.458.458 0 1 1-.002-.917h.928z"
                            />
                          </g>
                        </svg>
                        {intl.formatMessage({ id: 'algolia.filters.clear', defaultMessage: 'Clear filters' })}
                      </>
                    ),
                  }}
                />
              </div>
              <div className="clear-filters">
                <ResultsNumberMobile />
              </div>
            </div>
            {refinements && (
              <div className="container-body">
                <RefinementLists index={index} intl={intl} refinements={refinements} />
              </div>
            )}
          </section>

          <div className="container-filters-footer" data-layout="mobile">
            <div className="container-filters-footer-button-wrapper">
              <ClearFiltersMobile closeFilters={closeFilters} />
            </div>

            <div className="container-filters-footer-button-wrapper">
              <SaveFiltersMobile onClick={closeFilters} />
            </div>
          </div>
        </div>
        <div className="resultsContainer">
          <header className="container-header container-options">
            <SearchBox
              translations={{
                placeholder: intl.formatMessage({
                  id: 'algolia.search.placeholder',
                  defaultMessage: 'Search here...',
                }),
              }}
            />
            {sortByItems && <CustomSortBy sortByItems={sortByItems} index={index} />}
            <HitsPerPage
              className="container-option"
              items={nBOfHitsPerPage.map((value, i) => ({
                label: `${value} ${intl.formatMessage({ id: 'algolia.hitsPerPage', defaultMessage: 'per page' })}`,
                value: value,
              }))}
              defaultRefinement={nBOfHitsPerPage[0]}
            />
          </header>
          <CustomHits index={index} />

          <NoSearchResults />
          <footer className="container-footer">
            <Pagination
              padding={2}
              showFirst={true}
              showLast={true}
              // force scroll to top of the page each time you change page
              onClick={typeof window !== 'undefined' && window.scrollTo(0, 0)}
              translations={{
                previous: (
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="#000"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.143"
                    >
                      <path d="M9 5H1M5 9L1 5l4-4" />
                    </g>
                  </svg>
                ),
                next: (
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="#000"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.143"
                    >
                      <path d="M1 5h8M5 9l4-4-4-4" />
                    </g>
                  </svg>
                ),
              }}
            />
          </footer>
          {/* {index !== 'Challenge' && ( */}
          <aside data-layout="mobile">
            <button className="filters-button" data-action="open-overlay" onClick={openFilters} type="button">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14">
                <path
                  d="M15 1H1l5.6 6.3v4.37L9.4 13V7.3z"
                  stroke="#fff"
                  strokeWidth="1.29"
                  fill="none"
                  fillRule="evenodd"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              {intl.formatMessage({ id: 'algolia.filters', defaultMessage: 'Filters' })}
            </button>
          </aside>
          {/* )} */}
        </div>
      </div>
    </InstantSearch>
  );
};

export default Search;

// custom component to show a single SDG filter
const singleSdg = (sdg) => (
  <Box bg={sdg.color} color="white" px={2} py={2} borderRadius="5px" width="76px" minHeight="76px">
    <span style={{ lineHeight: sdg.label.length > 20 ? '.7rem' : '.85rem' }}>
      <span style={{ fontSize: '1rem' }}>{sdg.value} - </span>
      <span
        style={{
          fontSize: sdg.label.length > 20 ? '.75rem' : '.85rem',
          wordBreak: 'break-word',
        }}
      >
        {sdg.label}
      </span>
    </span>
  </Box>
);

const transformItems = (items, attribute, intl, index) =>
  // map through items, and transform them under certain conditions
  items.map((item, key) => ({
    ...item,
    label:
      attribute === 'interests' // if attribute is interests/sdg
        ? // show sdg custom component, with props coming from external "defaultSdgsInterests" function returning all sdgs info
          singleSdg(defaultSdgsInterests(intl.formatMessage).find((element) => element.value == item.label))
        : attribute === 'status' && index === 'Challenge' // if attribute is a status (from challenge), translate status to its full name
        ? intl.formatMessage({ id: `challenge.info.status.${item.label}`, defaultMessage: item.label })
        : attribute === 'status' && (index === 'Need' || index === 'Project') // if attribute is a status (from need or project), translate status to its full name
        ? intl.formatMessage({ id: `entity.info.status.${item.label}`, defaultMessage: item.label })
        : attribute === 'maturity' && index === 'Project' // if attribute is project's maturity, translate status to its full name
        ? intl.formatMessage({ id: `project.maturity.${item.label}`, defaultMessage: item.label })
        : // else simply display item label
          item.label,
  }));

const CustomSortBy = ({ sortByItems, index: defaultIndex }) => {
  // defaultRefinement is first index of sortByItems array (from [active-index.js])
  const defaultRefinement = sortByItems[0].index;
  const items = sortByItems.map(({ index = defaultIndex, label = 'Undefined' }) => ({ value: index, label }));
  return <SortBy className="container-option" defaultRefinement={defaultRefinement} items={items} />;
};

const attributeToTitleIntl = (attribute) => {
  switch (attribute) {
    case 'ressources':
      return { id: 'user.profile.resources', defaultMessage: 'Resources' };
    case 'skills':
      return { id: 'user.profile.skills', defaultMessage: 'Skills' };
    case 'interests':
      return { id: 'algolia.interests', defaultMessage: 'Sustainable Development Goals' };
    case 'project.title':
      return { id: 'general.projects', defaultMessage: 'Projects' };
    case 'programs.title':
      return { id: 'general.programs', defaultMessage: 'Programs' };
    case 'program.title':
      return { id: 'general.programs', defaultMessage: 'Programs' };
    case 'challenges.title':
      return { id: 'general.challenges', defaultMessage: 'Challenges' };
    case 'status':
      return { id: 'entity.info.status', defaultMessage: 'Status' };
    case 'maturity':
      return { id: 'project.maturity.title', defaultMessage: 'Maturity' };
    default:
      return { id: 'need.isUrgentShort', defaultMessage: 'Is urgent?' };
  }
};

const attributeToPlaceholderIntl = (attribute) => {
  switch (attribute) {
    case 'ressources':
      return { id: 'algolia.resources.placeholder', defaultMessage: 'Search for resources' };
    case 'interests':
      return { id: 'algolia.interests.placeholder', defaultMessage: 'Search for interests' };
    case 'challenges.title':
      return { id: 'algolia.challenges.placeholder', defaultMessage: 'Search for challenges' };
    case 'project.title':
      return { id: 'algolia.projects.placeholder', defaultMessage: 'Search for projects' };
    default:
      return { id: 'algolia.skills.placeholder', defaultMessage: 'Search for skills' };
  }
};

const CustomRefinementList = RefinementList;

const RefinementLists = ({ refinements, index, intl }) => {
  const refinementList = refinements.map(({ attribute, type, showmore = true, limit = 10, searchable = true }, key) => {
    const titleIntl = attributeToTitleIntl(attribute);
    const placeholderIntl = attributeToPlaceholderIntl(attribute);
    return (
      (index !== 'Project' || (index === 'Project' && attribute !== 'ressources')) && (
        <Panel header={intl.formatMessage(titleIntl)} className={attribute} key={key}>
          {/* {type !== 'toggle' && ( */}
          <CustomRefinementList
            attribute={attribute}
            showMore={showmore}
            limit={limit}
            searchable={searchable}
            transformItems={(items) => transformItems(items, attribute, intl, index)}
            translations={{
              placeholder: intl.formatMessage(placeholderIntl),
              noResults: intl.formatMessage({ id: 'algolia.noResult', defaultMessage: 'No result' }),
              showMore(expanded) {
                return expanded
                  ? intl.formatMessage({ id: 'general.showless', defaultMessage: 'Show less' })
                  : intl.formatMessage({ id: 'general.showmore', defaultMessage: 'Show more' });
              },
            }}
          />
          {/* )} */}
          {/* {type === 'toggle' && <ToggleRefinement attribute={attribute} label="Is urgent" value />} */}
        </Panel>
      )
    );
  });
  return refinementList;
};

const CustomHits = ({ index }) => {
  const HitComponent = React.memo(({ hit }) => {
    switch (index) {
      case 'User':
        return (
          <UserCard
            id={hit.id}
            firstName={hit.first_name}
            lastName={hit.last_name}
            nickName={hit.nickname}
            shortBio={hit.short_bio}
            logoUrl={hit.logo_url}
            canContact={hit.can_contact}
            projectsCount={hit?.stats?.projects_count}
            skills={hit.skills}
            avatarSize="6rem"
            source="algolia"
            noMobileBorder={false}
          />
        );
      case 'Project':
        if (hit.status !== 'draft') {
          // don't show draft challenges
          return (
            <ProjectCard
              id={hit.id}
              title={hit.title}
              shortTitle={hit.short_title}
              short_description={hit.short_description}
              members_count={hit.members_count}
              clapsCount={hit.claps_count}
              // postsCount={hit.posts_count}
              needs_count={hit.needs_count}
              has_saved={hit.has_saved}
              skills={hit.skills}
              banner_url={hit.banner_url || '/images/default/default-project.jpg'}
              source="algolia"
            />
          );
        } else return '';
      case 'Need':
        return (
          <NeedCard
            title={hit.title}
            project={hit.project}
            skills={hit.skills}
            resources={hit.ressources}
            hasSaved={hit.has_saved}
            id={hit.id}
            postsCount={hit.posts_count}
            publishedDate={hit.created_at}
            membersCount={hit.members_count}
            dueDate={hit.end_date}
            status={hit.status}
            source="algolia"
          />
        );
      case 'Community':
        return (
          <CommunityCard
            id={hit.id}
            title={hit.title}
            shortTitle={hit.short_title}
            short_description={hit.short_description}
            clapsCount={hit.claps_count}
            // postsCount={hit.posts_count}
            members_count={hit.members_count}
            has_saved={hit.has_saved}
            skills={hit.skills}
            banner_url={hit.banner_url || '/images/default/default-group.jpg'}
            source="algolia"
          />
        );
      case 'Challenge':
        return (
          <ChallengeCard
            id={hit.id}
            short_title={hit.short_title}
            title={hit.title}
            title_fr={hit.title_fr}
            short_description={hit.short_description}
            short_description_fr={hit.short_description_fr}
            membersCount={hit.members_count}
            needsCount={hit.needs_count}
            has_saved={hit.has_saved}
            clapsCount={hit.claps_count}
            status={hit.status}
            programId={hit.program.id}
            programShortTitle={hit.program.short_title}
            programTitle={hit.program.title}
            programTitleFr={hit.program.title_fr}
            projectsCount={hit.projects_count}
            banner_url={hit.banner_url || '/images/default/default-challenge.jpg'}
            source="algolia"
          />
        );
      case 'Space':
        return (
          <SpaceCard
            id={hit.id}
            short_title={hit.short_title}
            title={hit.title}
            title_fr={hit.title_fr}
            short_description={hit.short_description}
            short_description_fr={hit.short_description_fr}
            membersCount={hit.members_count}
            needsCount={hit.needs_count}
            has_saved={hit.has_saved}
            clapsCount={hit.claps_count}
            projectsCount={hit.projects_count}
            banner_url={hit.banner_url || '/images/default/default-space.jpg'}
            source="algolia"
          />
        );
      default:
        return (
          <UserCard
            id={hit.id}
            firstName={hit.first_name}
            lastName={hit.last_name}
            nickName={hit.nickname}
            shortBio={hit.short_bio}
            logoUrl={hit.logo_url}
            canContact={hit.can_contact}
            projectsCount={hit?.stats?.projects_count}
            avatarSize="6rem"
            source="algolia"
            noMobileBorder={false}
          />
        );
    }
  });
  return (
    <div className={`${index}-hits`}>
      <Hits hitComponent={(props) => <HitComponent {...props} />} />
    </div>
  );
};
