import Link from 'next/link';
import Loading from '../Tools/Loading';
import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuLink } from '@reach/menu-button';
import { useApi } from '~/contexts/apiContext';
import { DropDownMenu, StyledMenuButton } from './Header.styles';
import { displayObjectRelativeDate } from '~/utils/utils';
import P from '../primitives/P';
import { NotifIconWrapper } from '~/utils/notificationsIcons';
import { notifIconTypes } from '~/utils/notificationsIcons';
import tw, { theme, styled } from 'twin.macro';

const HeaderNotifications = ({ userId }) => {
  const api = useApi();
  const [loading, setLoading] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);

  useEffect(() => {
    // to make sure API calls do not set state when unmounted
    let isFetching = true;
    api
      .post('/api/graphql/', {
        query: `
            query {
              user (id: ${userId} ) {
                notifications(first: 1) {
                  unreadCount
                  totalCount
                }
              }
            }
          `,
      })
      .then((response) => {
        var notifications = response.data.data.user.notifications;
        isFetching && setUnreadNotificationsCount(notifications.unreadCount);
      });

    return () => (isFetching = false);
  }, []);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      });
  };

  const handleClick = () => {
    setLoading(true);
    api
      .post('/api/graphql/', {
        query: `
              query {
                user (id: ${userId} ) {
                  notifications(first: 20) {
                    edges {
                      node {
                        id
                        link
                        read
                        subjectLine
                        createdAt
                        category
                      }
                    }
                  }
                }
              }
            `,
      })
      .then((response) => {
        var notifications = response.data.data.user.notifications;
        setNotifications(notifications.edges.map((edge) => edge.node));
        setLoading(false);
      })
      .catch(() => setLoading(false));
  };

  return (
    <Menu>
      {({ isExpanded }) => (
        <React.Fragment>
          <div tw="flex self-end mb-1">
            <StyledMenuButton
              style={{ position: 'relative' }}
              onClick={() => isExpanded && handleClick()}
              onTouchStart={handleClick} // isExpanded seems to be not working on mobile, so always trigger handleClick on mobile
            >
              <FontAwesomeIcon icon="bell" style={{ fontSize: '1.2rem' }} />
              <div tw="absolute right-0.5 -top-2 text-white text-xs font-semibold py-0 px-0.5 bg-red-600 rounded">
                {unreadNotificationsCount > 0 && unreadNotificationsCount <= 99
                  ? // if notif number is between 1 and 99, display count
                    unreadNotificationsCount
                  : // if more than 99, display 99+
                    unreadNotificationsCount > 99 && '99+'}
              </div>
            </StyledMenuButton>
          </div>
          <NotifDropDown>
            <div tw="flex flex-row justify-between items-center py-2 px-3 border-0 border-b border-solid border-gray-700 flex-wrap">
              <div tw="font-bold pr-2">
                <FormattedMessage id="settings.notifications.title" defaultMessage="Notifications" />
              </div>
              <div tw="flex flex-row items-end border border-t-0">
                <FakeLink onClick={() => markAllNotificationsAsRead()}>
                  <FormattedMessage id="settings.notifications.markAsRead" defaultMessage="Mark All as read" />
                </FakeLink>
                <div tw="px-1">.</div>
                <Link href={`/user/${userId}/settings`}>
                  <a>
                    <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
                  </a>
                </Link>
              </div>
            </div>
            <NotificationsList>
              {loading ? (
                <Loading />
              ) : (
                notifications.map((notification) => {
                  return (
                    <Notification
                      as="a"
                      href={notification.link}
                      key={notification.id}
                      hasRead={notification.read}
                      onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                    >
                      <div tw="flex flex-row">
                        <div tw="flex pr-2">
                          <NotifIconWrapper>{notifIconTypes[notification.category]}</NotifIconWrapper>
                        </div>
                        <div tw="flex flex-wrap flex-row">
                          <P mr={2} mb={0}>
                            {notification.subjectLine}
                          </P>
                          <P mb={0} color="#797979">
                            {displayObjectRelativeDate(notification.createdAt)}
                          </P>
                        </div>
                      </div>
                    </Notification>
                  );
                })
              )}
            </NotificationsList>
            <div tw="flex flex-row justify-center py-2 border-t border-gray-700">
              <Link href="/notifications" passHref>
                <a>
                  <FormattedMessage id="program.seeAll" defaultMessage="See all" />
                </a>
              </Link>
            </div>
          </NotifDropDown>
        </React.Fragment>
      )}
    </Menu>
  );
};

const NotifDropDown = styled(DropDownMenu)`
  ${tw`border border-gray-600 border-solid`}
  > div * {
    ${tw`border-t-0`}
    font-size: 0.9rem;
  }
  a {
    ${tw`whitespace-normal`}
  }
  width: 400px;

  @media (max-width: 500px) {
    width: 330px;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
  @media (max-width: 425px) {
    width: 280px;
  }
  @media (max-width: 400px) {
    width: 235px;
  }
  @media (max-width: 360px) {
    width: 215px;
  }
  @media (max-width: 330px) {
    width: 205px;
  }
`;

const NotificationsList = styled.div`
  ${tw`overflow-y-scroll`}
  height: 470px;
  max-height: 70vh;
  a {
    ${tw`whitespace-normal`}
    font-size: .9rem;
  }
`;

const Notification = styled(MenuLink)`
  background: ${({ hasRead }) => (hasRead ? 'white' : '#EDF2FA')};
  ${tw`border-0 border-b border-gray-300 border-solid`}
  ${tw`py-2.5 px-3`}
  &:hover {
    background: ${({ hasRead }) => (hasRead ? '#f6f6f6' : '#e2e7ee')};
    color: inherit;
  }
`;

const FakeLink = styled.div`
  color: ${theme`colors.primary`};
  ${tw`cursor-pointer hover:opacity-80`}
`;

export default HeaderNotifications;
