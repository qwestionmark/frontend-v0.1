import styled from '~/utils/styled';
import { space, color, typography, flexbox, layout } from 'styled-system';

export default styled.p`
  ${[space, color, typography, flexbox, layout]};
  /* line-height: ${(p) => (p.fontSize ? p.fontSize : p.theme.fontSizes.xl)}; */
  color: ${(p) => (p.color ? p.color : p.theme.colors.greys['800'])};
  /* max-width: 80ch; */
`;
