import React from 'react';
import Carousel from './';
import Box from '../Box';
export default { title: 'Carousel' };
import { number, boolean } from '@storybook/addon-knobs';
const Element = () => <Box width={14} height={12} backgroundColor="primary" />;
export const index = () => (
  <Box width="30%">
    <Carousel spaceX={2}>
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
    </Carousel>
  </Box>
);
export const spaceX = () => (
  <Box width="30%">
    <Carousel spaceX={number('Space X', 5)}>
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
    </Carousel>
  </Box>
);
export const noDots = () => (
  <Box width="30%">
    <Carousel spaceX={2} showDots={boolean('Show dots', false)}>
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
      <Element />
    </Carousel>
  </Box>
);
