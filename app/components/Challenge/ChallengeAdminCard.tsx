import React, { FC, ReactNode, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import { Challenge } from '~/types';
import A from '../primitives/A';
import Box from '../Box';
import Button from '../primitives/Button';
import 'twin.macro';

interface Props {
  challenge: Challenge;
  callBack: () => void;
}
const ChallengeAdminCard: FC<Props> = ({ challenge, callBack }) => {
  const [sending, setSending] = useState<'remove' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const [imgToDisplay, setImgToDisplay] = useState('/images/default/default-challenge.jpg');
  const api = useApi();

  const removeChallenge = (): void => {
    setSending('remove');
    api
      .delete(`/api/programs/${challenge.program[0].id}/challenges/${challenge.id}`)
      .then(() => {
        setSending(undefined);
        callBack();
      })
      .catch((err) => {
        console.error(err);
        setSending(undefined);
        setError(<FormattedMessage id="err-" defaultMessage="An error has occured" />);
      });
  };
  useEffect(() => {
    challenge.logo_url && setImgToDisplay(challenge.logo_url);
  }, [challenge.logo_url]);

  const bgLogo = {
    backgroundImage: `url(${imgToDisplay})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (challenge) {
    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent="space-between" key={challenge.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <Box>
              <A href={`/challenge/${challenge.short_title}`}>{challenge.title}</A>
              <Box fontSize="85%" pt={1}>
                <FormattedMessage id="attach.status" defaultMessage="Status : " />
                <FormattedMessage id={`challenge.info.status.${challenge.status}`} defaultMessage={challenge.status} />
              </Box>
            </Box>
          </Box>
          <Box row alignItems="center" justifyContent={['space-between', 'flex-end']}>
            <Box pr={8}>
              <FormattedMessage id="attach.members" defaultMessage="Members : " />
              {challenge.members_count}
            </Box>
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeChallenge} tw="ml-3">
              {sending === 'remove' && (
                <>
                  <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                  &nbsp;
                </>
              )}
              <FormattedMessage id="general.remove" defaultMessage="Remove" />
            </Button>
            {error && <Alert type="danger" message={error} />}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ChallengeAdminCard;
