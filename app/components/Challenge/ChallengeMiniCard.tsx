import Link from 'next/link';
import React, { FC } from 'react';
import { useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import Box from '../Box';
import Card from '../Card';
// import Grid from '../Grid';
import Button from '../primitives/Button';
// import { StatusCircle, statusToStep } from './ChallengeStatus';
// import ReactTooltip from 'react-tooltip';

const CustomCard = styled(Card)`
  /* padding: 0; */
  transition: box-shadow 0.3s ease-in-out;
  a {
    color: ${(p) => p.theme.colors.greys['800']};
    text-decoration: none;
  }
  h3 {
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    &:hover {
      text-decoration: underline;
    }
  }
`;

interface Props {
  icon: string;
  title: string;
  shortTitle: string;
  status?: string;
  projectId?: number;
  projectChallengeId?: number;
  onChallengeDelete?: (id: number) => void;
  isEditCard?: boolean;
}

const ChallengeMiniCard: FC<Props> = ({
  icon,
  title,
  shortTitle,
  status = 'pending',
  projectChallengeId,
  projectId,
  onChallengeDelete,
  isEditCard = false,
}) => {
  const theme = useTheme();
  const api = useApi();
  const { formatMessage } = useIntl();
  const cancelParticipation = () => {
    api
      .delete(`/api/challenges/${projectChallengeId}/projects/${projectId}`)
      .then(() => {
        onChallengeDelete(projectChallengeId);
      })
      .catch((err) => {
        console.error(`Couldn't remove project of challenge with id=${projectChallengeId}`, err);
      });
  };
  // const statusStep = statusToStep(status);

  return (
    <CustomCard>
      <Box height="100%" width={['14rem']} justifyContent="space-between">
        <Link href={`/challenge/${shortTitle}`}>
          <a>
            <Box justifyContent="center" alignItems="center" spaceY={6}>
              {icon && (
                <img src={icon} width="100%" alt="Challenge Icon" style={{ objectFit: 'cover', maxHeight: '110px' }} />
              )}
              <Box fontFamily="secondary" fontSize={5} px={4}>
                <h3>{title}</h3>
              </Box>
            </Box>
          </a>
        </Link>
        {/* {status && !isEditCard && (
          // where statusStep[0] = step number, and statusStep[1] = step color
          <>
            <Grid
              display="grid"
              gridTemplateColumns="27% 53%"
              justifyContent="center"
              alignItems="center"
              data-tip={formatMessage({
                id: 'entity.info.status',
                defaultMessage: 'Status',
              })}
              data-for="challengeMiniCard_status"
            >
              {status !== 'draft' && (
                <>
                  <StatusCircle mr={2} step={statusStep[0]}>
                    <Box>{statusStep[0]}/4</Box>
                  </StatusCircle>
                  <Box color={statusStep[1]}>
                    {formatMessage({
                      id: `challenge.info.status.${status}`,
                      defaultMessage: status,
                    })}
                  </Box>
                </>
              )}
            </Grid>
            <ReactTooltip id="challengeMiniCard_status" effect="solid" place="bottom" />
          </>
        )} */}
        {status && isEditCard && (
          <Box row justifyContent="space-around" mt={3}>
            <Box
              textAlign="center"
              width="fit-content"
              py={1}
              px={2}
              color={status === 'pending' ? 'black' : 'white'}
              borderRadius={5}
              alignSelf="center"
              backgroundColor={status === 'pending' ? theme.colors.warnings['700'] : theme.colors.successes['500']}
            >
              {formatMessage({
                id: `challenge.acceptState.${status}`,
                defaultMessage: status,
              })}
            </Box>
            <Button btnType="danger" onClick={cancelParticipation}>
              {formatMessage({ id: 'attach.remove', defaultMessage: 'Remove' })}
            </Button>
          </Box>
        )}
      </Box>
    </CustomCard>
  );
};

export default ChallengeMiniCard;
