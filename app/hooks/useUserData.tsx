import { useContext } from 'react';
import { UserContext } from '~/contexts/UserProvider';
import useGet from '~/hooks/useGet';
import { User } from '~/types';

export default function useUserData() {
  const userContext = useContext(UserContext);
  const { data, error: userDataError, ...rest } = useGet<User>(
    userContext?.credentials?.userId && `/api/users/${userContext.credentials.userId}`
  );
  return { userData: data, userDataError, ...rest };
}
